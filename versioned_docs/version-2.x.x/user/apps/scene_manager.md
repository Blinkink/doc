---
title: Scene Manager
---

:::danger Work in progress
This documentation is not yet **redacted**.
:::

<img height="300" src={require("../../assets/scene-manager-base.png").default} />


## Introduction
:::caution Experimental project
- This application is a draft for a future tool called **Hypervisor**.
- It is only available for **Modo**.
- Since it was a R&D project, this app does not focus on UX.
- This application is **not maintained any more**.
:::

The **Scene Manager** <img  width="20" height="20" src={require("../../assets/apps/scene_manager.png").default} /> allows to inspect the content of a CG scene and handle its ins and outs. This is used for building scenes from scratch, updating existing assets, exporting/importing geometry, material...

### Features

### Future

## Usage
![](../../assets/scene-manager-add-item.png)

![](../../assets/scene-manager-asset.png)

## Reference
![](../../assets/scene-manager-base.png)

![](../../assets/scene-manager-toolbox.png)

![](../../assets/scene-manager-task.png)


