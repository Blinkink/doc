---
title: Troubleshooting
sidebar_position: 5
---
## Client
### Logs
The API and the applications record persistent logs. By default, these logs are store in `~/.blink/logs/`. The backend logs are in an `api` folder, while each app has its own directory.

:::tip
If Bip is crashing unexpectedly at launch, check first `~/.blink/logs/launcher` and `~/.blink/logs/api` to see the traceback.
:::

### Force update/reset
Sometimes, the **Client** git repository must be reset manually. These are the typical cases:
- An update breaks the retro-compatibility
- The updating system has been updated
- The user have modified the code
- Some parasite legacy files (obsolete .pyc fo example) can compromise the code integrity

A simple way to sort this out is to perform manually the git update, after cleaning it with the git `clean` and `reset` commands.

:::info
This is a **shell** example, it must be executed from a shell terminal (Git bash, Windows Powershell...)
:::

```shell
cd C:/Users/blinkink/AppData/Local/Blink/bip  # default installation directory
cp bip/config.yml config.yml  # copy out so it does not get reset
git reset --hard
git clean -fdx
git fetch --tags -f
git pull origin vX.X.X  # tag or branch
# Check your Server config to match the recommended version tag
mv config.yml bip/config.yml
```

## Server
### The database is not responding
:::caution
Work in progress
:::