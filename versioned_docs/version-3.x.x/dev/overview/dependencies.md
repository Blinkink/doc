---
title: Dependencies
sidebar_position: 2
---
## Core
- **Jeanpaulstart**: batches backend ([by our friends from Cube Creative](https://github.com/cube-creative/jeanpaulstart))
- **GitPython**: updating method
- **PySide2**: GUI
- **neo4j**: database
  
## Formats
- **PyYAML**: configuration
- **Markdown**: documentation

## Utils
- **OpenCV**: image data reading
- **pynput**: shortcut mapping
- **packaging**: version parsing