---
title: Shotgrid
---

:::caution
Shotgrid implementation is a **work in progress**. This page is often updated.
:::

:::info
If you are a developer, you might want to check the [reference documentation for `sgink`](../../dev/reference/plugins/sgink/project).
:::

## Reference
### Introduction
The Shotgrid plugin for Bip allows bridging data between Shotgrid and Bip. It is implemented by the `sgink` library, which is a built-in plugin.

### Features
- Allow Bip to interact with Shotgrid.
- Upload Bip movies as Shotgrid versions at creation.

### Future
- Upload Bip movies as Shotgrid versions.
- Synchronise sequences, shots and assets.

### Implementations
- **Media Builder**: The Media Builder can now "_Send to Shotgrid_" when generating a movie.
- **Project Manager (_config_)**: The production tracker can be defined per project from their settings.
- **Admin (_config_)**: The Bip instance and the Shotgrid server can be linked from the administration panel.

### Matching rules
- **Users**: match by email address.
- **Projects**: match by name and code.
  - _Name_: The Shotgrid name must be formatted like `XXX000 Project Name` (this is **case-sensitive**).
  - _Code_: The Shotgrid code must be formatted like `XXX000` and must be matching the Bip project prefix.
- **Items**: match by name (asset) or folder name (shots).
- **Tasks**: match by associated pipeline step (see the following _Tasks matching_ section)

#### Tasks matching
| Shotgrid (pipeline step) | Bip (task tag) |
| ------------------------ | -------------- |
| Design                   | ❌              |
| Model                    | Modelling      |
| Rig                      | Rigging        |
| Texture                  | Texturing      |
| Lookdev                  | Lookdev        |
| Character FX             | CFX            |
| Animation                | Animation      |
| Previs                   | ❌              |
| Online                   | ❌              |
| Tracking                 | Tracking       |
| Matte Painting           | ❌              |
| Paint                    | ❌              |
| Roto                     | Rotoscoping    |
| Layout                   | Layout         |
| Light                    | Lighting       |
| Render                   | ❌              |
| Comp                     | Compositing    |

## Configuration
:::caution
You must have administrator privileges on **Shotgrid and Bip** for this step.
:::

In order to allow Bip to interact with Shotgrid, you must provide Bip some authentication credentials.

- In your Shotgrid server, unfold to the user menu (top-right corner) and go to Admin > Scripts.
- Click on "_Add Script_".
- **Name**: `bip` _(recommended)_.
- Save the application key on a text editor (_it won't be shown again later_).
- Click on "_Create Script_".

![shotgrid_create_script](../../assets/shotgrid_create_script.png)

- Open the Bip Admin app and go to the Production trackers section.
- Enable _Shotgrid_.
- Enter the required details:
  - **Server**: `https://your-company.shotgunstudio.com`
  - **Script name**: `bip`
  - **Script key**: The key you kept from the Shotgrid script creation step
- If you want, you can define which production tracker will be set by default for new projects. This can be changed from the project settings any ways.

![shotgrid_bip_admin_config](../../assets/shotgrid_bip_admin_config.png)

## Usage
### Project Manager
For each project, you can set which production tracker they use. Unless you changed the default settings in the Admin app, the default is Internal.
If you switch to Shotgrid and save, it will make the Shotgrid implementation features (see above) available.

When you create a project, switch to the Tracker tab to set its tracker to Shotgrid. You can also change this setting later on.

:::caution
The fields **Project id** and **Sync** are not implemented yet.
:::

![shotgrid_project_settings](../../assets/shotgrid_project_settings.png)

### Media Builder
If your active project uses Shotgrid as a production tracker, the Media Builder shows an additional destination: **Send to Shotgrid**.

![shotgrid_mediabuilder_destination](../../assets/shotgrid_mediabuilder_destination.png)

:::info
You cannot upload over an existing version. You must version up your source files and try again.
:::

- If you check **Send to Shotgrid**, a _Settings_ button appears.
- You can either fill the required information by clicking on _Settings_, or wait for the pop-up to show up after clicking on _Export_.
- You must provide a comment.
- If a Shotgrid task is found, where the (Shotgrid) pipeline step matches the (Bip) task, it will be displayed.
- If a Shotgrid task is found, you can also log your time spent on it, in minutes, hours or days.

![shotgrid_mediabuilder_settings](../../assets/shotgrid_mediabuilder_settings.png)

