---
title: Mediabuilder
---

:::danger Work in progress
This documentation is not yet **redacted**.
:::

<img height="300" src={require("../../assets/media-builder-pipelined-settings.png").default} />


## Introduction
The **Media Builder** <img  width="20" height="20" src={require("../../assets/apps/media_builder.png").default} /> is an image and video converter. It can convert `exr` and `png` stills or sequences to `mp4`, `png` or `jpg`. When used with known files (registered with Bip), it offers additional pipeline integration: the generated movie can be ingested in the pipeline, and/or send to the dailies, and/or sent to Shotgrid, if _Sgink_ (Shotgrid integration plugin) is enabled and available for the project.

### Features

### Future

## Usage
![](../../assets/media-builder-base.png)

![](../../assets/media-builder-pipelined-settings.png)

![](../../assets/media-builder-overwrite.png)

![](../../assets/media-builder-convert.png)

![](../../assets/media-builder-success.png)

## Reference
### Settings
![](../../assets/media-builder-pipelined-settings.png)

![](../../assets/media-builder-non-pipelined-settings.png)

### Information
![](../../assets/media-builder-pipelined-information.png)