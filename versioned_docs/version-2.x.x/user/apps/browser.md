---
title: Browser
---

:::danger Work in progress
This documentation is not yet **redacted**.
:::

<img height="300" src={require("../../assets/browser-multi-project.png").default} />


## Introduction
The **Browser** <img  width="20" height="20" src={require("../../assets/apps/opener.png").default} /> is a pipeline data explorer. It allows browsing through any file that is tracked by Bip. The application can be restricted to certain rules, such as displaying only scenes. With such rules, the Browser is integrated in CG applications in different modes: **Opener** or **Saver**, which allows opening and save CG scenes without having to worry about path and filename.

### Features

### Future

## Usage

## Reference
### General
![](../../assets/browser-base.png)

![](../../assets/browser-save.png)

![](../../assets/browser-multi-project.png)

![](../../assets/browser-incomplete.png)

### Tools
![](../../assets/browser-bookmark.png)

![](../../assets/browser-inbox.png)

![](../../assets/browser-recent.png)

![](../../assets/browser-sendto-1.png)

![](../../assets/browser-search.png)

![](../../assets/browser-right-click.png)

### Messages
![](../../assets/browser-message-obsolete.png)

![](../../assets/browser-message-ownership.png)

![](../../assets/browser-message-ownership-obsolete.png)

![](../../assets/browser-message-new.png)

### Settings
![](../../assets/browser-settings.png)

