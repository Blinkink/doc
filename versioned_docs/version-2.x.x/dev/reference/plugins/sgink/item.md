---
sidebar_label: item
title: bip.plugins.sgink.item
---

## from\_bip

```python
def from_bip(item)
```

Get a Shotgrid shot or asset from a Bip item.

The item match is done with the name for assets,
or with the folder name for shots.

**Arguments**:

- `item` _Item_ - Item BipObject.
  

**Returns**:

- `dict` - A dictionary with the Shotgrid Shot/Asset id and type.

## all\_from\_bip

```python
def all_from_bip(project)
```

Get all Shotgrid items from a Bip project.

This function returns a list of mixed assets and shots
that matches the name of an item from the given project.

**Arguments**:

- `project` _Project_ - Project BipObject.
  

**Returns**:

- `list` - List of dictionaries, assets and shots.

