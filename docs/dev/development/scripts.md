---
title: For scripts
sidebar_position: 4
---

This guide explains how to **`import bip`** safely from your own code. 

There are three scenarii:

- **Development**: If you want to call the `bip` package from a script that is not ran by Bip itself (that would be from the _[Brioche](/user/apps/brioche/introduction)_ application for instance) and where the environment is not set up, you must set it manually. Typically, when developing for Bip, outside a Bip client installation, you would need to specify those environment variables.
- **Production (_internal_ to Bip runtime)**: On the contrary, if your code is to be called by Bip (indirectly or not), there is nothing to do, importing `bip` will work out-of-the-box. For example, if your script is called from a Maya that has been launched with Bip, the environment is already set up.
- **Production (_external_ to Bip runtime)**: Alternatively, if you are attempting to import `bip` from a non-Bip runtime, but there is an existing Bip client installation on your machine, the import would work, and the `bip` package would be initialized with the default values of the paths to the config and auth files, or the database password saved in the keyring.

Since Bip looks for certain **configuration files** and **data** stored in keyring at initialization, those configuration data must be explicitly provided through **environment variables**, unless it is a choice to have the default values and locations to be used (in the case of an existing Bip installation on the machine).

:::tip
For more information about environment variables, check **[this documentation](/dev/misc/env-var.md)**.
:::

In any scenario, you must be aware that any variable that is not overriden results in its default value being used. If that default value is valid (in case of an existing parallel client installation), it could for example lead to executing code to the wrong database, and therefore compromising its integrity. **Make sure you keep your development and production environments completely independent**.

:::info
If your script is using `mink.update`, you might also want to override `BIP_PYTHON_HOST`, `BIP_COMPATIBILITY_VERSION`, `BIP_CHECK_UPDATES`, `BIP_REFRESH_CONTENT` and `BIP_DEBUG_UPDATE_TARGET`.
:::

## Development

### Setup

Clone the client (https://git.blinkink.co.uk/bip/client.git) and add it to your development project dependencies (`PYTHONPATH`, `sys.path`, Pycharm's "_Project Dependencies_"...). Follow the [recommended folder structure](/dev/development/structure.md) for you working directory.

Create a **Python 3.8** virtual environment and install the client dependencies (`cd /path/to/bip/client & pip install -r requirements.txt`).

Follow [the setup tutorial](/dev/development/setup.mdx) to get a running database instance and a Bip config properly done.

### Running

When running your script, specify the following environment variables:
- `BIP_CONFIG`: Bip configuration file (_example_: `/path/to/bip/config/config.yml`)
- `BIP_AUTH`: Bip authentication file (_example_: `/path/to/bip/config/auth.yml`)
- `BIP_DATABASE_PASSWORD`: Neo4j password (_example_: `your-database-password`)

If your development machine has a production Bip client installed, **you must also override the following environment variables**, in order to avoid conflicting with the production instance:
- `BIP_PATH`: Root of the Bip ecosystem (_example_: `path/to/the/parent/folder/of/your/bip/repo`)
- `BIP_PYTHON`: Python virtual environment (_example_: `path/to/the/root/of/the/python_venv`)

:::info
If you want to develop from a host's interpreter:
- Create a **Python 3.7** virtual environment instead of 3.8.
- Install the hosted client dependencies (`cd /path/to/bip/client & pip install -r requirements-hosted.txt`).
- Specify `BIP_PYTHON_HOST` value (_example_: `path/to/the/root/of/the/hosted_python_venv`).
:::

## Production (Bip runtime)

**Nothing to do**. If running a script from a Python interpreter that has been launched through Bip, all the configuration is already set.

## Production (external)

If you want to import Bip from an existing installation, you have to add to your `PYTHONPATH` or `sys.path` the `/path/to/your/Blink/bip` installation. It is usually in `%USERPROFILE%/Blink/bip`.

Make sure your Python interpreter has all the dependencies required from `/path/to/your/bip/client/requirements.txt`.

There is no environment variable to specify since the default values will be used.
