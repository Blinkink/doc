---
title: Settings
---

<img height="300" src={require("../../assets/settings_profile.png").default} />


## Introduction

The **Settings** <img  width="20" height="20" src={require("../../assets/apps/settings.png").default} /> is the centralized user settings application for the Bip ecosystem. It allows to edit the user profile, change global (non app-specific) settings, and change applications settings.

:::info
All the Bip apps have their individual settings managed from this application.
:::

### Features
- Edit user profile and active project
- Edit application settings
- Edit system settings, relative to the local instance of the Bip client

### Future
- Version settings to be user-based instead of machine-based.

## Reference
### Profile

Edit your personal information, such as **email** (optional) and **messaging link** (optional). The first name, last name, status (active or not), role and privilege can only be changed by an administrator.

:::caution
The **password change** has not yet been implemented.
:::

![](../../assets/settings_profile.png)

### Project

Change your **current active project**.

In Bip, each user can work on multiple project (if allowed to by the administrator), but must have one active project, which is, in other words, your default project.

:::tip
The active project can also be changed from the `set project` [Launcher command](launcher).
:::

You can also **lock** your active project. Some Bip applications, like the [Browser](browser) can change your active project on the fly, by allowing you to select another project from their UI. Locking your active project disable this.

:::info
This feature can be globally **disabled and overridden by your administrator**. If so, the server value is shown and appears greyed.
:::

![](../../assets/settings_projects.png)

### Applications

See each application page for more details about their specific settings:
- [Browser](browser#settings)
- [Launcher](launcher#settings)
- [Watcher](watcher#settings)
- [Project Manager](project_manager#settings)

### Version

:::caution
These settings are **machine-based**, not user-based. It means that, for the same user, **they can differ from a machine to another**.
:::

Bip allows you to choose which version of the Bip client you are using. Bip is updated from its Git repository. 

:::tip
The version can also be changed from the `set source` [Launcher command](launcher).
:::

If you are not familiar with Git, it is a powerful versioning system which allows tracking modifications of the code and having several simultaneous states of this code, either with **branches** (like "stable", "experimental") or **tags** (like "v2.3.2").

From this view, you can choose how you want to set up your Bip client version.

Because Bip is in permanent active development, with updates being shipped almost every week, this feature exists for two reasons:
- Freeze a version of Bip per project. While Bip keeps changing, it can be safer to stick to the same version throughout a project.
- Be able to switch to a version of Bip which is under development, for testing purpose, and switch back to a production-safe version afterwards.

:::info
This feature can be globally **disabled and overridden by your administrator**. If so, the server value is shown and appears greyed.
:::

:::info
Any change of a version setting automatically triggers a **restart** and an **update** of Bip.
:::

#### Recommended

If you choose "**Recommended**", you get the version defined by your administrator in the Bip server settings. This means that your Bip client will only get update when the administrator decides so, and to the version they decided.

:::caution
If you don't know what you are doing, this should probably be your default setting.
:::

![](../../assets/settings_version_recommended.png)

#### Custom branch

If you choose "**Custom branch**", a list of the available branches will appear. These branches are read from the source code repository, which is defined by your administrator.

For example, if a TD or a developer is working on a new feature, or a bug fix that needs to be tested by users before being shipped in the stable version, they might ask some users to switch to the branch dedicated to this feature or fix. Once the tests have been run, the users can switch back to the "recommended" mode and be sure to work with a safe version of Bip.

![](../../assets/settings_version_custom_branch.png)

#### Custom tag

If you choose "**Custom tag**", a list of the available tags will appear. These tags are read from the source code repository, which is defined by your administrator.

For example, if a user has to work back in an old project, they might want to rollback to the Bip version with which this project has been made.

:::info
Depending on the administrator settings, the version switch can be done automatically when changing the active project.
:::

![](../../assets/settings_version_custom_tag.png)

### Updates

:::caution
These settings are **machine-based**, not user-based. It means that, for the same user, **they can differ from a machine to another**.
:::

- **Repository** (_read-only_): Url of the Bip code repository which is used for updates. Usually it is the official Bip repository, unless your administrator decided to work on their own fork of Bip. _This setting comes from the Bip server configuration, and is only displayed here for information._
- **Look for updates at startup** (_disabled_): Defines if Bip automatically looks for updates when started. _This setting is greyed because the case where it is disabled has not yet been implemented_.
- **Force version from server**: (_disabled_): Defines if Bip always uses the version settings from the server. _This setting is greyed because the case where it is disabled has not yet been implemented_.

![](../../assets/settings_updates.png)


### System

:::caution
These settings are **machine-based**, not user-based. It means that, for the same user, **they can differ from a machine to another**.
:::

#### Preferences
- **Start Launcher at startup:** If on, the Bip Launcher will start when login in.
- **Hostname**: Name of the current machine. By default, the system hostname is used, but it can be customized. This hostname is used in tracking apps, such as the [Watcher](watcher).

#### Database
:::info
These settings are only editable with **administrator** privileges.
:::

- **URI**: Address of the Bip server.
- **User**: Username for the Bip server.
- **Password**: Password for the Bip server.

#### Content
:::info
- The content is a group of files that TD use to customize Bip. See more in the [TD documentation](../../td/intro).
- These settings are only editable with **administrator** privileges.
:::

- **Repository** (_read-only_): Url of the Content files repository. _This setting is provided by the server config, which can only be edited from the [Admin](admin) application.
- **Path**: Location where the Content repository is cloned for the current machine.

![](../../assets/settings_system.png)