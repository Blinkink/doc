---
sidebar_label: specials
title: bip.link.specials
---

## `init`

```python
def init(username=DEFAULT_USERNAME, password=DEFAULT_PASSWORD)
```

Initializes an empty Neo4j database for Bip.

Creates elementary data in order to make the database
compatible with Bip.
- Default user privileges: admin, user and guest.
- Default user role: internal.
- Default user with admin privilege. If not specified, the username
will be "_admin_" and the password will be "_bip_".

This function must be executed on an **empty database**.

**Arguments**:

- `username` _str, optional_ - If set, it will override the default username ("admin")
  Must contain only alphanumerical characters, dashes, dots and underscores.
- `password` _str, optional_ - If set, it will override the default password ("bip")
  

**Returns**:

- `bool` - True for success, False otherwise.
  

**Raises**:

  ValueError:
  - The database is already populated.
  - The username contains forbidden characters

## `setup`

```python
def setup(content_source, bip_source=DEFAULT_SOURCE, minimal_version=None, recommended_version=None)
```

Creates the default ServerSetting.

Usually used after `link.specials.init()`, this function sets the base settings of a
Bip server by populating a unique Config object.

**Arguments**:

- `content_source` _str_ - A valid git url to a content repository.
- `bip_source` _str, optional_ - A valid git url to a Bip repository.
  - Use this parameter if you are using your own fork of Bip.
  - The official repository is use if unspecified.
- `recommended_version` _str, optional_ - Version number or `latest`. Defaults to None.
  - If using a version number, it must be an existing tag in
  - the `bip_source` repository. Formatted like so `vX.X.X`.
  - If left to None, the most recent tag found in the bip_source repository is used.
- `minimal_version` _str, optional_ - Version number or `latest`. Defaults to None.
  - If using a version number, it must be an existing tag in
  the `bip_source` repository. Formatted like so `vX.X.X`.
  - If using a version number, it must be inferior or equal to the
  recommended version.
  - If left to None, the most recent tag found in the bip_source repository is used.
  

**Returns**:

- `Config` - The server config object that has been created.
  

**Raises**:

  ValueError:
  - The server config already exists on the server.
  - `content_source` is not a valid Git repository.
  - `bip_source` is not a valid Git repository.
  - `minimal_version` is set to `latest`, but `recommended_version` is not.
  - `recommended_version` is smaller than `minimal_version`.
  - `minimal_version` is not found in the `bip_source` repository tags.
  - `recommended_version` is not found in the `bip_source` repository tags.

