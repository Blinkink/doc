---
title: Server
sidebar_position: 2
---

:::danger
The Bip server is under active development, and there is no installation process for the server. This step-by-step is for development purpose only.
:::

## Step 1: On the server
- Install Neo4j on your server machine ([_see the official documentation_](https://neo4j.com/docs/operations-manual/current/installation/linux/debian/))
- Go to **https://domain-name-or-ip:7473/browser/**
- Create a `bip` Neo4j user - Install Neo4j on your server machine ([_see the official documentation_](https://neo4j.com/docs/cypher-manual/current/administration/security/users-and-roles/#administration-security-users-create))

## Step 2: On your machine
- Checkout the Bip client repository
- Modify the `bip/config.yml` with these settings:
```yaml
database:
  password: your-password
  uri: bolt://domain-name-or-ip:7687
  user: bip
```
- Tweak the `utils/initialize_config.py` script with the relevant values
- Tweak the `utils/feed_database.py` script with your own workflow requirements
- Make sure you added the client repo to your `PYTHONPATH` if you are not using an IDE like PyCharm
- Run `initialize_config.py`
- Run `feed_database.py`
- Make a script or use the **Users** application from a client instance to create users. The default user is:
    - Username: `admin`
    - Password: `bip`