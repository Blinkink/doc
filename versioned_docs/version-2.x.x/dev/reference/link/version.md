---
sidebar_label: version
title: bip.link.version
---

## `Version`

```python
class Version(Data)
```

Representation of a Bip version.

A Version object represent a file or a group of files corresponding to an iteration of a Component.
For example, if a component represents a "lighting scene in Maya", the different versions,
or iterations of this file are represented with the Version object.

Unlike Projects, Items and Components, Versions are not defined by any tag.
Their rules are defined by the upper ComponentTag.

:::info
This class inherits from **Data**, like Project, Item and Component, since they share similar behaviour.
See `bip.link.data.Data` for inherited methods.
:::

:::tip
All paths are standardized: They use the `/` Unix separator.
:::

**Attributes**:

- `uid` _str_ - Bip unique identifier (uuid4).
  Edition is forbidden if this is an persistent (saved) entity.
- `slug` _str_ - Version number. Same as the `folder_name` property.
- `created_with` _str_ - Optional, name of the software used for creating the file(s).

### `number`

```python
@property
def number() -> int
```

Version number

**Returns**:

- `int` - Version number.

### `author`

```python
@property
def author() -> User
```

Author of the version.

:::danger
Not yet implemented.
:::

**Todo**:

  - To implement
  

**Returns**:

- `User` - Author of the version.

### `parent`

```python
@property
def parent() -> Component
```

Parent component.

**Returns**:

- `Component` - Parent component.

### `component`

```python
@property
def component() -> Component
```

Convenient alias for `Item.parent`.

### `item`

```python
@property
def item() -> Item
```

Parent item.

**Returns**:

- `Item` - Parent item.

### `folder_name`

```python
@property
def folder_name() -> str
```

Folder name of the Version.

:::caution
This is no the path of the file(s). The folder name is the formatted version number. Example: `"v001"`.
:::

**Returns**:

- `str` - folder name.

### `creation_time`

```python
@property
def creation_time() -> datetime
```

When the version has been added to the database.

**Returns**:

- `datetime` - creation time.

### `modification_time`

```python
@property
def modification_time() -> datetime
```

When the version has been changed on the database.

**Returns**:

- `datetime` - modification time.

### `path`

```python
@property
def path() -> str
```

Root path of the version.

:::caution
This path does not include the file(s) substructure.
:::

If the parent Component is not collapsed (see `Component.collapsed`), the folder name
is appended to the path:

**Examples**:

  - Collapsed: `/path/to/version/`
  - Not collapsed: `/path/to/version/v001/`
  

**Returns**:

- `str` - Root path.

### `full_path`

```python
@property
def full_path() -> str
```

Full path of the version.

This path includes the file substructure.

:::caution
Only works with single file. If the version has multiple files, use `full_paths` instead.
:::

**Raises**:

- `ValueError` - The version has multiple files.
  

**Returns**:

- `str` - Full path.

### `full_paths`

```python
@property
def full_paths() -> List[str]
```

List of full paths.

These paths include the file substructure.

If the version only has one file, prefer `full_path`.

**Returns**:

- `list` - Collection of full paths for each file of the version.

### `files`

```python
@property
def files() -> List[str]
```

List of files.

:::info
Files can contain a substructure, with as many sub-folders as needed.
For a Bip version, a file can be `my_file.ext` as well as `sub/path/my_file.ext`.
:::

**Returns**:

- `list` - Collection of files.

### `file`

```python
@property
def file() -> Union[str, None]
```

First file of the version.

:::caution
Only works with single file. If the version has multiple files, use `full_paths` instead.
:::

:::info
File can contain a substructure, with as many sub-folders as needed.
For a Bip version, a file can be `my_file.ext` as well as `sub/path/my_file.ext`.
:::

**Raises**:

- `ValueError` - The version has multiple files.
  

**Returns**:

- `list` - Collection of files.

### `is_single`

```python
@property
def is_single() -> bool
```

Does the version has more than one file entry.

**Returns**:

- `bool` - True is only one file, False otherwise.

### `save`

```python
def save()
```

Saves the Version to the database.

If the Version is floating, saving makes it persistent.

Attributes changes are not applied to the database at modification.
Saving the Version does.

**Raises**:

- `ValueError` - Failed validation.
- `TypeError` - Failed validation.

### `delete`

```python
def delete()
```

Deletes the Version.

Deleting the Component deletes any Metadata attached to it.

### `creation_time_to_text`

```python
def creation_time_to_text() -> str
```

Human-readable creation time.

Format pattern: _"%d/%m/%Y %H:%M:%S"_

**Returns**:

- `str` - Formatted creation time

### `modification_time_to_text`

```python
def modification_time_to_text() -> str
```

Human-readable modification time.

Format pattern: _"%d/%m/%Y %H:%M:%S"_

**Returns**:

- `str` - Formatted modification time

### `generate`

```python
@classmethod
def generate(cls, component: Component, files: Union[str, List[str]], author: Optional[User] = None, auto_save: bool = True, **kwargs) -> Version
```

Generates a Version object.

By default, the generated Version is saved straight after creation. In some cases, it can be useful to get the
floating (not recorded on the database) object in order to perform further operation, and save after that.
That can be achieved by setting `auto_save` to False.

**Arguments**:

- `component` - Component: Parent component.
- `files` - Union[str, List[str]]: Single file name or collection of file names.
  The file name can contain a substructure, with as many sub-folders as needed.
  It can be `my_file.ext` as well as `sub/path/my_file.ext`.
- `author` - Optional[User]: Author of the Version. If not specified,
  the current user (the one running Bip when executing the operation) is used.
- `auto_save` - bool: If True, the returned object is saved. Otherwise it is floating. (Default value = True)
- `**kwargs` - Any non-mandatory valid attribute of Item can be passed.
  

**Raises**:

- `ValueError` - Failed validation, if auto_save is True.
- `TypeError` - Failed validation, if auto_save is True.
  

**Returns**:

- `Version` - Generated Version.

### `get`

```python
@classmethod
def get(cls, component: Component, number: Optional[int] = None) -> Union[List[Version], Version]
```

Get a specific Version or all Versions from a Component.

It is possible to get a specific Version from its `number`.
If no number is specified, the getter becomes a global getter and returns a collection of Versions.

**Arguments**:

- `component` - Component: Component object to search within.
- `number` - Optional[str]: Specific version number. (Default value = None)
  

**Raises**:

- `TypeError` - An argument has an unexpected type.
- `LookupError` - No matching entity found.
  

**Returns**:

  - list: Collection of Versions, if no uid or slug specified.
  - Version: Single Version if a uid or a slug has been specified.

### `get_latest`

```python
@classmethod
def get_latest(cls, component: Component) -> Optional[Version]
```

Get only the latest version of a Component.

**Arguments**:

- `component` - Component: parent Component to look into.
  

**Returns**:

- `list` - Collection of matching Versions.

### `get_from_filename`

```python
@classmethod
def get_from_filename(cls, filename: str, component: Component) -> List[Version]
```

Get all the Versions containing the given filename.

**Arguments**:

- `filename` - str: Looked up filename.
- `component` - Component: parent Component to look into.
  

**Returns**:

- `list` - Collection of matching Versions.

## `get`

```python
def get(component: Component, number: Optional[int] = None) -> Union[Version, List[Version]]
```

Convenient shortcut to `Version.get` for getting a specific Version or all Versions.

## `get_version`

```python
def get_version(component: Component, number: int = None) -> Version
```

Convenient shortcut to `Version.get` for getting a specific Version.

## `get_versions`

```python
def get_versions(component: Component) -> List[Version]
```

Convenient shortcut to `Version.get` for getting all Versions.

## `new`

```python
def new(component: Component, files: Union[str, List[str]], author: Optional[User] = None, auto_save=True, **kwargs) -> Version
```

Convenient shortcut to `Version.generate`.

## `new_version`

```python
def new_version(component: Component, files: Union[str, List[str]], author: Optional[User] = None, auto_save=True, **kwargs) -> Version
```

Convenient shortcut to `Version.generate`.

## `get_latest_version`

```python
def get_latest_version(component: Component) -> Version
```

Convenient shortcut to `Version.get_latest`.

## `get_from_filename`

```python
def get_from_filename(filename: str, component: Component) -> List[Version]
```

Convenient shortcut to `Version.get_from_filename`.

