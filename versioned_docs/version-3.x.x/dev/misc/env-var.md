---
title: Environment Variables
sidebar_position: 6
---
## Parametric
These variables are not set by Bip, but if they are manually set, they would override the default internal value they refer to.

| Name                    | Description                                                  | Type                | Default                                       |
| ----------------------- | ------------------------------------------------------------ | ------------------- | --------------------------------------------- |
| `BIP_CHECK_UPDATES`     | Defines if the Launcher checks for updates                   | bool                | `true`                                        |
| `BIP_REFRESH_CONTENT`   | Defines if the Launcher reloads (`git pull`) the content when started | bool       | `true`                                        |
| `BIP_DATABASE_URI`      | Overrides the database URI                                   | str                 | Server-provided value (`ServerConfig` object) |
| `BIP_DATABASE_USER`     | Overrides the database user                                  | str                 | Server-provided value (`ServerConfig` object) |
| `BIP_DATABASE_PASSWORD` | Overrides the database password                              | str                 | Server-provided value (`ServerConfig` object) |



## Data
These variables are always set by Bip and can be used in Bip-executed user scripts.
Some of them can be overridden for **debugging** purpose only.

:::info
On a classic installation, `BIP_PATH` should be set permanently unless the Updater runs for the first time.
Otherwise, unset `BIP_PATH` should only be overridden for development purpose.
:::

| Name                    | Description                                                        | Type           | Example                                               | Overridable |
|-------------------------|--------------------------------------------------------------------|----------------|-------------------------------------------------------|-------------|
| `BIP_PATH`              | Main installation folder                                           | directory path | *`~\AppData\Local\Blink`*                             | ✅           |
| `BIP_ROOT`              | Bip repository root                                                | directory path | *`~\AppData\Local\Blink\bip`*                         | ✅           |
| `BIP_PACKAGE`           | Bip package root                                                   | directory path | *`~\AppData\Local\Blink\bip\bip`*                     | ❌           |
| `BIP_PYTHON`            | Python 3.8 virtual environment                                     | directory path | *`~\AppData\Local\Blink\python`*                      | ✅           |
| `BIP_PYTHON_LIBS`       | Python 3.8 site-packages folder                                    | directory path | *`~\AppData\Local\Blink\python\Lib\site-packages`*    | ❌           |
| `BIP_DCC`               | Python 3.7 virtual environment                                     | directory path | *`~\AppData\Local\Blink\dcc`*                         | ✅           |
| `BIP_DCC_LIBS`          | Python 3.7 site-packages folder                                    | directory path | *`~\AppData\Local\Blink\dcc\Lib\site-packages`*       | ❌           |
| `BIP_VERSION`           | Current Bip version (from `__init__.py`)                           | version number | *`3.1.5`*                                             | ❌           |
| `BIP_INTEGRATIONS`      | Bip integration scripts (mainly for embedding in DCC)              | directory path | *`~\AppData\Local\Blink\bip\bip\integrations`*        | ❌           |
| `BIP_ICONS`             | Bip applications icons                                             | directory path | *`~\AppData\Local\Blink\bip\bip\assets\images\icons`* | ❌           |
| `BIP_WORKING_DIRECTORY` | Working directory defined in the system config (see `BIP_CONFIG`)  | directory path | *`\\network\location\where\you\work`*                 | ❌           |
| `BIP_CONTENT`           | Content repository defined in the system config (see `BIP_CONFIG`) | directory path | *`~\AppData\Local\Blink\content`*                     | ✅           |
| `BIP_CONFIG`            | Local system config file                                           | file path      | *`~\AppData\Local\Blink\bip\bip\config.yml`*          | ✅           |
| `BIP_USER_DIRECTORY`    | Bip user folder                                                    | directory path | *`~\.blink`*                                          | ✅           |
| `BIP_AUTH`              | Authentication file (current username and password hash)           | file path      | *`~\.blink\auth.yml`*                                 | ✅           |
| `BIP_CURRENT_BRANCH`    | Current Git branch of the Client repository                        | str            | *`develop`, `master`*                                 | ❌           |
| `BIP_CURRENT_USER`      | Username of the current user                                       | str            | *`firstname.lastname`*                                | ❌           |
| `BIP_ACTIVE_PROJECT`    | Slug of the currently active project                               | str            | *`my-project`*                                        | ❌           |