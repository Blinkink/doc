---
title: My settings
sidebar_position: 3
---
## Your account
### Profile
![image](../assets/settings_profile.png)

### Project
![image](../assets/settings_project.png)

### Password
![image](../assets/settings_password.png)

### Preferences
![image](../assets/settings_preferences.png)

## This computer
### Version
![image](../assets/settings_version.png)

### Database
![image](../assets/settings_database.png)

### Config
![image](../assets/settings_config.png)