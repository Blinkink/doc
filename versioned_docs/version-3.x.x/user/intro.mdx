---
title: Introduction
sidebar_position: 1
---

export const Highlight = ({children, color}) => (  <span    style={{      backgroundColor: color,      borderRadius: '8px',      color: '#fff',      padding: '0.3rem',    }}>    {children}  </span>);

## What is Bip ?
Bip (**B**link**i**nk **p**ipeline) is a **collection of tools or applications for CG productions**, as well as a **production database**. For advanced users such as technical directors and developers, it is also a framework allowing to enhance Bip with additional content.

Bip **does not enforce any workflow choices**, it is left to the advanced users to configure and tailor it, so it matches the needs and practices of a studio, a director and/or a project.

:::info
See the **[TD (Technical director) documentation](../td/intro)** if you want to learn how to customize Bip by creating _tasks_, _batches_, _commands_...
:::

On the user side, Bip mostly consists of applications for artists and production. These tools can be **standalone** (started from the main Launcher app), or they can be **embedded** in supported software (such as Modo or Nuke. _See the list of [integrations](#integrations)_).

Some embedded applications can also modify the behaviour of their host application (for example, override the `Ctrl+s` shortcuts in order to use the Bip Saver)

## Launcher

One Bip application is particularly central in Bip's ecosystem: the **Launcher**.


The Launcher is the starting point for any user. Bip apps can be executed from here, but also _batches_ or _commands_ written by the technical directors or administrators.
- **Batches** are used to start your favorite CG applications while following a pre-defined scenario, such as installing add-ons, tweaking configuration files...
- **Commands** allow to execute Bip commands in a user-friendly way.


## Data
Bip is not only a bunch of applications, it also records **production data**.
- **Basics**: Projects, items (_e.g. assets, shots..._), groups (_e.g. sequences, asset categories...), tasks, users...
- **Files**: Any type can be tracked (_e.g. scenes, renders, textures, data..._) by iteration (versioning system), with dynamic path management.
- **Relationships**: It also silently keep records of dependencies (_who created what from what, what item has been imported where and when..._).
- **Custom metadata**: It can track any type of custom data thanks to a highly customizable metadata system.

In order to fetch, sort, filter and deliver the data fast enough, the Bip server is using a _graph database_ engine, Neo4j, meaning that data is stored as connected nodes, rather than rows in a table.

![database_nodes](../assets/database_nodes.png)

## Server
Bip relies on a **web-hosted server**, `Bop`, which hosts and provides production data and secured authentication.

:::danger Requirement
**You need an internet connection** in order to use Bip.
:::

## Features
### Artists and production
- Create and manage projects, assets, shots, documents, edits... (defined by your workflow templates).
- Save and open scenes from your favourite CG applications without having to think about the path.
- Control your scenes with your own tasks for exporting, importing or updating assets.
- Automate your render submission without having to think about the path.
- Convert images sequences into movies and publish them.
- Monitor your licenses usage.
- Start your CG applications with the right addons and settings.
- Ingest external files, with path and naming convention compliance.

### Technical
- **Batches**: Start and customize your favourite applications automatically by writing batches.
- **Tracking**: Record basic production data, dependency relationships, custom metadata...
- **Versioning**: Keep track of all the iterations of a component (files).
- **Commands**: Write powerful commands that can be executed from the Launcher.
- **Tasks**: Easily write tasks for your CG applications. Bip provides a simple framework that allow you to focus on the operations, while the data is provided and recorded automatically.
- **Apps**: Bip comes with a collection of default apps covering the essential needs of a CG pipeline.
- **Plugins**: Write plugins in order to extend Bip to your favourite applications.
- **Builtins**: Some plugins are bundled with Bip, bringing support for Modo, Nuke or Shotgrid (_see the [integrations](#integrations) section for more details_)
- **Ingestion**: Ingest external files, with path and naming convention compliance.
- **Path management**: Automatically generate dynamic paths from a folder structure template.
- **Authentication**: Secured login system for the users.
- **Updates**: Self-update system, with per-project version locks, as well as a rollback system.
- **Branch switcher**: Bip allows the users to switch branches and versions of their current installation. Useful for deploying beta features without affecting the whole team for example.
- **Agnostic**: Bip does not enforce any workflow or DCC.
- **Templates**: The workflow is defined via templates and presets by the technical directors and administrators.
- **Customization**: The studio logic (naming conventions, workflow decisions, available apps...) is defined on a separate area, which allow to tweak Bip's behaviour to make it match your practices.
- **API**: Bip comes with an API that you can use from your own scripts for retrieving Bip data, or create your whole client.
- **Toolkit**: An SDK allows you to create quickly your own BipApp.

### Apps


## Requirement
- Internet connection
- Windows 10

## Integrations
| ![nuke](../assets/logos/nuke.png) | ![maya](../assets/logos/maya.png) | ![shotgrid](../assets/logos/shotgrid.png) |
| ------------------------------------------------------------ | ------------------------------------------------------------ | ------------------------------------------------------------ |
|                                       **Nuke**<br /><Highlight color="#5693f2">WIP</Highlight>                       |               **Maya**<br /><Highlight color="#5693f2">WIP</Highlight>                                               |               **Shotgrid**<br /><Highlight color="#5693f2">WIP</Highlight>                                               |

## Installation
:::danger
Bip is at an early development stage. It is made **by** and **for** Blinkink. Installing Bip is at your own risk.
See the [disclaimer](#disclaimer) section below for more information.
:::

- For installing the **Bip client**, follow the instructions in the [client installation guide](../admin/client/install/windows)**.
- For installing your own **Bip server** instance, follow the instructions in th[server installation guide](../admin/server/install).

## Development
Bip is crafted with ❤️ by **[Blinkink](https://blinkink.co.uk)**, a London based animation studio.

It is built in **[Python](https://www.python.org/)**, the UI is made with **[Pyside](https://wiki.qt.io/Qt_for_Python)**, the data is managed with **[Neo4j](https://neo4j.com/)** and is tracked with **[Gitlab](https://about.gitlab.com/)**.

![dev_logos_banner](../assets/logos/dev_logos_banner.png)

:::info
- See the **[Developer documentation](../dev/overview/intro)** if you want to learn how to use Bip's API, SDK and builtin plugins.
- See our **[Gitlab instance](https://git.blinkink.co.uk/bip)** to follow the development or submit an issue.
:::

## Friends
The Launcher application, which is the Bip client backbone, massively relies on [**JeanPaulStart**](https://github.com/cube-creative/jeanpaulstart), developed with ❤️ by our friends from **[Cube](http://www.cube-creative.com/)**.

## Licence
Bip is an open source project, distributed under the **[MIT license](https://mit-license.org/)**.

![licenses](../assets/logos/licenses.png)

## Disclaimer
- Bip is under **active development**, updates are very frequent.
- We do not guarantee **stability**.
- **Retro-compatibility** is not a priority yet.
- It is developed **by** and **for** Blinkink, whose priorities define the Bip roadmap.
- It is not ready for public use until **v3.0.0**.
- Blinkink cannot provide support for this product.