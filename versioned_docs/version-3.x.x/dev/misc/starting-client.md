---
title: Client startup
---


## Process
- The Windows shortcut execute a silenced (with invisible.vbs) batch (`bip/scripts/windows/start.bat`)
- The batch activates the Python 3.8 venv and executes the entry script (`bip/scripts/windows/entry.py`)
- The entry script adds **bip** to the `PYTHONPATH` and starts the launcher application


## Shortcut
- **Command**: `wscript.exe invisible.vbs start.bat`
- **Start in**: `%userprofile%\AppData\Local\Blink\bip\scripts\windows\`