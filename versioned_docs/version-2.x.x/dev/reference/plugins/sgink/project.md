---
sidebar_label: project
title: bip.plugins.sgink.project
---

## active\_project

```python
def active_project()
```

Get a Shotgrid project from the current Bip user&#x27;s active project.

**Returns**:

- `dict` - A dictionary with the Shotgrid Project id and type.

## from\_bip

```python
def from_bip(project)
```

Get a Shotgrid project from a Bip item.

The project match is done with the code and the name:
- _Name_: The Shotgrid name must be formatted like
`XXX000 Project Name` (this is **case-sensitive**).
- _Code_: The Shotgrid code must be formatted like
`XXX000` and must be matching the Bip project prefix.

**Arguments**:

- `project` _Project_ - Project BipObject.
  

**Returns**:

- `dict` - A dictionary with the Shotgrid Project id, type, code and name.

