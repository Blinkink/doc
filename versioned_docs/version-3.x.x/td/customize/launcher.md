---
title: Launcher
---

## Help message
:::tip What?
Using the `help` command can display a custom message.
:::

![](./assets/launcher-help.png)

### How to
- Create a `help.html` file in your content repository: `custom/launcher/help_message`
- _Optional_: if you need custom styling, you can also add a `style.css` file.
- _Optional_: you can use **special keywords** (`$`) for links. ([see below](#special-variables))
- _Optional_: you can use **special anchors** (`#`) for triggering UI actions. ([see below](#special-variables))

### Example
```html
<h2>A bit lost?</h2>
<p>
    You can read <a href="$QUICK_START">how to use our pipeline</a>,
    or have a look at the <a href="#WELCOME_TUTORIAL">welcome tutorial</a>.
    If you need more information about your project,
    here is its <a href="$PROJECT_DOCUMENTATION">documentation</a>,
    and here is its <a href="#PROJECT_FOLDER">folder</a>!
</p>

<h2>Or do you have a question?</h2>
<p>
    Depending on the topic, you can ask your <a href="$MESSAGE_LEAD">lead</a>
    or your <a href="$MESSAGE_PRODUCER">producer.</a>
    If this is more of a technical matter, you can ask an <a href="$MESSAGE_IT">IT person</a>
    or a <a href="$MESSAGE_PIPELINE">pipeline developer?</a><br>
    If you found a bug you can even <a href="#TECH_CENTER">report it</a>!
</p>
```

## Welcome message
:::tip What?
Using the `welcome` command can display a custom slideshow.
:::

![](./assets/launcher-welcome.png)

### How to
- Create one or several files in your content repository: `custom/launcher/welcome_message`
- The files will be sorted alphabetically. You can call them `01.html`, `02.html`...
- _Optional_: if you need custom styling, you can also add a `style.css` file.
- _Optional_: if you need images, you can add them in an `images` directory next to the files.
- _Optional_: you can use **special keywords** (`$`) for links. ([see below](#special-variables))
- _Optional_: you can use **special anchors** (`#`) for triggering UI actions. ([see below](#special-variables))


### Example
#### `01.html`
```html
<p>Welcome at our <b>Company</b>.</p>
<p><img src="images/logo.png"></p>
```

#### `02.html`
```html
<p>Once started, <b>the Launcher always runs in the background</b>. You can call it using this shortcut:</p>
<p id="bip_shortcut">$LAUNCHER_SHORTCUT</p>
```

#### `style.css`
```css
#bip_shortcut {
    color: white;
    font-size: 48px;
}
```

## Tips
:::tip What?
Custom tips can be displayed randomly in the Launcher footer (if the user allows it)
:::

![](./assets/launcher-tips.png)

### How to
- Create a `tips.yml` file in your content repository: `custom/launcher`
- Populate it with a list of short sentences
- You can use html basic formatting
- You can use `<a></a>` for adding links
- Use double-quotes if your sentence contains `:`, and escape symbol `\` if you have double-quotes.

### Example
```yml
- Bip doesn't have bugs, it has unexpected features
- You can follow Bip development <a href="https://git.blinkink.co.uk/bip/client/-/issues">here</a>
- <i>(Windows)</i> Use "Windows+E" to open the file explorer quickly
- "You can type non-continuous searches for faster result: \"pdir\" will execute \"project directory\""
```

## Special variables
### Keywords
- `$PROJECT_DOCUMENTATION`: Replace with the custom URL (if defined in the [config](../config))
- `$QUICK_START`: Replace with the custom URL (if defined in the [config](../config))
- `$LAUNCHER_SHORTCUT`: Replace with the Launcher shortcut binding
- `$MESSAGE_IT`: Replace with the user messaging URL (if the current project has an assigned IT contact)
- `$MESSAGE_PIPELINE`: Replace with the user messaging URL (if the current project has an assigned pipeline contact)
- `$MESSAGE_PRODUCER`: Replace with the user messaging URL (if the current project has an assigned producer contact)
- `$MESSAGE_LEAD`: Replace with the user messaging URL (if the current project has an assigned lead contact)

### Anchors
:::caution
Only works if used as `href` parameter for `<a>` hypertext links
:::
- `#PROJECT_FOLDER`: Open the current project root directory in the explorer
- `#WELCOME_TUTORIAL`: Switch to the Launcher welcome view
- ~~`#TECH_CENTER`: Open the Tech Center app (_not implemented_)~~