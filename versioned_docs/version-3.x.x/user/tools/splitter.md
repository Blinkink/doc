---
title: Splitter
---

# Splitter

## What is the Splitter?

This is a tool to split a previs scene file into individual shot scene files based upon the names of the cameras found in the camera sequencer.

It can be temporarily found on the Bip Toolbox Shelf for testing purposes, and thereafter on the BIP shelf

## Features

* Find all cameras in the scene, by searching the **CAMERAS** folder for groups ending in **_GRP**
* Display list of these cameras, along with their start and end frames, as found in the **Camera Sequencer**
* For each camera a scene file will be created. Prior to this the splitter will:
  * Remove all other Cameras from the scene
  * Move all keyframes of the chosen camera and it's controllers so that the start frame is now 1001
  * Move all keyframes of any controllers **(nurbs curves or locators ending in "_CTL")** found in the **ANIMATED** folder so that the start frame is now 1001
  * Offset the image plane of the camera to retain the correct frame range
  * Offset and trim the audio to retain the correct frame range
  * Save the scene to the correct file_path

## Naming Conventions 

* Each Camera in your scene should use the following naming conventions:
* 
  * "[**Episode**]_[**SequenceNumber**]\_[**SequenceName**]_SH[**ShotNumber**]_GRP"
    * "[**Episode**]_[**SequenceNumber**]\_[**SequenceName**]_SH[**ShotNumber**]_World_CTL"
      * "[**Episode**]_[**SequenceNumber**]\_[**SequenceName**]_SH[**ShotNumber**]_Local_CTL"
        * "[**Episode**]_[**SequenceNumber**]\_[**SequenceName**]_SH[**ShotNumber**]_CAM"
      
* For Example:
  * ENK101_Seq050_Leaving_SH0595_GRP
    * ENK101_Seq050_Leaving_SH0595_World_CTL
      * ENK101_Seq050_Leaving_SH0595_Local_CTL
        * ENK101_Seq050_Leaving_SH0595_CAM

## Tips

:::tip

* If the correct cameras and frame ranges are not displayed, please check you are following the correct naming conventions
* The create Camera has been updated to follow this convention - Just enter the shot number when creating a new camera <img src={require('./assets/basic_camera.png').default} width="32" />
* Please also check you have the correct folders in your scene. Folders can be generated using create folders in the Bip Toolbox <img src={require('./assets/folder.png').default} width="32" />
* If you still have errors please check with Klaas (Product Owner) or Lisa (Developer)

:::