---
title: Future
sidebar_position: 5
---
## Client
- To be a background _deamon_, detached from the Launcher application
- `link` to be a simple object wrapper for server's `sink` API.
- Apps to be handled as addons, individually enabled and updated.

## Server
- New data API (`sink`), RESTful
- New filesystem agent (`kink`), allowing to execute centralized operations on the working directory.
- Workflow model from templates
  
## Misc
- Rustlang support
- Automated unit tests