---
sidebar_label: user
title: bip.link.user
---

## `User`

```python
class User(BipObject)
```

User Bip object.

**Attributes**:

- `uid` _str_ - Bip unique identifier (uuid4).
  Edition is forbidden if this is an existing (recorded in database) entity.
- `slug` _str_ - Human-readable unique identifier.
- `first_name` _str_ - First name of the user.
- `last_name` _str_ - Last name of the user.
- `username` _str_ - The username is used for logging into Bip.
- `password` _str_ - Hashed password.
- `email` _str_ - Email address of the user.
- `messaging_link` _str_ - Messaging url.
  It can be a direct messaging url from Slack, Rocket.Chat, Mattermost...
- `initials` _str_ - Two capital letters (`XX`).
  Convenient identifier that can be used in filenames for instance.
- `active` _bool_ - True if active, False if inactive user.
- `last_announcement_seen` _str_ - Identifier of the last announcement seen (uuid4).
- `is_new` _bool_ - Helper value for determining if a user has never used their account yet.
- `last_tip_day` _str_ - Date when the last tip has been seen (`%d/%m/%Y`).
- `role` _Role_ - Bip role object.
- `privilege` _privilege_ - Bip privilege object.

### `deleted`

```python
@property
def deleted() -> bool
```

Is the user a ghost data.

**Returns**:

- `bool` - True is the user is a ghost (kept for database consistency), False if the user is enabled.

### `cached_project_uid`

```python
@property
def cached_project_uid() -> str
```

Active project uid.

This value is cached when the User object is retrieved, since the active
project must be gotten from an additional and expensive request.

Helper property for internal needs (avoids massive queries).

**Returns**:

- `str` - Bip unique identifier (uuid4).

### `last_version_used`

```python
@property
def last_version_used()
```

Last version of Bip used.

**Returns**:

- `str` - Formatted like `vX.X.X`.

### `last_hostname`

```python
@property
def last_hostname()
```

str: Machine hostname where the user has used Bip for the last time.

### `last_seen`

```python
@property
def last_seen()
```

str: Date and time when the user has used Bip for the last time (%d/%m/%Y %H:%M:%S).

### `recent_connections`

```python
@property
def recent_connections()
```

list: List of data from the last ten connections.

Each dict has the following keys:
- `hostname` (str): Machine
- `date` (str): Date and time (%d/%m/%Y %H:%M:%S)
- `version` (str): Bip version (vX.X.X)

### `full_name`

```python
@property
def full_name()
```

str: space-joined first name and last name.

### `save`

```python
def save()
```

Saves a `User` object.

If the object is not floating (exists in the database)
and has not been modified, nothing will be done.

If floating, the uid is automatically generated.

If not specified, the unique slug is automatically
generated from the name.

**Raises**:

  ValueError:
  - The first name is empty.
  - The last name is empty.
  - The username contains illegal characters.
  - The username already exists.
  - The role is not set.
  - The privilege is not set.
  - The initials are empty.
  - The initials are not in capital.
  - The initials are not two-letters long.

### `delete`

```python
def delete()
```

Deletes a user.

**Notes**:

  Bip does not delete users in order to keep existing
  data relationship consistent. Instead, the user is
  marked as deleted. The username becomes available again.

### `add_recent_connection`

```python
def add_recent_connection(hostname, date, version)
```

Adds a recent connection.

**Arguments**:

- `hostname` _str_ - Machine name.
- `date` _str_ - Date and time. Must be formatted like: `%d/%m/%Y %H:%M:%S`.
- `version` _str_ - Bip version. Must be formatted like: `vX.X.X`.
  

**Todo**:

  - Check date formatting
  - Check version formatting

### `get_active_project`

```python
def get_active_project()
```

Gets the user's active project.

**Notes**:

  Object method for `link.user.get_active_project(user)`.

### `set_active_project`

```python
def set_active_project(project)
```

Sets the user's active project.

**Notes**:

  Object method for `link.user.set_active_project(user, project)`.

### `clear_active_project`

```python
def clear_active_project()
```

Clears the user's active project.

**Notes**:

  Object method for `link.user.clear_active_project(user)`.

### `set_password`

```python
def set_password(clear_password)
```

Sets a hashed and secured password from a clear string.

**Arguments**:

- `clear_password` _str_ - Non-hashed password

### `get`

```python
@classmethod
def get(cls, username=None, active_only=False)
```

Gets all users or gets one user by username.

If username is left to None, all users are returned.

**Arguments**:

- `username` _str, optional_ - Looked up username. Defaults to `None`.
- `active_only` _bool_ - Show only active users. Defaults to `False`.
  

**Returns**:

- `list` - If `username` is not set, list of `User` objects.
- `User` - If `username is set, and a match has bee found, a user object.
- `bool` - False otherwise.

## `Privilege`

```python
class Privilege(BipObject)
```

Privilege Bip object.

Privileges are used for handling permissions within applications.

**Attributes**:

- `uid` _str_ - Bip unique identifier (uuid4).
  Edition is forbidden if this is an existing (recorded in database) entity.
- `slug` _str_ - Human-readable unique identifier.
- `name` _str_ - Name of the privilege.

### `save`

```python
def save()
```

Saves a `Privilege` object.

If the object is not floating (exists in the database)
and has not been modified, nothing will be done.

If floating, the uid is automatically generated.

If not specified, the unique slug is automatically
generated from the name.

**Raises**:

  ValueError:
  - The name is empty.
  - The slug already exists.

## `Role`

```python
class Role(BipObject)
```

Role Bip object.

**Attributes**:

- `uid` _str_ - Bip unique identifier (uuid4).
  Edition if forbidden is this is an existing (recorded in database) entity.
- `slug` _str_ - Human-readable unique identifier.
- `name` _str_ - Name of the role.
- `plural` _str_ - Plural of the role.

### `save`

```python
def save()
```

Saves a `Role` object.

If the object is not floating (exists in the database)
and has not been modified, nothing will be done.

If floating, the uid is automatically generated.

If not specified, the unique slug is automatically
generated from the name.

**Raises**:

  ValueError:
  - The name is empty.
  - The slug already exists.

## `get`

```python
def get(username=None, active_only=False)
```

Gets all users or gets one user by username.

If username is left to None, all users are returned.

**Arguments**:

- `username` _str, optional_ - Looked up username. Defaults to `None`.
- `active_only` _bool_ - Show only active users. Defaults to `False`.
  

**Returns**:

- `list` - If `username` is not set, list of `User` objects.
- `User` - If `username is set, and a match has bee found, a user object.
- `bool` - False otherwise.

## `get_by_username`

```python
def get_by_username(username)
```

Get user by username.

**Arguments**:

- `username` _str_ - Looked up username.
  

**Returns**:

- `User` - If a match has been found, a user object.
- `bool` - False otherwise.

## `get_current`

```python
def get_current()
```

Gets the currently logged in user.

The credentials validity is automatically checked.

**Returns**:

- `User` - A user object if successful.
- `bool` - False otherwise.

## `get_current_user`

```python
def get_current_user()
```

Alias for `link.user.get_current()`

## `get_privileges`

```python
def get_privileges()
```

Get all privileges.

But don't forget to seize all the means of production. ✊

**Returns**:

- `list` - Collection of `Privilege` objects.

## `get_privilege`

```python
def get_privilege(slug=None, uid=None)
```

Get a privilege by name or uid.

At least one parameter (slug or uid) must be specified.
If both are specified, the slug is used.

**Arguments**:

- `slug` _str_ - Looked-up slug.
- `uid` _str_ - Looked-up uid.
  

**Returns**:

- `Privilege` - A privilege object if successful.
- `bool` - False otherwise.
  

**Raises**:

- `ValueError` - If no slug nor uid are specified.

## `get_roles`

```python
def get_roles()
```

Get all roles.

**Returns**:

- `list` - Collection of `Role` objects.

## `get_role`

```python
def get_role(slug=None, uid=None)
```

Get a role by name or uid.

At least one parameter (slug or uid) must be specified.
If both are specified, the slug is used.

**Arguments**:

- `slug` _str_ - Looked-up slug.
- `uid` _str_ - Looked-up uid.
  

**Returns**:

- `Role` - A role object if successful.
- `bool` - False otherwise.
  

**Raises**:

- `ValueError` - If no slug nor uid are specified.

## `new`

```python
def new(username, password, first_name, last_name, privilege, role, initials=None, auto_save=True)
```

Creates a user and saves it.

**Arguments**:

- `username` _str_ - The username is used for logging into Bip.
  Must only contain alphanumerical characters, dashes, dots and underscores.
- `password` _str_ - Clear password.
- `first_name` _str_ - First name of the user.
- `last_name` _str_ - Last name of the user.
- `privilege` _Privilege_ - Bip privilege object.
- `role` _Role_ - Bip role object.
- `initials` _str_ - Two capital letters.
  

**Returns**:

- `User` - The generated user.

## `new_user`

```python
def new_user(username, password, first_name, last_name, privilege, role, initials=None, auto_save=True)
```

Convenience alias for `link.user.new()`

## `new_role`

```python
def new_role(name, plural=None, slug=None, auto_save=True)
```

Creates a role and saves it.

**Arguments**:

- `name` _str_ - The name of the role.
- `plural` _str, optional_ - If unspecified, a "s" is added to the name.
- `slug` _str, optional_ - If unspecified, the slug is generated automatically from the name.
  

**Returns**:

- `Role` - The generated role.

## `new_privilege`

```python
def new_privilege(name, slug=None, auto_save=True)
```

Creates a privilege and saves it.

**Arguments**:

- `name` _str_ - The name of the privilege.
- `slug` _str, optional_ - If unspecified, the slug is generated automatically from the name.
  

**Returns**:

- `Privilege` - The generated privilege.

## `set_active_project`

```python
def set_active_project(user, project)
```

Sets the active project of a user.

Active project is the default/current user's project.

**Arguments**:

- `user` _User_ - User that must have their active project changed.
- `project` _Project_ - New active project.

## `clear_active_project`

```python
def clear_active_project(user)
```

Clears the active project of a user.

**Arguments**:

- `user` _User_ - User that must have their active project cleared.

## `get_active_project`

```python
def get_active_project(user)
```

Get the active project of a user.

**Arguments**:

- `user` _User_ - User that must have their active project retrieved.
  

**Returns**:

- `Project` - Active project.
- `bool` - False if no active project is set for this user.

## `reset_announcement_views`

```python
def reset_announcement_views()
```

Resets the announcement views.

If an announcement is currently active, set it
as _unseen_ for all of the users, even if they have seen it
already.

## `clear_announcement_views`

```python
def clear_announcement_views()
```

Clears the announcement views.

If an announcement is currently active, set it
as _seen_ for all of the users, even if they have
not seen it already.

## `force_welcome_tutorial`

```python
def force_welcome_tutorial()
```

Forces to display the welcome tutorial for all users.

Sets the `is_new` attribute of all users to `True`, which would
triggers the "newcomers" procedures in the various Bip applications.

## `get_all_project_referents`

```python
def get_all_project_referents(project)
```

Get all the referents of a given project.

Referents are users who have a specific role in a project.

The referent scopes can be:
- IT
- Lead
- Production
- Pipeline

**Arguments**:

- `project` _Project_ - A Bip project.
  

**Returns**:

- `dict` - pairs of referent type (`str`) with user (`User').

## `get_project_referent`

```python
def get_project_referent(project, referent_type)
```

Gets a project referent per type.

**Arguments**:

- `project` _Project_ - Project to get the referent from.
- `referent_type` _str_ - Targeted type.
  Value can be:
  - `it`
  - `production`
  - `lead`
  - `pipeline`
  

**Returns**:

  - User: Found referent if defined.
  - bool: False otherwise.

## `set_project_referent`

```python
def set_project_referent(project, user, referent_type)
```

Sets a project referent.

**Arguments**:

- `project` _Project_ - Project to get the referent from.
- `user` _User_ - Assigned user.
- `referent_type` _str_ - Targeted type.
  Value can be:
  - `it`
  - `production`
  - `lead`
  - `pipeline`

