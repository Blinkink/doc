---
sidebar_label: config
title: bip.link.config
---

## `Config`

```python
class Config(BipObject)
```

Server Config Bip object.

The server config should be created with `link.specials.setup()` in order to get
the best default values.

**Attributes**:

- `uid` _str_ - Bip unique identifier (uuid4).
  Edition is forbidden if this is an existing (recorded in database) entity.
- `slug` _str_ - Human-readable unique identifier.
  Since the Config is a singleton (only one per Bip instance), the slug is always `server-config`.
- `bip_source` _str_ - Url to the Git repository of Bip. *The repository must be public.*
  Used for updates. Unless you want to use you own fork of Bip, you should
  be using the official repository url (https://git.blinkink.co.uk/bip/client.git)
- `content_source` _str_ - Url to your Git repository for content. *The repository must be public.*
- `minimal_version` _str_ - The minimal version allowed for the Bip instance clients.
  Must be `latest` or a valid Git tag name, in the semver format (vX.X.X).
- `recommended_version` _str_ - The minimal version allowed for the Bip instance clients.
  Must be `latest` or a valid Git tag name, in the semver format (vX.X.X).
- `enable_announcement` _bool, optional_ - If `True`, an announcement is shown to all clients through the Launcher app.
- `announcement_message` _str, optional_ - If `enable_announcement` is set to `True`, the message that will be displayed.
- `announcement_uid` _str, optional_ - Announcement unique identifier (uuid4).
  Useful for determining if an announcement has been seen by a user.
  This is automatically generated if the announcement has been modified.
- `announcement_level` _str, optional_ - If `enable_announcement` is set to `True`, the level of the announcement.
  Value can be:
  - _"info"_ (`wink.INFO`)
  - _"error"_ (`wink.ERROR`)
  - _"warning"_ (`wink.WARNING`)
- `closable_announcement` _bool, optional_ - If `enable_announcement` is set to `True`, defines if the announcement can be dismissed by users.
- `active_project_timeout` _int, optional_ - In days, defines after how many days of inactivity the active project of a user is cleared (set to none).
- `default_tracker` _str, optional_ - Default production tracker.
  The chosen tracker must be enabled in the server config (see `internal_enabled` and `shotgrid_enabled`)
  Value can be:
  - _"internal"_ (`bip.plugins.INTERNAL`)
  - _"shotgrid"_ (`bip.plugins.SHOTGRID`)
- `internal_enabled` _bool, optional_ - If `True`, the internal tracker can be used by any project.
- `shotgrid_enabled` _bool, optional_ - If `True`, the Shotgrid tracker can be used by any project.
  The plugin must be enabled.
- `shotgrid_server` _str, optional_ - If the Shotgrid tracker is enabled, url to the Shotgrid instance.
- `shotgrid_script_name` _str, optional_ - If the Shotgrid tracker is enabled, API script name.
- `shotgrid_script_key` _str, optional_ - If the Shotgrid tracker is enabled, API script key.

### `save`

```python
def save()
```

Saves a `Config` object.

If the object is not floating (exists in the database)
and has not been modified, nothing will be done.

If floating, the uid is automatically generated.

If not specified, the unique slug is automatically
generated from the name.

**Raises**:

  ValueError:
  - bip_source is not a valid (or public) Git repository.
  - content_source is not a valid (or public) Git repository.
  - announcement_level is invalid.
  - minimal_version is not "latest" and is not found in the `bip_source` tags.
  - recommended_version is not "latest" and is not found in the `bip_source` tags.
  - minimal_version is greater than recommended_version.
  - default_tracker is set to "internal" but internal_enabled is false.
  - default_tracker is set to "shotgrid" but shotgrid_enabled is false.
  - shotgrid_enabled is set to true, but shotgrid_server, shotgrid_script_name or shotgrid_script_key are not set.

### `enabled_trackers`

```python
@property
def enabled_trackers()
```

All enabled production trackers.

**Returns**:

- `list` - List of enabled production trackers ids.

### `is_announcement_modified`

```python
@property
def is_announcement_modified()
```

Tells if any announcement setting has been modified.

**Returns**:

- `bool` - True if modified, False otherwise.

### `clear_announcement`

```python
def clear_announcement()
```

Deletes the current announcement if any

## `get`

```python
def get(autogen=True)
```

Gets the current Bip instance server config.

**Arguments**:

- `autogen` _bool, optional_ - If set to true, the getter automatically returns an empty Config
  

**Returns**:

- `Config` - If found and/or if not found but `autogen` is set to true.
- `bool` - If not found and `autogen` is set to false.

