---
title: Ingestion
---
:::caution
The Ingestor application and its API are work in progress. Some features are missing:
- `{version}` variable not implemented
- `{task}` variable not implement
:::
  

## General
Ingestion presets define how an untracked file or fileset can be ingested to your Bip instance.
Presets are per file types.

Presets can be available for all projects or project-specific. If both are found, the project-specific is used.

## Location
:::info
The files must live in your content repository
:::
- Global: `presets/ingestion/global/[preset_name].yml`
- Project-specific: `presets/ingestion/[project_folder_name]/[preset_name].yml`

## Tenplate
### Mandatory parameters
```yml
name: Example
file_type: file-type-slug
allowed_extensions:
  - ext
patterns:
  my_pattern:
    name: My Pattern
    expression: "*"
```

### All parameters
```yml
name: Example
default: False
visible: False
file_type: file-type-slug
allowed_items:
  - shot
  - asset
allowed_extensions:
- ext
patterns:
  my_pattern:
    name: My Pattern
    expression: "{item}_{variant}_{frame:4}"
default_pattern: my_pattern
version:
  editable: False
  visible: False
  default: increment # "increment" | "latest" | int | None
variant:
  editable: True
  visible: True
  default: None
  allow_custom: True
  force_reformat: False
  case: None # "upper_camel" | "lower_camel" | None
  auto_select_if_single: False
item:
  editable: True
  visible: True
  default: None
task:
  editable: True
  visible: True
  default: None
  task_auto_select_if_single: False
```

## Pattern syntax
### Elements
- **Variables**: Special keywords used for extracting useful data, such as item name, variant or frames.
- **Wildcards**: Allows undetermined characters. Can be used at the beginning or the end of an expression only.
- **Spacers**: Expects specific characters.

### Variables
:::tip Definition
Variables are between brackets {} and can have flags (at the beginning) and parameters (at the end).
:::

#### Keys
- **`{item}`**: Looks for an item folder name. The allowed item tags are defined by the preset (shot, asset...)
- **`{frame}`**: Looks for a frame number. Padding **must** be specified as a parameter
- **`{variant}`**: Looks for a variant folder name. Floating flag can be used.
- ~~**`{version}`**: Looks for a version number. Floating flag can be used. Pattern must be specified as a parameter **(Not implemented yet)**~~
- ~~**`{task}`**: Looks for a task folder name. Floating flag can be used. **(Not implemented yet)**~~

#### Flags
:::tip Definition
Single special character at the begining of the variable name
:::

- **Floating**: `?` If the variable can not be  found in the database, the raw string found will be kept and the entity  (task, variant...) will be created on the fly at ingestion.

#### Parameters

:::tip Definition
IInteger or string at the end of the variable name, using `:` as a separator
:::

- **Padding**: Specify digits length. For {frame} only. *Example*: `{frame:4}`
- **Format**: Specify a subpattern. For {version} only. *Example*: `{version:v###}`

### Examples
- `{item}_{?variant}_{frame:4}`
- `{item}_{?variant}_{?version:v###}_{frame:4}`
- `{item}*`
- `*EXPORT_{variant}_SHOT{item}*`
