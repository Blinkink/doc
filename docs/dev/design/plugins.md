---
title: Plugins
sidebar_position: 4
---

:::info Looking for?
- A complete reference for plugins classes, `App`, `Tracker`, `Host` and `Submiter`, check the **[SDK](/dev/reference/sdk/app.md)**.
- How to create a plugin, check the **[guide](/dev/guides/plugins.md)**.
- How to set up your environment for plugin development, check the **[development](/dev/development/plugins.md)**.
- How to manage plugins in your instance, check the **[TD documentation](/td/plugins.md)**.
:::

## Introduction
Plugins are for third-party developers to extend Bip with their own needs while staying integrated to Bip. A plugin for instance be an GUI application, or a host bringing implementing a DCC.

Plugins are created by subclassing class templates that can be found in the SDK (`bip.sdk`). They are designed to provide a standardized set of methods and attributes, in order to comply with the Bip design guidelines, particularly regarding high modularity and agnosticity of elements in Bip.

Bip without plugins is just a core API. Therefore, Bip ships a set of plugins by default, in particular some GUI applications handling the most common functionalities. The launcher (Brioche), updater (Bugne) or manager (Clafoutis) applications are some of these builtins plugins. Their usage is recommended, but not mandatory, and it is possible to seamlessly switch them with your own applications.

## Features
Using the plugin template to extend Bip allows to natively delegate and unify some tedious tasks, so the developer can focus on features for artists.

- **Deployment**: Plugins installation and update is handled by the core API (`mink`) and executed by the updater (_Bugne_ by default).
- **Version control**: Plugins are version-locked.
- **Per-project ecosystem**: Plugins can be enabled and version locked per project (if they are not global project).
- **Dependencies**: Plugins can require other plugins.
- **Integrations**: Using `jeanpaulstart` batches, plugins can be integrated to the Launcher or into a host batch (e.g. load a Bip application into Maya).
- **Config**: Plugins can describe a configuration template, where values can be set by the instance administrators and stored in the database.
- **User settings**: Plugins can describe a settings template, where values can be individually set by each user and stored in the database.
- **Changelogs**: Following a standardized changelog file format, plugins changes throughout versions can be tracked and shown to the relevant users.
- **Entity mapping**: Since each project can have a different data model, with custom entities, it is possible to map those entities to their corresponding plugin hook (e.g. if a plugin needs to manipulate _scenes_, it is possible to map the ComponentTag corresponding to that concept, without hardcoding workflow concepts into the plugin.)
- **API compatibility**: The plugin API can check and track if a plugin is compatible with the current Bip instance API version.

## Types
There are two types of plugins:
- **Applications**: GUI frontends.
- **Handlers**: Third-party APIs wrappers.

### Application

An application is a GUI plugin integrated in Bip environment and preloaded with a certain amount of features such as:
- **Error catching**: Top-level error catching, with automatic logging and incident report.
- **Unified logging**: Use the Bip centralized logging system.
- **Context management**: Whether your application is standalone, hosted (executed from a DCC interpreter) or remote (attached to a DCC), the SDK's runner seamlessly handles those different cases.
- **Safety checks**: Various procedures executed at startup such as checking if the Bip server can be reached, if the currently logged-in user is valid...
- **Config and settings**: The plugin configuration values and the current user settings values are automatically loaded.
- **UI helpers**: Easy access to builtins UI widgets, such as an automatic "What's new" new, overlays, message dialogs with optional traceback extraction...
- **Bip stylesheet**: Visual integration of the PySide GUI in the Bip style.
- **Permissions**: Automatically checks user permissions and handles privilege errors.


### Handler

A handler provides a uniformed abstraction layer between various kind of external tools of libraries. The main interest behind this additional layer is to reduce the surface of the interface and therefore risks of spaghetti code. 

If you need to add support for a new DCC, or if you need to switch from a production tracker to another, you only need to create a new handler plugin for that new product, while the access interface will remain the same. You won't have to update anything if the code of the applications using these handlers. 

In other words, instead of having applications talking directly to Maya, they talk to a Handler with standardized methods and attributes, which talks to Maya. Your application is them agnostic by design, since it contains no direct calls to Maya's API.

There are three handler types:
- **Submitter**: Interface with farm management tools (e.g. Deadline).
- **Host**: Interface with DCC API (e.g. Maya).
- **Tracker**: Interface with production tracking tools (e.g. Shotgrid).

## Workflow
### Entities
A **Source** object must be registered to a Bip instance. The **Source** consists of a URL to a Git repository. In order for the registration to be successful, the repository must follow the mandatory structure, mainly by containing a valid `bip.yml` descriptor at its root.

Once a _Source_ is registered, it is then possible to enable a specific version of plugin, either for a project or for the whole instance, depending on the plugin's scope. When enabling that _Source_, a new **Plugin** object is created.

### Scopes
- **Global**: The plugin is instance-wide.
- **Project**: The plugin is attached to a project. Its version, config and settings can be different from a project to another.

:::caution
The _global_ scope is reserved to some the entry point **applications**, launcher and updater. Handlers cannot be global.
:::

### Levels
- **Core**: The plugin is a vital element distributed by Blink (Bugne, Brioche and Clafoutis)
- **Circle**: The plugin is a default element distributed by Blink.
- **World**: The plugin is third-party and is not supported by Blink.

:::caution
This flag is set automatically by the API, as an indicative label. It has no effect on permissions.
:::

### Git
The API uses **Git** for cloning the plugins onto the client machine and for checking out the relevant tag version.


## Initializers
Plugins are initialized internally by two APIS. The main goal is to control the execution and to ensure the running plugin matches an existing plugin source registered and enabled on the database. Finding the matching plugin data is the designed way to associate the running plugin with its configuration, user settings and mapping data, as well as integrations choices.

A plugin is initialized by:
- The `sdk.runner` for applications.
- The `mink.plugins` getter methods for handlers.

:::info
Hosted and remote applications also get automatically fed with their corresponding host plugin instance.
:::


## Application contexts

Applications can be executed in three different contexts:

- **Standalone**: The application is executed from the main Bip runtime, and has no host.
- **Hosted**: The application is executed from a DCC's embedded Python interpreter, and the host plugin associated with the DCC is provided to the application at initialization.
- **Remote**: The application is executed from the main Bip runtime, but is virtually attached to a DCC which can send signals (with `mink.remote`), and the host plugin associated with the DCC is provided to the application at initialization.

## Procedures

These rules are owned by the API and are located, depending on their scope, in:
- `link.plugin`
- `link.specials.install`
- `mink.update`
- `mink.batches`
- `mink.env`

#### Creating a Bip server instance
- The ***core*** apps are automatically registered (with the version set to the latest valid git tag), enabled and locked at server level:
    - **Brioche** (Launcher)
    - **Bugne** (Updater)
	- **Clafoutis** (Center)
- The ***circle*** apps are registered (with the version set to the latest tag by default):
	- **Moka** (Editor)
	- **Kouglof** (Explorer)

#### Installing a Bip client
- Get core applications from server.
- If Brioche and Bugne are not found, raise an error and interrupt installation.
- Clone Brioche (Updater) to `$BIP_PATH/apps`.
- Clone Bugne (Launcher) to `$BIP_PATH/apps`.

#### Updating a Bip client
- Get all registered plugins from server.
- Checkout applications to `$BIP_PATH/apps` .
- For _global_ applications: set their version from server.
- For _project_ applications: set their version from active project.
- Checkout handlers to `$BIP_PATH/handlers`.
- Set their version from active project.

#### Starting a Bip client
- For each enabled application in the project:
	- Append root to `PYTHONPATH`.
	- If a `bip` integration exists and is enabled, append to batches.
- For each enabled handler (except of Host type):
	- Append root to `PYTHONPATH`.

#### Waking-up a Bip client
- Get server enabled plugins, with their registered version.
- Compare with currently installed version.
- If newer, trigger an update.

#### Changing active project
- Trigger an update.

#### Registering a Source
- Clone the Source  locally in _temp_ folder.
- Validate descriptor.
- Register to database.

#### Enabling/updating a Plugin
##### Application
- Check if API version is compatible.
- Check if project's model is compatible.
- If there are required handlers, check if they are enabled for the project.
- Save the plugin descriptor (mainly the API version, for later checks) for the current project.

##### Handler
- Check if API version is compatible.
- Check if project's model is compatible.
- Save the plugin descriptor (mainly the API version, for later checks) for the current project.

#### Disabling a Plugin
##### Application
- If there are plugins requiring this application, forbid change.

##### Handler
- If there are plugins requiring this handler, forbid change.
- If there are plugins optionally supporting this handler, allow change but notify.

#### Changing the instance version
- For each project:
	- For each enabled plugin:
		- If future API breakpoint > plugin API version: disable plugin.

#### Starting a host batch
- Get host plugin (matching `product_id`):
	- Append matching host handler to `PYTHONPATH`
	- If the host plugin has an integration:
		- Append integration batch to existing batch
- For each application enabled for the active project:
	- If an integration is matching the batch `product_id`:
		- Append integration batch to existing batch

## Limitations
- **No requirements**: The plugin must only use the Python libraries shipped with Bip.
- **Descriptor consistency**: Even if the descriptor can be completely changed from a version to another, only the type from when the Source has been registered is used.