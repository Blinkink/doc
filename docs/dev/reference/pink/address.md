---
sidebar_label: address
title: bip.pink.address
---

## `Address`

```python
class Address()
```

Complete address of a Version.

It includes upstream hierarchy as well as task and author,
and provides some useful helpers.

The address is found by inspecting a given path. The inspection scope
is the currently logged-in's active project.

**Arguments**:

- `path` _str_ - Path to be matched to a Bip version from the current active project.
  

**Attributes**:

- `path` _str_ - Provided path.
- `project` _Project_ - Project owning the version.
- `item` _Item_ - Item owning the version.
- `component` _Component_ - Component owning the version.
- `version` _Version_ - Version found matching the given path.
- `task` _Task_ - Task linked to the found version's component, if any.
- `author` _User_ - User who created the version.
- `is_tracked` _bool_ - If True, the given path has been
  matched to an existing version. Defaults to `False`.
- `is_owned` _bool_ - If True, the currently logged-in
  user is the author of the found version. Defaults to `False`.
- `is_latest` _bool_ - If True, the found version is the
  most recent of its parent component. Defaults to `False`.

## `get`

```python
def get(path: str)
```

Get a version's address from a path.

Attempt to extract the address of a version if the
given path matches an existing version found
in the currently logged-in user's active project.

**Arguments**:

- `path` _str_ - Path to the version to find.
  

**Returns**:

- `Address` - Address object.

## `get_from_host`

```python
def get_from_host(host)
```

Get a version's address from a Host's currently opened file.

This function is a shortcut to the get() function which
takes a path as an argument.

**Arguments**:

- `host` _Host_ - Host plugin.
  

**Returns**:

- `Address` - Address object.

