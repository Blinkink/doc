---
title: Auto-generate Folders
---

:::tip What?
You can ask Bip to automatically generate tasks and group directories when the item is created. To do so, you must specify what folders will  be created, in a preset yml file.
:::

## How to
- In your Bip content repository, create the following folders: `presets/projects/folders/`.
- In this directory, you can two types of directories:
  - **`global`**: Will be used as the default preset if no project-specific presets are found
  - **`{project-folder-name}`**: Will be used instead of `global` if found. (eg: `*INK493_DoorDash2*`)
- In the previously created directory, you can create a yml file following this filename rule: **`{item-tag}.yml`**
  - *`shot.yml`*
  - *`edit.yml`*
- The content of the yml file must follow this structure:
```yml
File type 1 name:
- Task 1 name
- Task 2 name
- ...
File type 2 name: all
File type 2 name: none
...
```

## Example
### `asset.yml`
```yml
documents:
  - modelling
  - lookdev
models:
  - modelling
  - lookdev
textures:
  - modelling
  - lookdev
```