---
title: Development setup
sidebar_position: 10
---

:::caution
This page is work in progress
:::

How to set up a development environment for working on Content?
- Checkout content repo
- Override content repo location with `BIP_CONTENT`
- Disable auto-refresh with `BIP_REFRESH_CONTENT`
