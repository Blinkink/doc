---
title: Offline mode
sidebar_position: 3
---

## Introduction
You might have security requirements that forces you to run you Bip instance disconnected.
In that case you will have to make sure the following are offline:
- Bip server
- Git repositories (for Bip and Content updates)
- Python package repository (used by `pip` for installation and updates)

## Bip server
Host your Bip server on a local machine, reachable with a FQDN.

## Git repositories
:::caution
For now, access to git.blinkink.co.uk is still required for the vendored dependencies: `jeanpaulstart` and `shotgun_api3`/
:::

Host a Git repository manager, like Gitea (lightweight), reachable with a FQDN.
- If this server is allowed to access internet, it can simply mirror the official repositories.
- Otherwise, you must clone the repositories and work detached from official.

## Python package repository
Bip uses `pip` both at install and updates. The best solution for offline is to use `pip2pi` to build an offline repository.

:::caution
You have to make sure that the repository content matches the `requirements.txt` from Bip client, and also includes any `api_repository` requested by your activated plugins.
:::

### pip2pi
First let's locally create the repository from a machine that has Internet access.

:::info
Make sure you run this from Windows, and not Linux, since the wheels for PySide and OpenCV will differ based on the execution platform.
:::

```bash
mkdir python-packages

# Copy needed archives
pip2tgz python-packages/ -r /path/to/a/cloned/bip/client/requirements.txt

# Build index
dir2pi python-packages/ --no-symlink # If on Windows, avoid symlinks

# Rename some packages (unsure why it does not work without doing this)
cd python-packages/simple
mv pyside2-5-15-2 pyside2
mv shiboken2-5-15-2 shiboken2
mv pyyaml/PyYAML-5.3.1-cp38-cp38-win_amd64.whl pyyaml/PyYAML-5.3.1-py3-none-any.whl
mv markupsafe/MarkupSafe-2.1.0-cp38-cp38-win_amd64.whl markupsafe/MarkupSafe-2.1.0-py3-none-any.whl
```

Then edit the `index.html` of `markupsafe/` and `pyyaml/` so they match the new filenames.

Then you can copy this `python-packages` to a local http server that will be accessible by any machine that will use Bip.

:::danger
`file://` does not work well with UNC path. Use a `http://` address instead.
:::

Finally, edit the server config by enabling the Offline mode, and setting the Python package repository starting with `http://`, `https://` or `file://`, like `file:///C:/location/to/python-packages/simple` or `http://local.server/python-packages/simple`.