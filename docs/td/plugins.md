---
title: Plugins
sidebar_position: 6
---

:::info Looking for?
- The design principles behind plugins, check **[guide](/dev/design/plugins.md)**.
- How to create a plugin, check the **[guide](/dev/guides/plugins.md)**.
- How to manage plugins from a GUI, check the **[Clafoutis guide](/user/apps/clafoutis/tutorials/manage-plugins.mdx)**.
:::

## Introduction

In order to enable a plugin, you must first **register its source**. The source only takes a http(s) URL to the Git repository of the plugin.

Once the source is registered, **it can be enabled**, by specifying a version, and a project if the plugin's scope is not global (this is the most common case). A plugin object is then returned.

## Register a source
```python
from bip import link

link.plugin.register("https://github.com/someone/my-plugin.git")
```

## Enable a plugin
:::tip
If the plugin has a global scope, just don't specify a project!
:::

```python
from bip import link

demo = link.project.get_project("demo", tag="basic")
source = link.plugin.get_source("salambo")
tag = source.get_latest_version()  # v0.0.1
plugin = source.enable(version=tag, project=demo)
```

## Update a plugin

```python
from bip import link

demo = link.project.get_project("demo", tag="basic")
source = link.plugin.get_source("salambo")
plugin = source.get_plugin(demo)
plugin.set_version("v0.0.2")
```

## Disable a plugin

:::danger
Not yet implemented!
:::