---
title: For plugins
sidebar_position: 3
---

:::info Looking for?
- The design principles behind plugins, check **[guide](/dev/design/plugins.md)**.
- A complete reference for plugins classes, `App`, `Tracker`, `Host` and `Submiter`, check the **[SDK](/dev/reference/sdk/app.md)**.
- How to create a plugin, check the **[guide](/dev/guides/plugins.md)**.
- How to manage plugins in your instance, check the **[TD documentation](/td/plugins.md)**.
:::

## Introduction

This guide shows how to set up your development environment for working on a plugin.

By default, a plugin is initialized by:
- The `sdk.runner` for applications.
- The `mink.plugins` getter methods for handlers.

In both cases, the `App` or `Handler` object initialized is loaded with a `Plugin` object (`link.plugin`) from the database. This is the way designed in Bip for the `App` or `Handler` instance to be loaded with the plugin configuration, user setting and mapping values defined by the user and the administrator of the Bip instance.

But when developing a plugin, you don't want to register the plugin source to the database, and enable a locked version of that plugin. You want to be able to modify the `bip.yml` descriptor any-time, without reflecting those changes of the database.

This is why the **`BIP_DEBUG_PLUGIN`** environment variable has been created. When set with the path(s) to the root directory of the plugin(s) you are working on, this variable tells Bip to initialize the plugin object with a fake floating Plugin object generated temporally from the repository descriptor instead of trying to find it on the database. same for the user settings. Some fake default user settings are generated on the fly.

## Setup

:::caution
It is not recommended to develop a plugin by using an existing production client. You should set up a local development instance of Bip.
:::

Clone the client (https://git.blinkink.co.uk/bip/client.git) and add it to your development project dependencies (`PYTHONPATH`, `sys.path`, Pycharm's "_Project Dependencies_"...). Follow the [recommended folder structure](/dev/development/structure.md) for you working directory.

Create a **Python 3.8** virtual environment and install the client dependencies (`cd /path/to/bip/client & pip install -r requirements.txt`).

Follow [the setup tutorial](/dev/development/setup.mdx) to get a running database instance and a Bip config properly done.

## Write the plugin

[Create and populate](/dev/guides/plugins.md) the minimal working structure for your plugin: descriptor `bip.yml` and entry point `handler.py` if the plugin is a handler.

:::tip
If the plugin is an application, you can use the demo application [**Chocolatine**](https://git.blinkink.co.uk/bip/apps/chocolatine) as a base template.
:::

## Test run

### Standalone
When running your plugin, specify the following environment variables:
- **`BIP_DEBUG_PLUGIN`**: Get the plugin data from the working project instead of the database (_example_: `/path/to/my/plugin;path/to/another/plugin`)
- `BIP_CONFIG`: Bip configuration file (_example_: `/path/to/bip/config/config.yml`)
- `BIP_AUTH`: Bip authentication file (_example_: `/path/to/bip/config/auth.yml`)
- `BIP_DATABASE_PASSWORD`: Neo4j password (_example_: `your-database-password`)

Optionally, you can also specify `BIP_DEBUG_PLUGIN_DATA` in order to provide values to the **mapping** and/or **config** parameters. The environment variable value should be a path to a yaml file containing the values for each parameter you want to set. The file should be structure like so:

```yaml
plugin-slug:
  config:
    param1: Hello world
    param2: 123
  mapping:
    mapping1: asset
    mapping2: shot
```

If your development machine has a production Bip client installed, **you must also override the following environment variables**, in order to avoid conflicting with the production instance:
- `BIP_PATH`: Root of the Bip ecosystem (_example_: `path/to/the/parent/folder/of/your/bip/repo`)
- `BIP_PYTHON`: Python virtual environment (_example_: `path/to/the/root/of/the/python_venv`)


### Hosted
If you are developing a Host plugin or an application which accepts the `HOSTED` context, you need to use a **Python 3.7** virtual environment with the Bip client dependencies installed on. It won't be used for its interpreter, but for its `site-packages` folder.

You can use the following hacky script for running your plugin from the DCC interpreter without having to restart the DCC.

:::caution
Deleting modules that way is not recommended and can lead to buggy behaviours. Keep that in mind while debugging.
:::

```python
import os
import sys

# Variables
python = "/path/to/bip/hosted/Lib/site-packages"
root = "/path/to/bip"
config = "/path/to/bip/config/config.yml"
auth = "/path/to/bip/config/auth.yml"
database_password = "bipbop"

# Environment variables
os.environ["BIP_CONFIG"] = config
os.environ["BIP_ROOT"] = root + "/client"
os.environ["BIP_DATABASE_PASSWORD"] = database_password
os.environ["BIP_AUTH"] = auth

# Function
def delete_modules(starts_with):
    to_delete = [m for m in sys.modules if m.startswith(starts_with)]
    for module in to_delete:
        if module in sys.modules:
            del sys.modules[module]

# Python path
sys.path.append(python)

# Clear app import
delete_modules(starts_with="my_app")
sys.path.append(root + "/apps/my_app")

# App
import my_app

my_app.run()
```