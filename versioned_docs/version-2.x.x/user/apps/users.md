---
title: Users
---
:::caution
This application requires **administrator** privileges.
:::

<img height="300" src={require("../../assets/users-list.png").default} />


## Introduction

The **Users** <img  width="20" height="20" src={require("../../assets/apps/users.png").default} /> app allows managing (view, create, edit and delete) Bip users.

### Features
- View users.
- Create, edit, delete user.

:::tip
At user creation, the administrator can set a default password. [See the TD preset documentation](../../td/config#global).
:::

## Usage
- **Create a user**: Click on the **+** button in the bottom-right corner.
- **Edit a user**: Double-click or `right-click > Edit` on a user.
- **Delete a user**: `Right-click > Delete`.

:::info
When a user is deleted, it is not deleted from the database, for data integrity reason.
:::

## Reference
### List view

See all the users of your Bip instance.

Active users are displayed first in the list, while inactive users are greyed out at the bottom of the list, yet still editable.

![](../../assets/users-list.png)

### Edit view

Creation or edition form.

The following fields are mandatory:
- _First name_
- _Initials_
- _Username_
- _Role_
- _Privilege_

![](../../assets/users-edit.png)