---
sidebar_label: component
title: bip.link.component
---

## `Component`

```python
class Component(Data)
```

Representation of a Bip component.

A Component is a part of an Item. It can be seen as the representation of a group of files.
For example, in a CG production, the modelling scenes of an asset can be represented as a Component.
A Component has versions, which are represented by the smallest entity of Bip, the Version object.

Components are defined by ComponentTags, which define a set of rules and behaviours for the Component. For example
the tag controls the naming convention.

:::info
This class inherits from **Data**, like Project, Item and Version, since they share similar behaviour.
See `bip.link.data.Data` for inherited methods.
:::

**Attributes**:

- `uid` _str_ - Bip unique identifier (uuid4).
  Edition is forbidden if this is an persistent (saved) entity.
- `slug` _str_ - Human-readable unique identifier, namespaced in the parent tag scope.
- `name` _str_ - Name of the project.
- `folder_name` _str_ - Folder name for path generation.
- `description` _str_ - Short description of the item.
- `collapsed` _bool_ - Used for path generation. If True, the versions
  don't have a sub-directory between them and the root of the Component. If False they do.
  Typically, it adds a `v001` directory.
- `tag` _ComponentTag_ - Bip item tag object.

### `path`

```python
@property
def path() -> str
```

Dynamic path of the component directory.

The path is dynamically generated by the running Bip client, based on the working directory.

:::caution
The path may differ from a machine to another since the path uses the `working_directory`
local setting, which might vary between machines, depending on the client config.
:::

**Returns**:

- `str` - Path of the component directory.

### `parent`

```python
@property
def parent() -> Item
```

Parent item.

**Returns**:

- `Item` - parent of the component.

### `task`

```python
@property
def task() -> Union[Task, None]
```

Task associated to the Component.

**Returns**:

  Union[Task, None]: Task if found, None otherwise.

### `latest_version_number`

```python
@property
def latest_version_number() -> int
```

Version number of the latest (highest) Version of the Component.

**Returns**:

- `int` - version number.

### `new_version_number`

```python
@property
def new_version_number() -> int
```

Next version number.

**Returns**:

- `int` - next version number.

### `has_children`

```python
@property
def has_children() -> bool
```

Does the Component have Versions.

**Returns**:

- `bool` - True if the Component has Versions, False otherwise.

### `children_count`

```python
@property
def children_count() -> int
```

Number of Versions parented to the Component.

**Returns**:

- `int` - Versions count.

### `save`

```python
def save()
```

Saves the Component to the database.

If the Component is floating, saving makes it persistent.

Attributes changes are not applied to the database at modification.
Saving the Component does.

**Raises**:

- `ValueError` - Failed validation.
- `TypeError` - Failed validation.

### `delete`

```python
def delete(safe: bool = True)
```

Deletes the Component.

By default, if the Component has Versions parented to it, the deletion won't be accepted,
unless `safe` is set to True.

Deleting the Component deletes any downstream entity:
Version, Group (children only) and Metadata.

**Arguments**:

- `safe` - bool: Prevents the deletion if the Component has children (Versions) (Default value = True)
  

**Raises**:

- `ValueError` - If safe is True and the Component has children.

### `generate_slug`

```python
def generate_slug(suffix: Optional[str] = None) -> str
```

Generate a unique slug based on groups memberships.

There can be a lot of components, and finding human-readable slugs can be complicated.
This method uses all the groups that the component is member of, as well as any found task,
to generate a slug from it.

**Arguments**:

- `suffix` - Optional[str]: If set, the custom suffix is added to the generated slug.
  

**Returns**:

- `str` - generated slug.

### `generate_filename`

```python
def generate_filename(ext: Optional[str] = None, suffix: Optional[str] = None, version: Optional[int] = None)
```

Generate a filename from the tag pattern.

If the ComponentTag has a filename_pattern set, it is used to generate a filename.

This is useful for saving or creating new files and naming them automatically, by the Projects specifications.

**Arguments**:

- `ext` - Optional[str]: File extension. If None specified, the filename has no extension.
- `suffix` - Optional[str]: Custom suffix.
- `version` - Optional[int]: Version number. If none specified, Component.new_version_number is used.
  

**Raises**:

- `ValueError` - If the ComponentTag has no filename_pattern set.
  

**Returns**:

- `str` - Generated filename.

### `auto_slug`

```python
def auto_slug(suffix: Optional[str] = None)
```

Automatically sets a generated slug.

Uses Component.generate_slug() and set it as a slug.

### `get_version`

```python
def get_version(number: int) -> Version
```

Convenient class implementation of: `bip.link.version.get_version`.

:::caution
The signature of `Component.get_version()` and `bip.link.version.get_version()`
are different since `Component.get_version()` passes itself to the function as `component=self`.
:::

### `get_latest_version`

```python
def get_latest_version() -> Version
```

Convenient class implementation of: `bip.link.version.get_latest_version`.

:::caution
The signature of `Component.get_latest_version()` and `bip.link.version.get_latest_version()`
are different since `Component.get_latest_version()` passes itself to the function as `component=self`.
:::

### `get_versions`

```python
def get_versions() -> List[Version]
```

Convenient class implementation of: `bip.link.version.get_versions`.

:::caution
The signature of `Component.get_versions()` and `bip.link.version.get_versions()`
are different since `Component.get_versions()` passes itself to the function as `component=self`.
:::

### `get_children`

```python
def get_children() -> List[Version]
```

Alias for: `Component.get_versions`.

### `new_version`

```python
def new_version(files: Union[str, List[str]], author: Optional[User] = None, auto_save: bool = True, **kwargs) -> Version
```

Convenient class implementation of: `bip.link.version.new_version`.

:::caution
The signature of `Item.new_version()` and `bip.link.version.new_version`
are different since `Item.new_version()` passes itself to the function as `component=self`.
:::

### `generate`

```python
@classmethod
def generate(cls, item: Item, name: Optional[str] = None, groups: Optional[List[Group]] = (), task: Optional[Task] = None, tag: Optional[ComponentTag] = None, auto_save: bool = True, **kwargs, ,) -> Component
```

Generates a Component object.

By default, the generated Component is saved straight after creation. In some cases, it can be useful to get the
floating (not recorded on the database) object in order to perform further operation, and save after that.
That can be achieved by setting `auto_save` to False.

**Todo**:

  - Check if Task is owned by the Item.
  

**Arguments**:

- `name` - str: Name of the new component.
- `item` - Item: Parent item.
- `groups` - Union[List, Tuple]: If the ComponentTag has required_memberships, list of the
  mandatory groups (Default value = ())
- `task` - Optional[Task]: Associated task. The Task must be owned by the Item.
- `tag` - Optional[Union[ComponentTag, str]]: Tag of the new component. Can be left to None if the project
  has a `default_component_tag` (see `link.project`),
  or can be the string of a ItemTag slug (Default value = None)
- `auto_save` - bool: If True, the returned object is saved. Otherwise it is floating. (Default value = True)
- `**kwargs` - Any non-mandatory valid attribute of Item can be passed.
  

**Raises**:

- `ValueError` - Failed validation, if auto_save is True.
- `TypeError` - Failed validation, if auto_save is True.
  

**Returns**:

- `Component` - Generated Component.

### `get`

```python
@classmethod
def get(cls, tag: ComponentTag, slug: Optional[str] = None, uid: Optional[str] = None) -> Union[Component, List[Component]]
```

Get a specific Component or all Components from a ComponentTag.

It is possible to get a specific Component from its `uid` or its `slug`.
When providing one, the other can be left to None. If none of these two parameters are specified,
The getter becomes a global getter and returns a collection of Components.

**Arguments**:

- `tag` - Union[ItemTag, str]: ComponentTag object to search within.
- `slug` - Optional[str]: Slug of an Component. If specified, `uid` can be left blank. (Default value = None)
- `uid` - Optional[str]: Uid of an Component. If specified, `slug` can be left blank. (Default value = None)
  

**Raises**:

- `TypeError` - An argument has an unexpected type.
- `LookupError` - No matching entity found.
  

**Returns**:

  - list: Collection of Components, if no uid or slug specified.
  - Component: Single Component if a uid or a slug has been specified.

### `get_from_item`

```python
@classmethod
def get_from_item(cls, item: Item, slug: Optional[str] = None, uid: Optional[str] = None, task: Optional[Task] = None) -> Union[Component, List[Component]]
```

Get a specific Component or all Components from a Project.

It is possible to get a specific Item from its `uid` or its `slug`.
If no uid is provided, a collection of all the project Items is returned.

**Arguments**:

- `item` - Item: Item object to search within.
- `slug` - Optional[str]: Slug of an Item. If specified, `uid` can be left blank. (Default value = None)
- `uid` - Optional[str]: Uid of an Item. If specified, `slug` can be left blank. (Default value = None)
- `task` - Optional[Task]: If no `slug` and `uid` are specified, specifying a Task is allowed to only get the
  Components from a specific task. Otherwise, all of the Item Components are returned.
  

**Raises**:

- `TypeError` - An argument has an unexpected type.
- `LookupError` - No matching entity found.
  

**Returns**:

  - list: Collection of Components, if no uid or slug specified.
  - Component: Single Component if a uid or a slug has been specified.

### `get_from_filename`

```python
@classmethod
def get_from_filename(cls, filename: str, item: Item) -> List[Component]
```

Get all the Components with a version containing the given filename.

**Arguments**:

- `filename` - str: Looked up filename.
- `item` - Item: parent Item to look into.
  

**Returns**:

- `list` - Collection of matching Components.

## `ComponentTag`

```python
class ComponentTag(BipObject)
```

Representation of a Bip item tag.

The ItemTag is a template, which sets some rules that any Item tagged with this tag must follow.

**Attributes**:

- `uid` _str_ - Bip unique identifier (uuid4).
  Edition is forbidden if this is an persistent (saved) entity.
- `slug` _str_ - Human-readable unique identifier.
- `name` _str_ - Name of the tag.
- `plural` _int_ - Plural of the name.
- `folder_name` _str_ - Folder name if the tag is referenced in a path pattern.
- `path_pattern` _str_ - Path pattern for Components path generation.
- `filename_pattern` _str_ - Filename pattern for Components path generation.
- `force_task` _str_ - If True, Components with this tag must be associated with a task.
- `version_prefix` _str_ - Version prefix. (default: "v")
- `version_padding` _int_ - Version padding. (default: 3)

### `project`

```python
@property
def project() -> Project
```

Parent project.

**Returns**:

- `Project` - parent of the ComponentTag.

### `children_count`

```python
@property
def children_count() -> int
```

Number of Components linked to the ComponentTag.

**Returns**:

- `int` - Component count.

### `has_children`

```python
@property
def has_children() -> bool
```

Does the ComponentTag have Components.

**Returns**:

- `bool` - True if the ComponentTag has Items, False otherwise.

### `has_required_memberships`

```python
@property
def has_required_memberships() -> bool
```

Does the ComponentTag requires memberships.

This utility method does not query the database, and is a
helper for checking if it is necessary to get the required memberships.

**Returns**:

- `bool` - True if the ComponentTag requires memberships, False otherwise.

### `save`

```python
def save()
```

Saves the ComponentTag to the database.

If the ComponentTag is floating, saving makes it persistent.

Attributes changes are not applied to the database at modification. Saving the ComponentTag does.

### `delete`

```python
def delete(safe: bool = True)
```

Deletes the ComponentTag.

By default, if the ComponentTag has Components parented to it, the deletion won't be accepted,
unless `safe` is set to True.

Deleting the ComponentTag deletes any downstream entity:  Component, Version and Metadata.

**Arguments**:

- `safe` - bool: Prevents the deletion if the ComponentTag has children (Components) (Default value = True)
  

**Raises**:

- `ValueError` - If safe is True and the Components has children.

### `get_components`

```python
def get_components() -> List[Component]
```

Convenient class implementation of: `bip.link.component.get_items`.

:::caution
The signature of `ComponentTag.get_components()` and `bip.link.component.get_components()`
are different since `ComponentTag.get_components()` passes itself to the function as `tag=self`.
:::

### `get_required_memberships`

```python
def get_required_memberships() -> List[GroupTag]
```

Convenient class implementation of: `bip.link.group.get_required_memberships`.

:::caution
The signature of `ComponentTag.get_required_memberships()` and `bip.link.group.get_required_memberships()`
are different since `ComponentTag.get_required_memberships()` passes itself to the function as `entity=self`.
:::

### `add_required_membership`

```python
def add_required_membership(group_tag: GroupTag)
```

Adds a required membership.

:::tip
Required memberships are GroupTag that any new Item created with the current ComponentTag must be member of in order
to be saved.
:::

**Arguments**:

- `group_tag` - GroupTag: Required GroupTag.
  

**Raises**:

- `ValueError` - Failed validation.
- `TypeError` - Failed validation.

### `format_version_number`

```python
def format_version_number(number: int) -> str
```

Format a version number according to the rules of the tag.

For example, if version_prefix is set to `v` and version_padding is set to `3`,
this method would return `v003`.

### `generate`

```python
@classmethod
def generate(cls, name: str, project: Project, auto_save: bool = True, **kwargs) -> ComponentTag
```

Generates a ComponentTag object.

By default, the generated ComponentTag is saved straight after creation.
In some cases, it can be useful to get the floating (not recorded on the database)
object in order to perform further operation, and save after that.
That can be achieved by setting `auto_save` to False.

**Arguments**:

- `name` - str: Name of the new tag.
- `project` - str: Parent Project of the new tag.
- `auto_save` - bool: If True, the returned object is saved. Otherwise it is floating. (Default value = True)
- `**kwargs` - Any non-mandatory valid attribute of ComponentTag can be passed.
  

**Raises**:

- `ValueError` - Failed validation, if auto_save is True.
- `TypeError` - Failed validation, if auto_save is True.
  

**Returns**:

- `ComponentTag` - Generated ComponentTag.

### `get`

```python
@classmethod
def get(cls, project: Project, slug: Optional[str] = None, uid: Optional[str] = None) -> Union[ComponentTag, List[ComponentTag]]
```

Get a specific ComponentTag or all ProjectTags.

It is possible to get a specific ComponentTag from its `uid` or its `slug`.
When providing one, the other can be left to None. If none of these two parameters are filled,
The getter becomes a global getter and returns a collection.

**Arguments**:

- `project` - str: Parent Project.
- `slug` - Optional[str]: Slug of a ComponentTag. If specified, `uid` can be left blank. (Default value = None)
- `uid` - Optional[str]: Uid of a ComponentTag. If specified, `slug` can be left blank. (Default value = None)
  

**Raises**:

- `LookupError` - No matching ComponentTag found.
  

**Returns**:

  - list: Collection of ComponentTags, if no uid or slug specified.
  - ComponentTag: Single ComponentTag if a uid or a slug has been specified.

## `get`

```python
def get(tag: ComponentTag, slug: Optional[str] = None, uid: Optional[str] = None) -> Union[Component, List[Component]]
```

Convenient shortcut to `Component.get` for getting a specific Component or all Components.

## `get_from_item`

```python
def get_from_item(item: Item, slug: Optional[str] = None, uid: Optional[str] = None, task: Optional[Task] = None) -> Union[Component, List[Component]]
```

Convenient shortcut to `Component.get_from_item` for getting a specific
Component or all Components from an Item.

## `get_component`

```python
def get_component(tag: ComponentTag, slug: Optional[str] = None, uid: Optional[str] = None) -> Component
```

Convenient shortcut to `Component.get` for getting a specific Component.

**Raises**:

- `ValueError` - No slug and no uid provided.

## `get_components`

```python
def get_components(tag: ComponentTag) -> List[Component]
```

Convenient shortcut to `Component.get` for getting all Components.

## `new`

```python
def new(item: Item, name: Optional[str] = None, groups: Optional[List[Group]] = (), task: Optional[Task] = None, tag: Optional[ComponentTag] = None, auto_save: Optional[bool] = True, **kwargs, ,) -> Component
```

Convenient shortcut to `Component.generate`.

## `new_component`

```python
def new_component(item: Item, name: Optional[str] = None, groups: Optional[List[Group]] = (), task: Optional[Task] = None, tag: Optional[ComponentTag] = None, auto_save: Optional[bool] = True, **kwargs, ,) -> Component
```

Convenient shortcut to `Component.generate`.

## `get_tag`

```python
def get_tag(project: Project, slug: Optional[str] = None, uid: Optional[str] = None) -> ComponentTag
```

Convenient shortcut to `ComponentTag.get` for specific ComponentTag.

**Raises**:

- `ValueError` - No slug and no uid provided.

## `get_tags`

```python
def get_tags(project: Project) -> List[ComponentTag]
```

Convenient shortcut to `ComponentTag.get` for all ComponentTags.

## `new_tag`

```python
def new_tag(name: str, project: Project, auto_save=True, **kwargs) -> ComponentTag
```

Convenient shortcut to `ComponentTag.generate`.

## `get_default_component_tag`

```python
def get_default_component_tag(item: Item) -> Union[None, ComponentTag]
```

Get the default ComponentTag of the Bip server, if any.

**Returns**:

  Union[None, ComponentTag]: ComponentTag if a default is set, None otherwise.

## `get_from_filename`

```python
def get_from_filename(filename: str, item: Item) -> List[Component]
```

Convenient shortcut to `ComponentTag.generate`.

