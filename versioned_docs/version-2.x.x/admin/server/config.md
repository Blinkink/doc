---
title: Configuration
sidebar_position: 2
---
The Server administration and configuration can be done through the builtin application Admin. You can start this application by typing its name in the Launcher.

:::caution
You must have an admin-level user account.
:::

## Settings
![](../../assets/admin-settings.png)

- **Active project timout**: In days. If a user hasn't connect to the server more than X days, their Active Project is reset and they will have to pick it again next time they log in. Set to `0` for no timeout.

## Sources
![](../../assets/admin-sources.png)

- **Bip**: Url to a Bip client repository. By default, you can use `https://git.blinkink.co.uk/bip/client.git`, but you can also fork it and use your own version of the Bip client.
- **Content**: URL to a Bip content repository. The repository must follow the [structure guidelines explains in the TD documentation](../../td/intro.md).

:::info
Bip does not support yet ssh keys, so the repositories must be public.
:::

## Versions
![](../../assets/admin-versions.png)

This is how you manage the updates for your users. When a new version of the client is published, the tag appears in this list. If you change the **recommended version**, your users will have a non-blocking update footer message in their Launcher:

![](../../assets/launcher-optional-update.png)

If you modify the **minimal version** and their current version is below, they will get a blocking updater message:
![](../../assets/launcher-forced-update.png)

:::note
The tags must follow the [semantic versioning (semver)](https://semver.org/) guidelines in order to be properly sorted and compared.
:::

The recommended version must be superior or equal to the minimal version.

This works both ways, so you can use this for emergency downgrading for instance.

Unless you have a good reason not to, it is recommended to keep **minimal version** and **recommended version** synchronised all the time.


## Announcement
![](../../assets/admin-announcements.png)

You can set short announcements messages that will be displayed at the bottom of the Launcher:

![](../../assets/launcher-announcement-info.png)

- **Enabled**: Activate the announcement
- **Message**: Short text
- **Level**: Info (blue) or warning (orange)
- **Closable**: Allow users to close the announcement
- **Force show**: Set this message status as unseen for all users, even the ones who closed it already

If you make your announcement not closable, you must disable it from here in order to stop its display.

Once an announcement is closed by a user, there is no way for them to see it again.

## Production tracker
:::danger
**Experimental feature!** Introduced in _v2.2.1_.
:::
![](../../assets/admin-tracker-default.png)

By default, the production tracker is the Internal one, which is not implemented yet. If you can to connect some of your projects to a production tracker, you must enable it here.

Then, when creating a project, you will be able to chose which production tracker will be used for the project.

The production tracker choice is per project, but the integration setting is server-wide.

Available trackers:
- Internal (_null_)
- Shotgrid (_experimental_)

### Shotgrid
![](../../assets/admin-tracker-shotgrid.png)

In order to connect Bip to Shotgrid, you must use the Script authentication method. _[See the official documentation...](https://developer.shotgridsoftware.com/python-api/authentication.html#script-based-authentication)_

Once you have created the Script key, fill the Bip settings:
- **Enable**: Make Shotgrid available in the projects tracker settings
- **Server**: You Shotgrid url (`https://mycompany.shotgrid.com`)
- **Script name**: The script name from the previous step
- **Script key**: The script key from the previous step

## Toolbox
![](../../assets/admin-toolbox.png)

Various helpful utility tools:
- **Force announcement for every user**: Announcement is marked as unseen for everyone.
- **Clear announcement for every user**: Announcement is disabled for everyone.
- **Show welcome tutorial for everyone**: Every user will see the Welcome Tutorial, if it exists, next time they wake up their Launcher.