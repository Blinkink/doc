---
title: Data
sidebar_position: 2
---

:::caution
This page is Work in Progress
:::
## Entities
| Hierarchical                   | Tags                     | Tracking    | Meta                      | Users               | System                                         |
| ------------------------------ | ------------------------ | ----------- | ------------------------- | ------------------- | ---------------------------------------------- |
| Project<br />Item<br />Component<br />Version | ItemTag<br />GroupTag<br />TaskTag | Group<br />Tasks | Definition<br />Metadata<br />Value | User<br />Role<br />Privilege | TaskStatus<br />ProjectStatus<br />ServerConfig<br />Settings |



## Rules
- **Hierarchy**: Project > Item > Component > Version
- **Groups can contain**: Projects, Items, Components and Versions
- **Identifier**: 
  - Everything is UID-based, therefore, no attribute other than uid needs to be unique
  - For human-readability and easy scripting, all entities also have slugs, which are unique within a local scope
- **Tagging**: Tasks, Groups and Elements MUST be tagged
- **Tag exclusiveness**: Once created, a tag is reserved to only ONE entity

### Relationships
- Tasks are associated to one item
- Components can be associated to one task

### Graph example
![Pseudo graph view](../assets/data_model.png)