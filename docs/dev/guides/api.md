---
title: API
---

:::caution Obsolete
These examples are mostly obsolete (v3.x.x). They are to be rewritten soon(ish).
:::

## Examples

This document shows an ordered scenario that shows the main `link` features, from initializing the database to retrieving data.

:::info
When getting entities, these examples mostly use slugs, which are human-readable identifiers, but in a real-life GUI application case, either the uid or general getters would be more likely used.
:::

### Set up the database
```python
from bip import link

link.specials.init(username="admin", password="admin")
bip_repo = "https://git.blinkink.co.uk/bip/client.git"
content_repo = "https://git.blinkink.co.uk/blinkink/bip-content.git"
link.specials.setup(
    content_source=content_repo,
    bip_source=bip_repo,
    minimal_version="latest",
    recommended_version="latest")

```

### Create roles
```python
from bip import link

link.user.new_role(name="Artist", plural="Artists")
link.user.new_role(name="Producer", plural="Producers")
```

### Create top level groups
It is possible to create top-level groups, that can contain projects.

In this case, we create two GroupTags, called type and tag, and we create some groups in them

```python
from bip import link
from bip import wink

# Levels
level = link.group.new_tag(name='level', type=wink.PROJECT)
level.new(name='Job')
level.new(name='Pitch')

# Types
type_ = link.group.new_tag(name='type', type=wink.PROJECT)
type_.new(name='Project')
type_.new(name='Sandbox')
type_.new(name='Library')
```

### Create a user
```python
from bip import link

link.user.new(
    username="zinedine.zidane",
    password="1998",
    initials="ZZ",
    first_name="Zinedine",
    last_name="Zidane",
    privilege=link.user.get_privilege(slug='admin'),
    role=link.user.get_role(slug='artist'))
```

### Create the task statuses
```python
from bip import link

template = {
    'Ready to start': True,
    'Retake': True,
    'Final': False,
    'In progress': True,
    'On hold': True,
    }

for name, open_ in template.items():
    link.task.new_status(name=name, open=open_)
```

### Create a default project tag
```python
from bip import link

generic = link.project.new_tag(
    name="Generic",
    path_pattern="{group:type}/{self}"
)

# A generic project must be a member of a type group and a level group.
level = link.group.get_tag('level')
generic.add_required_membership(level)

type_ = link.group.get_tag('type')
generic.add_required_membership(type_)

# The generic tag is the default for the server.
config = link.config.get()
config.set_default_project_tag(generic)
```

### Create project with a custom model
```python
from bip import link
from bip import wink

model = {
    "info": {
        "name": "",
        "description": "",
        "author": "",
        "version": "",
    },
    "project": {
        "default_component_tag": "generic-file"
    },
    "item_tags": {
        "asset": {
            "path_pattern": "Libraries/Assets/{group:category}/{self}",
            "required_memberships": ["category"],
            "uniqueness_scope": "category",
        },
        "shot": {
            "path_pattern": "Episodes/{group:sequence}/{self}",
            "required_memberships": ["sequence"],
            "uniqueness_scope": "sequence",
        },
    },
    "component_tags": {
        "generic-file": {
            "path_pattern": "{group:file-type}/{task}/{group:variant}",
            "filename_pattern": "{item}_{task}_{group:variant}_{version}",
            "version_prefix": "v",
            "version_padding": 3,
            "force_task": True,
            "required_memberships": ["file-type","variant"]
        },
        "render-file": {
            "path_pattern": "{group:file-type}/{task}/{group:variant}/{group:render-pass}",
            "filename_pattern": "{item}_{task}_{group:variant}_{version}",
            "version_prefix": "v",
            "version_padding": 3,
            "force_task": True,
            "required_memberships": ["file-type", "variant", "render-pass"]
        }
    },
    "task_tags": ["animation", "cfx", "compositing", "fx", "layout", "lighting", "lookdev", "modelling", "rigging"],
    "group_tags": {
        "category": {
            "name": "Category",
            "type": wink.ITEM,
            "plural": "Categories",
            "limits": ["asset"],
            "groups":
                ["characters", "effects", "elements", "gizmo", "lightrigs", "lut", "materials", "props", "rnd",
                 "set", "templates", "vehicles", "environment"]
        },
        "sequence": {
            "name": "Sequence",
            "type": wink.ITEM,
            "plural": "Sequences",
            "limits": ["shot"],
        },
        "file-type": {
            "name": "File type",
            "type": wink.COMPONENT,
            "plural": "File types",
            "groups":
                ["cameras", "data", "elements", "lights", "materials", "models", "plates", "textures", "scenes",
                 "renders", "movies", "sounds", "documents"]
        },
        "variant": {
            "name": "Variant",
            "type": wink.COMPONENT,
            "local": True,
        },
        "render-pass": {
            "name": "Render pass",
            "type": wink.COMPONENT,
            "local": True,
        },
    },
    "definitions": {
        "framerate": {
            "type": wink.FLOAT,
            "name": "Framerate",
            "entity": wink.PROJECT,
            "default": 25.0
        }
    }
}

project = link.group.get_tag(slug="type").get_group(slug="project")
job = link.group.get_tag("level").get_group("job")

example = link.project.new(
    name="Example",
    model=model,
    folder_name="Example",
    groups=(project, job)
)
```

### Create an item
```python
from bip import link

example = link.project.get(slug="example")
asset = example.get_item_tag(slug='asset')
characters = example.get_group_tag(slug="category").get_group(slug="characters")
# Shorter way
characters = example.get_group("category", "characters")


john_doe = asset.new(
    name="John Doe",
    folder_name="JohnDoe",
    groups=(characters, )  # groups is expecting a tuple, even if of one member
)
```

### Create a group
```python
from bip import link

example = link.project.get(slug="example")
sequence = example.get_group_tag("sequence")
sequence.new(
    name="Demo 001",
    folder_name="DE001",
)
```

### Create a local group
```python
from bip import link

example = link.project.get(slug="example")
variants = example.get_group_tag("variant")
john_doe = example.get_item_by_slug("john-doe", "asset", "characters")

variants.new("Main", parent=john_doe, folder_name="Main")
```

### Create a task for an asset
```python
from bip import link

example = link.project.get(slug="example")
john_doe = example.get_item_by_slug("john-doe", "asset", "characters")
modelling = john_doe.new_task("modelling", status="ready-to-start")  # Status can be string or
modelling.set_status('in-progress')
```

### Create a job for a task
```python
from bip import link

example = link.project.get("example")
john_doe = example.get_item_by_slug("john-doe", 'asset', "characters")
modelling = john_doe.get_task("modelling")

title = 'Make the character taller'
description = "The neck could be longer, as well as the legs"
assignee = link.user.get(username='zinedine.zidane')
modelling.new_job(
    title=title,
    assignee=assignee,
    description=description,
    status='ready-to-start')
```

### Create a component
```python
from bip import link

example = link.project.get("example")
john_doe = example.get_item_by_slug("john-doe", 'asset', "characters")

modelling = john_doe.get_task("modelling")
scenes = example.get_group_tag("file-type").get_group("scenes")
main = john_doe.get_group("variant", "main")

scene = john_doe.new_component(
    collapsed=True,  # if False, "/v003/" is added to the path
    task=modelling,
    groups=(scenes, main))

```

### Create a version
```python
from bip import link

example = link.project.get("example")
author = link.user.get(username='zinedine.zidane')
john_doe = example.get_item_by_slug("john-doe", 'asset', "characters")
cg_scene = john_doe.get_component("modelling-scenes-main")
file = cg_scene.generate_filename(ext="lxo")

print(file)
# JohnDoe_Modelling_Main_v001.lxo

cg_scene.new_version(
    files=[file],
    author=author)
```

### Create a metadata
```python
from bip import link

example = link.project.get("example")
john_doe = example.get_item_by_slug("john-doe", 'asset', "characters")
cg_scene = john_doe.get_component("modelling-scenes-main")

# Set metadata
example.set_metadata("working-format", value=[1920, 1080])
metadata = example.get_metadata("working-format")
print(metadata.value)
# [1920, 1080]

# Test limits
try:
    john_doe.set_metadata("framerange", value=[1001, 1020])
except LookupError as e:
    print(e)
    # No definition slug=framerange compatible with BipItem:john-doe for BipProject:example

```

### Get items
```python
from bip import link

example = link.project.get("example")

for item in example.get_items():
    print(item)
# BipItem:0100
# BipItem:john-doe

for item in example.get_items("asset"):
    print(item)
# BipItem:john-doe

john_doe = example.get_item_by_slug("john-doe", 'asset', "characters")
```

### Get groups
```python
from bip import link

example = link.project.get("example")
john_doe = example.get_item_by_slug("john-doe", 'asset', "characters")

# Get children groups and group tags
characters = example.get_group_tag(slug="category").get_group(slug="characters")  # or
characters = example.get_group(slug="characters", tag="category")

renders = example.get_group_tag("file-type").get_group("renders")

main = john_doe.get_group(slug="main", tag="variant")

all_groups = john_doe.get_groups()

variants = john_doe.get_groups(tag="variant")
```

### Get containers
```python
from bip import link

example = link.project.get("example")
john_doe = example.get_item_by_slug("john-doe", 'asset', "characters")

# Get groups that contain the current entity
for group in example.get_containers():
    print(group)
# BipGroup:project
# BipGroup:job

category = john_doe.get_container(tag="category")
print(category)
# BipGroup:characters
```

### Get tasks
```python
from bip import link

example = link.project.get("example")
john_doe = example.get_item_by_slug("john-doe", 'asset', "characters")
tasks = john_doe.get_tasks(open_only=True)
```

### Get a component
```python
from bip import link

example = link.project.get("example")
john_doe = example.get_item_by_slug("john-doe", 'asset', "characters")
modeling_scenes = john_doe.get_component("modelling-scenes-main")
```

### Create a version
```python
from bip import link

example = link.project.get("example")
john_doe = example.get_item_by_slug("john-doe", 'asset', "characters")
modeling_scenes = john_doe.get_component("modelling-scenes-main")

print(modeling_scenes.get_version(0))
# BipVersion:v001
latest = modeling_scenes.get_latest_version()
print(latest.number)
# 1
print(latest.file)
# JohnDoe_Modelling_Main_v001.lxo
print(latest.files)
# ['JohnDoeA_Modelling_Main_v001.lxo']
print(latest.full_paths)
# ['/demo/Example/Libraries/Assets/Characters/JohnDoe/Scenes/Modelling/Main/v001/JohnDoe_Modelling_Main_v001.lxo']
```

### Get group content
```python
from bip import link

example = link.project.get("example")
characters = example.get_group(slug="characters", tag="category")

for entity in characters.get_content():
    print(entity)
# BipItem:john-doe
```

### Get metadata and definitions
```python
from bip import link

example = link.project.get("example")
john_doe = example.get_item_by_slug("john-doe", 'asset', "characters")

for metadata in example.get_metadata(existing_only=True):
    print(metadata)
# BipMetadata:working-format

# Get definitions
example.get_all_definitions()  # List of available definitions
example.get_definitions()  # List of available definitions
john_doe.get_definitions()  # List of available definitions

working_format = example.get_metadata("working-format").value
print(working_format)
# [1920, 1080]

fps = example.get_metadata("framerate").value
print(fps)
# None


```

### Show paths
```python
from bip import link

example = link.project.get("example")
john_doe = example.get_item_by_slug("john-doe", 'asset', "characters")
modeling_scenes = john_doe.get_component("modelling-scenes-main")
latest = modeling_scenes.get_latest_version()

print(example.path)
# /demo/Example
print(john_doe.path)
# /demo/Example/Libraries/Assets/Characters/JohnDoe
print(modeling_scenes.path)
# /demo/Example/Libraries/Assets/Characters/JohnDoe/Scenes/Modelling/Main
print(latest.path)
# /demo/Example/Libraries/Assets/Characters/JohnDoe/Scenes/Modelling/Main/v001/JohnDoe_Modelling_Main_v001.lxo
```

### Get version from path
```python
from bip import link

path = "/demo/Example/Libraries/Assets/Characters/JohnDoe/Scenes/Modelling/Main/v001/JohnDoe_Modelling_Main_v001.lxo"

version = link.path.get_entity_from_path(path=path, single=True)
print(version)
# BipVersion:v001
print(version.item)
# BipItem:john-doe
```