---
title: Structure
sidebar_position: 1
---
## Client
- **bip**: API
  - **link**: _Data_ - Production data object API
  - **sink**: _Database_ - Database querying library
  - **mink**: _Internal_ - Bip utility modules
  - **pink**: _Workflow_ - Work files management
  - **wink**: _Variables_ - Constants, paths and exceptions
  - **uink**: _GUI_ - Widget library and Qt utilities
  - **sdk**: _Toolkit_ - Application template
  - **utils**: Non-Bip utilities
- **assets**: Images, icons, stylesheets
- **builtins**: Essential commands and batches
- **vendor**: External dependencies (ffmpeg)


## Server
:::info Not implemented
This is for future reference. Bip is not yet server-sided, apart from the Neo4j database instance.
:::

- **bop**: API
  - **sink**: RESTful data
  - **kink**: Filesystem agent
  - **fink**: Event listener