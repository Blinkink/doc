---
title: Ingestor (Toolbox)
---

:::caution 
This application is under active development. The following features are not yet implemented:
- Version support
- VPN connection checker
:::
  
## Features
This tool ingests unknown files into the pipeline.

1. **Inspects** the given files based on custom filename **patterns** (defined in **presets**).
2. **Extracts** the required data for ingestion.
3. **Asks** the user to select the missing data if some are missing and optionnals.
4. **Simulates** the copy and renaming during a dry run in order to catch potential errors.
5. **Copies and renames** files to their pipelined destination.
6. **Registers** the newly copied files to the database.

## Information
- The presets are `.yml` files created and managed *per project* by a lead of a supervisor.
- For ingesting a fileset (sequence or single file), Bip needs:
  - **File type** (plate, texture, scene, render...)
  - **Item** (a shot, an asset...)
  - **Task** (compositing, modelling...)
  - **Variant** ("Main", custom text...)
- The tool can extract the following data from a filename:
  - Item
  - Task *(not yet implemented)*
  - Variant
  - Frame
  - Version *(not yet implemented)*
- **Folder names are not used** for data extraction and are ignored
- Depending on the preset pattern, **the tool can ask you to manually select the missing dat**a.
- When running the ingestion itself, the tool starts with a **dry run** in order to detect potential conflicts.
- The ingestion will:
  - Copy and rename the files at the right location, with a naming-convention formatted filename
  - Register the new files to the Bip database
- For now, the Ingestor will automatically **create a new incremented version** if the target already has some data.
- The preset can allow to manually edit the variant name

## Usage
:::caution
The Ingestor looks for presets for your **active project**.
:::

Select the type of file you are about to ingest and the filename pattern you want to use.

:::info
If you have a new filename pattern to ingest, you must ask a **supervisor** to create a preset for it.
:::

![](./assets/ingestor-01.png)

Drag and drop a selection of files or folders containing files (the inspector will recursively analyse all found files with the allowed extension (defined by the preset).

Any unmatching file will be listed in the post-inspection report:

![](./assets/ingestor-02.png)

Depending on your preset, you might have to complete some information by hand. Here, the pattern does not extract the task, so it must be selected manually:

![](./assets/ingestor-03.png)

Clink on Ingest when you are ready. The tool will start with a dry run in order to catch potential errors (writting permissions, already existing directories...). If the dry run was successfull, a confirmation dialog appears:

![](./assets/ingestor-04.png)

If you click Yes, the copy will start

:::danger
Careful! The copy operation can be very long. It is not recommended running this tool through VPN
:::

![](./assets/ingestor-05.png)

If everything went well, the report will show the path of all the newly ingested files

![](./assets/ingestor-06.png)
