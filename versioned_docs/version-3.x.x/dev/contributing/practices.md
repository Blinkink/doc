---
title: Practices
sidebar_position: 1
---

# Specifications
- **Commits**: https://www.conventionalcommits.org/en/v1.0.0/
- **Changelog**: https://keepachangelog.com/en/1.0.0/
- **Versioning**: https://semver.org/