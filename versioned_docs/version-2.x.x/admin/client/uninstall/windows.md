---
title: Windows
sidebar_position: 1
---
:::caution
There is not automated tool for removing Bip yet
:::


Remove the following files or directories:
- `C:\Users\<username>\AppData\Local\Blink`
- `C:\Users\<username>\AppData\Roaming\Microsoft\Windows\Start Menu\Programs\Startup\Bip Launcher.lnk`
- `C:\Users\<username>\AppData\Roaming\Microsoft\Windows\Start Menu\Programs\Bip Launcher.lnk`
- `C:\Users\<username>\Desktop\Bip Launcher.lnk`