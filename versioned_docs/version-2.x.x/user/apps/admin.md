---
title: Admin
---
:::caution
This application requires **administrator** privileges.
:::

<img height="300" src={require("../../assets/admin-versions.png").default} />


## Introduction

The **Admin** <img  width="20" height="20" src={require("../../assets/apps/admin.png").default} /> application is managing the server configuration. It is limited to admin-level users.

## Reference
Please see the administrator documentation about **[Server Configuration](../../admin/server/config.md)** for more details...