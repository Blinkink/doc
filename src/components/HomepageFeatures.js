import React from 'react';
import clsx from 'clsx';
import styles from './HomepageFeatures.module.css';

const FeatureList = [
  {
    title: 'Client',
    Svg: require('../../static/img/logos/client.svg').default,
    description: (
      <>
        The Bip client can be installed on all the users machine. It provides an API, <code>link</code> allowing to interact with the data server or create custom tools.
      </>
    ),
  },
  {
    title: 'Apps',
    Svg: require('../../static/img/logos/apps.svg').default,
    description: (
      <>
        Bip comes with a collection of useful applications, standalone or embedded in DCC such as a Launcher, Browser, a Project Manager, or a Scene Manager.
      </>
    ),
  },
  {
    title: 'Server',
    Svg: require('../../static/img/logos/server.svg').default,
    description: (
      <>
        A RESTful API, <code>sink</code> gives a direct access to the production data. The server can also execute operations on the production filesystem.
      </>
    ),
  },
  {
    title: 'Content',
    Svg: require('../../static/img/logos/content.svg').default,
    description: (
      <>
        Bip comes bare. It can be extended in order to implement a specific workflow, integration with a DCC or custom scene tasks.
      </>
    ),
  },
];

function Feature({Svg, title, description}) {
  return (
    <div className={clsx('col col--3')}>
      <div className="text--center">
        <Svg className={styles.featureSvg} alt={title} />
      </div>
      <div className="text--center padding-horiz--md">
        <h3>{title}</h3>
        <p>{description}</p>
      </div>
    </div>
  );
}

export default function HomepageFeatures() {
  return (
    <section className={styles.features}>
      <div className="container">
        <div className="row">
          {FeatureList.map((props, idx) => (
            <Feature key={idx} {...props} />
          ))}
        </div>
      </div>
    </section>
  );
}
