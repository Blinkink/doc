---
sidebar_label: data
title: bip.link.data
---

## `Data`

```python
class Data(BipObject)
```

Utility class for data entities.

Data entities are the hierarchical backbone of Bip: Project > Item > Component > Version.

This utility class groups all the methods that are shared for these four entities.

### `project`

```python
@property
def project() -> Project
```

Parent project.

**Returns**:

- `Project` - parent of the entity.

### `add_to`

```python
def add_to(group: Group)
```

Add the entity to a Group.

The Group's tag must allow the current entity type.

**Arguments**:

- `group` - Group: The Group to which the entity is added.
  

**Raises**:

- `ValueError` - If the operation is forbidden by the GroupTag rules.

### `get_container`

```python
def get_container(tag: Union[str, GroupTag]) -> List[Group]
```

Convenient shortcut for `get_containers()` returning a single Group.

Returns a single Group from a given GroupTag. This is useful when it is certain that
there is only one Group per tag that can contain the current Item.

**Examples**:

  ```python
  # In the case where
  example = link.project.get("example")
  shot0100 = example.get_item_by_slug("0100", 'shot', "demo-001")
  sequence = shot0100.get_container(tag="sequence")
  ```
  

**Raises**:

- `ValueError` - If the GroupTag accepts multiple memberships (an entity can be member of
  several Groups of the same GroupTag).
  

**Returns**:

- `Group` - Found Group.

### `get_containers`

```python
def get_containers(tag: Optional[Union[str, GroupTag]] = None) -> List[Group]
```

Convenient class implementation of: `bip.link.group.get_containers`.

:::caution
The signature of `Project.get_containers()` and `bip.link.group.get_containers`
are different since `Project.get_containers()` passes itself to the function as `project=self`.
:::

:::info
This method only returns Groups that contains the Project.
For getting the Groups that are owned (children) by the Project, `get_group()` or `get_groups()` must be used.
:::

### `get_group`

```python
def get_group(tag: Union[GroupTag, str], slug: Optional[str] = None, uid: Optional[str] = None) -> Group
```

Get a child Group from a GroupTag.

The tag can be provided as GroupTag object or a slug (`str`).

The method uses the `GroupTag.get_group()`

:::info
This method only returns Groups that are owned (children) by the Project.
For getting the Groups that contains the Project, `get_containers()`  must be used.
:::

**Arguments**:

- `tag` - Union[GroupTag, str]: GroupTag (or its slug) to search into.
- `slug` - Optional[str]: Slug of a Group. If specified, `uid` can be left blank. (Default value = None)
- `uid` - Optional[str]: Uid of a Group. If specified, `slug` can be left blank.  (Default value = None)
  

**Raises**:

- `LookupError` - No matching GroupTag or Group found.
- `ValueError` - No slug nor uid provided.
  

**Returns**:

- `Group` - Matching group.

### `get_groups`

```python
def get_groups(tag: Optional[Union[GroupTag, str]] = None) -> List[Group]
```

Get the children Groups from a GroupTag.

The tag can be provided as GroupTag object or a slug (`str`).

The method uses the `GroupTag.get_groups()`

:::info
This method only returns Groups that are owned (children) by the Project.
For getting the Groups that contains the Project, `get_containers()`  must be used.
:::

**Arguments**:

- `tag` - Union[GroupTag, str]: GroupTag (or its slug) to search into.
  

**Raises**:

- `LookupError` - No matching GroupTag or Group found.
  

**Returns**:

- `list` - Collection of Groups.

### `get_metadata`

```python
def get_metadata(definition: Optional[Union[str, Definition]] = None, existing_only: bool = False) -> Union[Metadata, List[Metadata]]
```

Convenient class implementation of the `link.metadata` getters.

- If `definition` is provided, using `bip.link.metadata.get_metadata`.
- Otherwise, using `bip.link.metadata.get_all_metadata`.

:::caution
The signature of `Data.get_metadata()` and `bip.link.metadata.get_metadata` or
`bip.link.metadata.get_all_metadata` are different since `Data.get_metadata()`
passes itself to the function as `entity=self`.
:::

### `set_metadata`

```python
def set_metadata(definition: Union[str, Definition], value)
```

Convenient class implementation of: `bip.link.metadata.set_metadata`.

:::caution
The signature of `Data.set_metadata()` and `bip.link.metadata.set_metadata`
are different since `Data.set_metadata()` passes itself to the function as `entity=self`.
:::

### `delete_metadata`

```python
def delete_metadata(definition: Definition)
```

Convenient class implementation of: `bip.link.metadata.delete_metadata`.

:::caution
The signature of `Data.delete_metadata()` and `bip.link.metadata.delete_metadata`
are different since `Data.delete_metadata()` passes itself to the function as `entity=self`.
:::

### `get_definition`

```python
def get_definition(slug: Optional[str] = None, uid: Optional[str] = None) -> Definition
```

Convenient class implementation of: `bip.link.metadata.get_definition`.

:::info
This method only returns Definitions that are compatible with the Project entity.
For getting all the Project Definitions, `get_all_definitions()` must be used.
:::

:::caution
The signature of `Data.get_definition()` and `bip.link.metadata.get_definition`
are different since `Data.get_definition()` passes itself to the function as `project=self`.
:::

### `get_definitions`

```python
def get_definitions() -> List[Definition]
```

Convenient class implementation of: `bip.link.metadata.get_definitions`.

:::info
This method only returns Definitions that are compatible with the Project entity.
For getting all the Project Definitions, `get_all_definitions()` must be used.
:::

:::caution
The signature of `Data.get_definitions()` and `bip.link.metadata.get_definitions`
are different since `Data.get_definitions()` passes itself to the function as `project=self`.
:::

