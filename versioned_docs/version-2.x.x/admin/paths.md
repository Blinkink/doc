---
title: Paths
sidebar_position: 6
---
## Client
- Default installation directory: `~/AppData/Local/Blink/`
- Config file: `~/AppData/Local/Blink/bip/bip/config.yml`
- Content repository: `~/AppData/Local/Blink/content`
- User directory: `~/.blink/`
- Authentication file: `~/.blink/auth.yml`


