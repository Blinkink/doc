---
title: Bip is going public
authors: [corentin]
date: 2021-07-12
tags: [documentation, open-source]
---

On its way to the future v3, where we aim at making our framework stable enough for external use, Bip is moving to healthier development practices, such as a well documented API, but also a complete and user-friendly documentation for each type of users.
