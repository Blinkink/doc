---
title: Batches
sidebar_position: 2
---
## Introduction
:::info
The Bip Launcher backend is [JeanPaulStart](https://github.com/cube-creative/jeanpaulstart), from Cube Creative. JeanPaulStart is a batch executor. Batches are redacted in YAML or JSON, in a way similar to Ansible playbooks.
:::

The batches are a way to execute a series of operation described in yaml. Typically, in CG production, they can be used to copy addons, modify config files, set environment variables before launching a DCC.


## Skeleton
:::note Specifications
- Filename: `*.yml`
- Location: `batches/`
:::
  

:::tip
The batch file inspection is recursive, so there can be as many subdirectories as needed.
:::

This is the minimal form of a Batch:

```yaml
---
uid: unique-id
name: Application name
description: Short text
version: x.y
os:
  - Windows
  - Linux
icon_path: $BIP_CONTENT/assets/icons/app.png
environment:
  BINARY_PATH: C:/Program Files/Editor/Software/application.exe
tasks:
- name: Running application
  raw:
    command: $BINARY_PATH
    async: yes
...
```

### Custom batches

It can be useful for developers who want to test their work without using an alternative Content repository to add custom and local batches. There are two ways to do it:
- Save them in `~/.blink/batches`.
- Set the environment variable `BIP_CUSTOM_BATCHES` to a folder containing your batch files.


## Parameters

### Canonic
These parameters are from JeanPaulStart
- **`name`**: Display name
- **`icon_path`**: Path to a png file
- **`tags`**: List of tags
- **`environment`** _(optional)_: List of environment variables
- **`tasks`**: List of tasks _([see below](#tasks))_

#### Environment
Using the environment feature can be very useful:
- Append your own Python packages to `PYTHONPATH`
- Set your ocio configuration file with `OCIO`
- For Nuke, use the `NUKE_PATH` variable to add your plugins and scripts

#### Existing environment variables
Bip sets a few environment variables. You can see [all of them here](../dev/misc/env-var), but some can be particularly useful for batches:
- `BIP_INTEGRATIONS`: Path to the internal Bip integration files for supported DCC
- `BIP_ICONS`: Path to the Bip assets directory
- `BIP_WORKING_DIRECTORY`: Path to the working directory 
- `BIP_CONTENT`: Path to the root of the content repository
- `BIP_USER_DIRECTORY`: Path to the user bip directory (*`~\.blink`*)

:::tip
- You can store your assets (icons) at the root of your content folder, so you can refer to them with `$BIP_CONTENT/assets/icons/file.png`
- You can vendor some resources (plugins, addons) at the root of your content folder, so you can refer to them with `$BIP_CONTENT/vendor/maya/an_addon`
:::

### Bip
These parameters are for Bip. They are all **mandatory**.
- **`uid`**: A unique identifier amongst all your batches
- **`description`**: A short description that will be shown in the Launcher's result view
- **`version`**: Software version
- **`os`**: `[Linux, Windows]` List of operating systems that can run the current batch
- **`limits`**:
    - **`users`**: List of usernames who can see the batch.
    - **`roles`**: List of roles which can see the batch.
    - **`privileges`**: List of privileges which can see the batch.
- **`host`**: `[none]` Used for BipApp applications plugins integrations. The product name of the DCC, if the batch starts a DCC. Any Bip application plugin with an integration batch matching this name will be loaded at the beginning of the current batch.

## Tasks
### Builtins
:::tip
These tasks are JeanPaulStart builtins, please read the [original documentation](https://github.com/cube-creative/jeanpaulstart#tasks) for more information.
:::
- `copy`: Copies a file
- `file`: Creates a file
- `include_tasks`: Runs another batch file
- `ini_file`: Modifies a config file (.ini)
- `pip`/`pip3`: Runs `pip install` / `pip3 install`
- `raw`: Run a command in the terminal
- `template`: Copy a file and applies Jinja2 templating, based on environment variables
- `url`: Opens the given url in the default browser

#### Copy
:::caution
Bip uses a modified version of the `copy` task
:::

- The support for directories has been added.
- If source is a directory, the destination must be a directory too and if the destination exists, is a file, an exception is raised.
- If destination is a non-existent path and if either destination ends with "/" or source is a directory, destination is created.
- If the source is a file, and the destination dirname is a non-existent path, dirname is created.

### Additional
Bip adds `git`, `delete` and `line_in_file` tasks.

#### Git
Allows to clone a public git repository to a specified location.

If not specified, `branch` is set to `master`.
```yaml
- name: Task name
  git:
    url: https://url.to/your/repository.git
    dest: /path/where/repo/will/be/cloned
    branch: branch
```

#### Delete
Deletes a directory or a file.

:::tip
If the path is a directory and finishes with a `/` or a `\`, only the content of the directory will be deleted, and the root folder will be preserved.
:::

```yaml
- name: Task name
  delete:
    path: /path/to/delete
```

#### Line In File
Allows to append a text to an editable file, after a searched value.
```yaml
- name: Task name
  line_in_file:
    filepath: /path/to/editable/file
    value: "text to look for"
    insert_after: "text to insert after found value"
    replace: [false|true]
```
## Example
This is a batch for **Modo**

```yaml
---
uid: modo
name: Modo
description: ''
version: 14.2v2
os:
  - Linux
bip:
  core_app: false
  context: standalone
  dcc: none
limits:
  users:
  - zinedine.zidane
  - michael.sardoo
  roles:
  - developper
  privileges:
  -admin
icon_path: $BIP_CONTENT/assets/icons/modo.png
environment:
  BINARY_PATH: /opt/Modo14.2v2/modo
  MODO_DIRECTORY: $HOME/.luxology
  OCIO: //studio/tech/ocio/aces/config.ocio
  PYTHONPATH:
  - $BIP_PYTHON_DCC_LIBS
  - $BIP_ROOT
tags:
  - CG
  - Foundry
tasks:
- name: Add custom Bip kits path from Modo config file
  line_in_file:
    filepath: $MODO_DIRECTORY/.modo14.2rc
    value: "  <import>user:Bip</import>"
    insert_after: "  <import>kits:</import>"
    replace: false
- name: Copying Bip integration kits
  copy:
    src: $BIP_INTEGRATIONS/modo/kits
    dest: $MODO_DIRECTORY/Bip/
    replace: yes
    force: yes
- name: Copying DeadlineModo kit
  copy:
    src: $BIP_CONTENT/vendor/modo/scripts/DeadlineModo
    dest: $MODO_DIRECTORY/Scripts/DeadlineModo/
    replace: yes
    force: yes
- name: Synchronizing Modo Toolbag
  git:
    url: https://git.blinkink.co.uk/inkubator/modo-toolbag.git
    dest: $MODO_DIRECTORY/Kits/Toolbag
    branch: master
- name: Running application
  raw:
    command: $BINARY_PATH
    async: yes
...
```