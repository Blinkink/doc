---
title: Introduction
sidebar_position: 1
---
Bip uses a custom git repository to tailor its usage. That repository is called **Content**.

Adding custom content to this repository is the designed method for tailoring Bip the way you prefer. You can choose what application can be started from the Launcher by writing your batch files, write your own tasks that will be loaded by the Scene Manager...

The content repository is mandatory, even if not used. It should be set up via the Admin app, when you create your Bip instance. _See the [server configuration](../admin/server/config) documentation page._

It can be hosted anywhere, but must be public and reachable from both _ssh_ and _https_.

On client machines, the **Content** repository is cloned by the **Client installer**, and checked out by the **Launcher**, when started, restarted (_builtin command_: `restart`) or when reloaded (_builtin command:_ `reload`).

## Requirements
- Public repository
- Reachable from both ssh and https
- At least a `master` branch

:::tip
By default, the branch checked out by the Launcher is `master`. But the Launcher will always try to match the current Bip (client) branch, and will fall back to `master` if not found. Therefore, in your content repository, you can create as many branches as you want (typically, a `develop` one) and if there is a match, the Launcher will use it.
:::

## Directories
Bip will be looking for specific directories:

| Folder name | Description                                                  |
| ----------- | ------------------------------------------------------------ |
| `commands`  | Custom Launcher commands                                     |
| `config`    | Config files                                                 |
| `custom`    | Applications customization files (UI presets, integrated documentation...) |
| `batches` | Batches `yml` files                                        |
| `preset`    | Various presets used by applications or scripts              |
| `python`    | If it exists, this directory is appended to the `PYTHONPATH`, it is useful if you need to easily share modules (for Tasks for example) |
| `tasks`     | DCC tasks read by the Scene Manager.                         |

### Recommended

In order to keep your repository tidy, it is recommended, but not necessary, to also create the following useful directories. Because the root of the content repository is stored in an environment variable (`BIP_CONTENT`, see [environment variables](../dev/misc/env-var), those locations would be easy to access from custom scripts.

| Folder name | Description                                                  |
| ----------- | ------------------------------------------------------------ |
| `assets`    | Store your icons and images                                  |
| `misc`      | Various files                                                |
| `vendor`    | Any external content you might want to ship, typically, DCC plugins or addons deployed by some launchers |