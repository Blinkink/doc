const lightCodeTheme = require('prism-react-renderer/themes/github');
const darkCodeTheme = require('prism-react-renderer/themes/dracula');

/** @type {import('@docusaurus/types').DocusaurusConfig} */
module.exports = {
  title: 'Bip',
  tagline: 'A flexible pipeline framework and toolset for animated productions',
  url: 'https://bip.blinkink.co.uk',
  noIndex: true,
  baseUrl: '/',
  onBrokenLinks: 'warn',
  onBrokenMarkdownLinks: 'warn',
  favicon: 'img/logo.svg',
  organizationName: 'blinkink', // Usually your GitHub org/user name.
  projectName: 'bip', // Usually your repo name.
  themeConfig: {
    announcementBar: {
      id: 'in-progress-1',
      content:
        '🚧 This documentation is <b>in progress</b>. The user applications manuals and the developer cookbooks are being redacted.',
      // backgroundColor: '#ed602c',
      // textColor: '#ffece0',
      isCloseable: true,
    },
    navbar: {
      title: 'Bip',
      logo: {
        alt: 'Bip logo',
        src: 'img/logo.svg',
      },
      items: [
        {
          type: 'doc',
          docId: 'user/intro',
          position: 'left',
          label: 'User',
        },
        {
          type: 'doc',
          docId: 'td/intro',
          position: 'left',
          label: 'TD',
        },
        {
          type: 'doc',
          docId: 'admin/intro',
          position: 'left',
          label: 'Administrator',
        },
        {
          type: 'doc',
          docId: 'dev/intro',
          position: 'left',
          label: 'Developer',
        },
          {
          type: 'docsVersionDropdown',
          position: 'right',
        },
        {to: '/blog', label: 'Blog', position: 'right'},
        {
          href: 'https://git.blinkink.co.uk/bip/',
          label: 'Git',
          position: 'right',
        },
      ],
    },
    footer: {
      style: 'dark',
      links: [
        {
          title: 'Docs',
          items: [
            {
              label: 'User',
              to: 'docs/user/intro',
            },
            {
              label: 'Technical Director',
              to: 'docs/td/intro',
            },
            {
              label: 'Administrator',
              to: 'docs/admin/intro',
            },
            {
              label: 'Developer',
              to: 'docs/dev/intro',
            },
          ],
        },
        {
          title: 'More',
          items: [
            {
              label: 'Dev Blog',
              to: '/blog',
            },
            {
              label: 'Client sources',
              href: 'https://git.blinkink.co.uk/bip/client',
            },
            {
              label: 'Server sources',
              href: 'https://git.blinkink.co.uk/bip/server',
            },
            {
              label: 'Apps sources',
              href: 'https://git.blinkink.co.uk/bip/apps',
            },
          ],
        },
        {
          title: 'Blinkink',
          items: [
            {
              label: 'Website',
              href: 'https://blinkink.co.uk/',
            },
            {
              label: 'Linkedin',
              href: 'https://www.linkedin.com/company/blinkink/mycompany/',
            },
            {
              label: 'Instagram',
              href: 'https://www.instagram.com/blink_ink',
            },
            {
              label: 'Blink',
              href: 'https://blinkprods.com/',
            },
          ],
        },
      ],
      copyright: `Copyright © ${new Date().getFullYear()} Bip, Blinkink.`,
    },
    prism: {
      theme: lightCodeTheme,
      darkTheme: darkCodeTheme,
    },
  },
  presets: [
    [
      '@docusaurus/preset-classic',
      {
        docs: {
          lastVersion: 'current',
          versions: {
            current: {
              label: '4.0.0',
              path: 'current',
            },
          },
          sidebarPath: require.resolve('./sidebars.js'),
          editUrl:
            'https://git.blinkink.co.uk/-/ide/project/bip/doc/edit/master/-/',
        },
        blog: {
          showReadingTime: true,
          editUrl:
            'https://git.blinkink.co.uk/-/ide/project/bip/doc/edit/master/-/blog/',
          blogSidebarCount: 'ALL',
          blogSidebarTitle: 'All our posts',
        },
        theme: {
          customCss: require.resolve('./src/css/custom.css'),
        },
      },
    ],
  ],
};
