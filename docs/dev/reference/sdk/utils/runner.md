---
sidebar_label: runner
title: bip.sdk.utils.runner
---

## `run`

```python
def run(app, context=STANDALONE, quit_at_close=True, *args, **kwargs)
```

Run an SDK application.

:::info
The runner makes sure the application is compatible with HDPI scaling.
:::

The runner provides a clean and complete application initialization:
- Generate a logger and pass it to the application instance.
- Get the Plugin object associated with the application.
- If the context is set to `wink.STANDALONE`, attempt to get an existing
QApplication instance, and create one if not found.
- Instantiate the application, providing the `context` and `logger` arguments.
- Execute the application.
- Execute the QApplication.

The runner catches any top-level exception uncaught within the application,
issues them as a Bip dialog and then exits.

If BIP_DEBUG_PLUGIN is on, a temporary PluginSource and Plugin object is generated,
allowing to work without needing to register and enable the plugin on the database.

**Arguments**:

- `app` _App_ - Application class. It must not be instanced.
- `context` _str_ - Execution context. Accepted values are `wink.STANDALONE` and `wink.HOSTED`.
- `quit_at_close` _bool_ - If True, the QApplication is destroyed when the main
  widget is closed. Defaults to `True`.
- `*args` - Any argument to be forwarded to the application at instantiation.
- `**kwargs` - Any keyword argument to be forwarded to the application at instantiation.
  

**Returns**:

- `App` - Application instance.

