---
sidebar_label: task
title: bip.plugins.sgink.task
---

## from\_bip\_tag

```python
def from_bip_tag(task_tag, item)
```

Get a Shotgrid task from an Bip task tag and a Bip item.

**Arguments**:

- `task_tag` _TaskTag_ - TaskTag BipObject.
- `item` _Item_ - Item BipObject.
  

**Returns**:

- `dict` - A dictionary with the Shotgrid Task id and content.

## log\_time

```python
def log_time(duration, project, task, user)
```

Create a Timelog for a given task.

**Arguments**:

- `duration` _int_ - Time in minutes.
- `project` _dict_ - Dictionary with the Shotgrid Project id.
- `task` _dict_ - Dictionary with the Shotgrid Task id.
- `user` _dict_ - Dictionary with the Shotgrid HumanUser id.

