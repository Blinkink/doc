---
title: Administration
sidebar_position: 4
---

## Users

![image](../assets/admin_user_view.png)

![image](../assets/admin_user_edit.png)

## Projects

:::caution In Progress
This view is not yet implemented!
:::

## Content

:::caution In Progress
This view is not yet implemented!
:::

## Incidents

:::caution In Progress
This view is not yet implemented!
:::

## Announcements

![image](../assets/admin_announcements.png)

## Toolbox

![image](../assets/admin_toolbox.png)

## Plugins

### Manage
![image](../assets/admin_plugin_manage_view.png)
![image](../assets/admin_plugin_manage_enable.png)
![image](../assets/admin_plugin_manage_edit.png)

### Registry
![image](../assets/admin_plugin_registery_view.png)
![image](../assets/admin_plugin_registery_edit.png)

## Configuration

![image](../assets/admin_config.png)