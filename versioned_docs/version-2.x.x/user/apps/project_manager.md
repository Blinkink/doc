---
title: Project Manager
---

:::danger Work in progress
This documentation is not yet **redacted**.
:::

<img height="300" src={require("../../assets/project-manager-manage-list.png").default} />


## Introduction

The **Project Manager** <img  width="20" height="20" src={require("../../assets/apps/project_manager.png").default} /> is the main data handling application. It allows viewing, creating, deleting and editing projects, items (such as assets, shots), groups (such as sequences or asset categories) or metadata.

### Features

### Future

## Usage
### Create a project
![](../../assets/project-manager-projects-list-full.png)

![](../../assets/project-manager-create-project-settings-general.png)

![](../../assets/project-manager-create-project-settings-tracker-internal.png)

![](../../assets/project-manager-create-project-settings-tracker-shotgrid.png)

### Create group
![](../../assets/project-manager-create-group-empty.png)

![](../../assets/project-manager-create-group-filled.png)

### Create an item
![](../../assets/project-manager-create-item-valid.png)

![](../../assets/project-manager-create-item-not-valid.png)

## Reference
### Overview
![](../../assets/project-manager-overview.png)

### Manage
![](../../assets/project-manager-manage-list.png)

![](../../assets/project-manager-edit-item.png)

### Metadata
![](../../assets/project-manager-metadata-list.png)

![](../../assets/project-manager-metadata-edit.png)

### Members
![](../../assets/project-manager-members-heads.png)

### Project settings
![](../../assets/project-manager-edit-project-settings-general.png)

![](../../assets/project-manager-edit-project-settings-tracker-shotgrid.png)

### Settings
![](../../assets/project-manager-settings.png)
