---
title: Structure
sidebar_position: 3
---
## Client
- **bip**: API
  - **link**: _Data_ - Interact (CRUD) with the database (Neo4j)
  - **mink**: _Tools_ - Frontend APIs, utilities, special processes, and internal scripts
  - **pink**: _Config_ - Local settings, login, environment variables...
  - **wink**: _Utils_ - Constants, paths and exceptions
  - **uink**: _User interface_ - Custom widgets, Qt wrapper, stylesheet manager...
  - **plugins**: Official _production trackers_ or _DCC_ wrappers
    - **sgink**: Shotgrid
    - **nkink**: Nuke
    - **myink**: Maya
  - **toolkit**: SDK - Application template
  - **utils**: Non-Bip utility modules (git, os-specific, formatting, decorators...)
- **assets**: Images, icons, stylesheets...
- **builtins**: Essential commands and batches
- **integrations**: Official extensions (Maya, Nuke, Shotgrid)
- **vendor**: External dependencies (ffmpeg)


## Server
- **bop**: API
  - **sink**: RESTful data API interacting with Neo4j instance
  - **kink**: Filesystem agent
  - **fink**: Event listenner