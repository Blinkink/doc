---
title: Offline mode
sidebar_position: 3
---

## Introduction
You might have security requirements that forces you to run you Bip instance disconnected.
In that case you will have to make sure the following are offline:
- Bip server
- Git repositories (for Bip and Content updates)
- Python package repository (used by `pip` for installation and updates)

## Bip server
Host your Bip server on a local machine, reachable with a FQDN.

## Git repositories
Host a Git repository manager, like Gitea (lightweight), reachable with a FQDN.
- If this server is allowed to access internet, it can simply mirror the official repositories.
- Otherwise, you must clone the repositories and work detached from official.

## Python package repository
Bip uses `pip` both at install and updates. The best solution for offline is to use `pip2pi` to build an offline repository.

:::caution
You have to make sure that the repository content matches the `requirements.txt` and `requirements_dcc.txt` from Bip client, and also includes any `api_repository` requested by your activated plugins.
:::

### pip2pi
First let's locally create the repository from a machine that has Internet access.
```bash
mkdir python-packages

# Copy needed archives
pip2tgz python-packages/ -r /path/to/a/cloned/bip/client/requirements.txt
pip2tgz python-packages/ -r /path/to/a/cloned/bip/client/requirements_dcc.txt
pip2tgz python-packages/ -r git+git://github.com/shotgunsoftware/python-api.git # If using Shotgrid

# Build index
dir2pi python-packages/ --no-symlink # If on Windows, avoid symlinks
```

Then you can copy this `python-packages` to a network location that will be accessible by any machine that will use Bip.

Finally, edit the server config by enabling the Offline mode, and setting the Python package repository starting with `file://`, like `file:///network/location/to/python-packages`.