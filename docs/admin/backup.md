---
title: Backup
sidebar_position: 4
---
:::caution
There is no internal tool for backup. You must use Neo4j's. If you are using Neo4j Community, you will have to turn off your database first.
:::

- **Neo4j Community**: [Back up an offline database](https://neo4j.com/docs/operations-manual/current/backup-restore/offline-backup/)
- **Neo4j Enterprise**: [Back up an online database](https://neo4j.com/docs/operations-manual/current/backup-restore/online-backup/)