---
title: Project Manager
---
## Help text
:::tip What?
If you want to display a custom help text at item/project creation/edition, you can create an html file that will show in the sidebar of the creation dialog.
:::


![Help text at creation](./assets/project-manager-help-text.png)

### How to
- In your Content repository, create the following folders: `custom/project_manager/`.
- In this directory, you can create a html file following this filename rule:
- `[create|edit]_[item|group|project](_{item-tag}).html.`
  - `create_project.html`
  - `create_item_asset.html`
  - `edit_group_shot.html`

### Example
#### `create_item_asset.html`

```html
<h3>Blinkink naming convention</h3>
An asset name must be made of at least <b>one word</b> and <b>one letter</b>.
The identifier letter is used to differentiate variants.
The first id should normally be "A".
The folder name must not contain spaces or any symbol.
The case should be CamelCase
<h4>Examples:</h4>
<ul>
<li>Asset A</li>
<li>LongCharacterName A</li>
<li>Phone A</li>
<li>Bob A</li>
<li>Bob B</li>
</ul>
```

## Task presets
:::tip What?
You can create tasks preset for each item tags in your project.
:::

![Task presets](./assets/project-manager-tasks.png)

### How to
- In your Bip content repository, create the following folders: `presets/projects/tasks/`.
- In this directory, you can two types of directories:
  - **`global`**: Will be used as the default preset if no project-specific presets are found
  - **`{project-folder-name}`**: Will be used instead of `global` if found. (eg: `*INK493_DoorDash2*`)

- In the previously created directory, you can create a yml file following this filename rule: **`{item-tag}.yml`**
  - *`asset.yml`*
  - *`document.yml`*
- The content of the yml file must follow this structure:
```yml
default: [None|Preset name]
presets:
	Preset name:
    - Task 1 name
    - Task 2 name
    - ...
```

:::info
Case doesn't matter for the tasks name: both `Lighting` and `lighting` would be recognized if a Lighting task existed for the project.
:::

### Example
#### `asset.yml`

```yml
default: None
presets:
  3d Asset:
  - Texturing
  - Rigging
  - Modelling
  - Lookdev
  2d Asset:
  - Rotoscoping
  - Cleanup
  - Texturing
```