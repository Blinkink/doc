---
title: Renderer
---

:::danger Work in progress
This documentation is not yet **redacted**.
:::

<img height="300" src={require("../../assets/renderer-base.png").default} />


## Introduction
:::caution Experimental project
- This application is meant to be merged to the **Hypervisor** application.
- It is only available for **Modo**.
- Only works with **Deadline**.
- This application is **not maintained any more**.
:::

The **Renderer** <img  width="20" height="20" src={require("../../assets/apps/renderer.png").default} /> is a render pass manager and render submitter for Modo. It provides auto-pathing, versioning, output selection and an override system.

### Features

### Future

## Usage
![](../../assets/renderer-base.png)

![](../../assets/renderer-submission.png)

## Reference
![](../../assets/renderer-base-data.png)

## Data
![](../../assets/renderer-base-no-data.png)

![](../../assets/renderer-data-mismatch.png)

### Pass
![](../../assets/renderer-pass.png)

![](../../assets/renderer-pass-outputs.png)

### Presets
![](../../assets/renderer-new-preset.png)

### Settings
![](../../assets/renderer-override-tile-rendering.png)


### Overrides
![](../../assets/renderer-override-format.png)

![](../../assets/renderer-override-framerange.png)

![](../../assets/renderer-override-resolution.png)

![](../../assets/renderer-override-camera.png)

![](../../assets/renderer-override-camera-settings.png)

