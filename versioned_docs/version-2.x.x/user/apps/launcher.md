---
title: Launcher
---

:::danger Work in progress
This documentation is not yet **redacted**.
:::

<img height="300" src={require("../../assets/launcher-search-batch.png").default} />


## Introduction

The **Launcher** <img  width="20" height="20" src={require("../../assets/apps/launcher.png").default} /> is the backbone of the Bip client. It allows to start your favourite applications and get them set up (default settings, addons, Bip integrated tools...) automatically. It ensures the Bip tools are up-to-date and properly integrated in your favourite applications.

### Features
- **Updates**: The Launcher ensures the user is using the correct version Bip, based on the user settings, server settings and project settings (for example, a project can restrict a certain version of Bip, for stability reasons).
- **Commands**: TD can write custom commands that can be executed from the Launcher.
- **Batches**: TD can write batches, which are configuration files describing what to do before starting an application: installing/update addons, changing default parameters...
- **Login**: The Launcher allows login in and login out from Bip.

### Future

## Usage
### First start
![](../../assets/launcher-login.png)

![](../../assets/launcher-login-wrong-username.png)

![](../../assets/launcher-login-project-picker-selected.png)

![](../../assets/launcher-splash.png)

![](../../assets/launcher-welcome-1.png)

### Start an application
![](../../assets/launcher-search-batch.png)

### Execute a command
![](../../assets/launcher-search-command.png)

![](../../assets/launcher-command-set-project-1.png)

![](../../assets/launcher-command-set-project-2.png)

![](../../assets/launcher-command-set-source.png)

## Reference
![](../../assets/launcher-base-without-tooltip.png)

### Tooltips
![](../../assets/launcher-base-with-tooltip.png)


### Announcements
![](../../assets/launcher-announcement-info.png)

![](../../assets/launcher-announcement-warning.png)

### Settings
![](../../assets/launcher-settings-menu.png)

### App grid
![](../../assets/launcher-app-grid-1.png)

![](../../assets/launcher-app-grid-2.png)

### Updates
![](../../assets/launcher-forced-update.png)

![](../../assets/launcher-optional-update.png)

![](../../assets/updater.png)

![](../../assets/launcher-new-version.png)


### Special views
#### Welcome
![](../../assets/launcher-welcome-2.png)

#### Help
![](../../assets/launcher-help.png)

#### User
![](../../assets/launcher-user-card.png)

#### Changelogs
![](../../assets/launcher-changelogs.png)


### Shortcuts
![](../../assets/launcher-all-commands.png)


### Bubble
![](../../assets/launcher-bubble.png)


### Settings
![](../../assets/launcher-settings-1.png)




