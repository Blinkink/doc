---
sidebar_label: app
title: bip.sdk.app
---

## `App`

```python
class App(_Plugin)
```

Bip Application class.

The App class provides convenient methods for creating a well-integrated
Bip application. It comes with the following features:
- Easy access to Bip data via `link`.
- Bip-flavored window embedment, using Bip Qt stylesheet.
- Threaded loading.
- Support for **hosted** and **standalone** execution contexts.
- Bip widgets (MessageBox, Notifications...).
- Hosted address retrieving (see `get_address()` method).
- Preset logger.

The recommended way to create a Bip application is to subclass and extend
the App class. Then the instantiation of the App is handled
by the sdk runner, which also executes the application.

:::warning
Unless you are debugging,the subclassed application must be instantiated by the sdk runner.
:::

This class provides special methods to override, such as `init()` and
`load()`, which are respectively called during the threaded
initialization (`init()`), and immediately after (`load()`)


When executed, the application will automatically perform the following actions:
- Instance arguments validity
- Check connection with database
- Retrieve the current user
- Retrieve the user's active project
- Retrieve the server configuration

**Examples**:

  ```python
  from bip.sdk import App, run
  from myapp.ui import Widget
  from myapp.api import get_data
  
  
  class SampleApp(App):
  name = "Sample"
  slug = "sample"
  version = "v0.0.1"
  
  def __init__(self, *args, **kwargs):
  super().__init__(*args, **kwargs)
  self.widget = Widget()
  self.set_widget(self.widget)
  
  self.data = None
  
  def init(self, callback):
  # Executed at initialization (threaded).
  self.data = get_data()
  
  def load(self):
  # Executed after initialization.
  self.widget.load(self.data)
  
  app = run(SampleApp)
  ```
  
  See the demo Chocolatine application for a complete example: https://git.blinkink.co.uk/bip/apps/chocolatine.
  

**Todo**:

  **Find a better error handling system.** For now, common errors like
  PermissionError and Connection error are handled in two different ways:
  - When not threaded: with from @error_handler (which requires the
  widget to have self.app).
  - When threaded: with the Worker `failed` signal.
  

**Attributes**:

- `resizable` _bool, optional_ - (class attribute) If True, the window can be resized. Defaults to `False`.
- `closable` _bool, optional_ - (class attribute) If True, the window can be closed. Defaults to `True`.
- `minimizable` _bool, optional_ - (class attribute) If True, the window can be minimized. Defaults to `True`.
- `maximizable` _bool, optional_ - (class attribute) If True, the window can be maximized. `resizable` must be set to `True`. Defaults to `False`.
  the application loading is interrupted and an explanatory message is prompted. Defaults to `False`.
- `use_header` _bool, optional_ - (class attribute) If True, the default Bip application window header is used. Defaults to `True`.
- `floating_header` _bool, optional_ - (class attribute) If True, the window header overlaps the main widget. Defaults to `False`.
- `supported_contexts` _list, optional_ - (class attribute) List of supported contexts for the application. Allowed context are:
  - `bip.wink.HOSTED`: The application can be executed from a host (DCC) runtime.
  - `bip.wink.STANDALONE`: The application can be executed as a standalone, using Bip default Python runtime.
  - `bip.wink.REMOTE`: The application is executed as a standalone from a host (DCC), using Bip default Python runtime.
  name
- `context` _str_ - Current execution context. Accepted values are `bip.wink.HOSTED` and `bip.wink.STANDALONE`.
- `logger` _logging.Logger_ - Application logger. If the app has been executed with the sdk runner, the logger is handled by `bip.mink.logs`.
- `host` _bip.sdk.plugin.Host_ - If the application is hosted or remote, Host plugin of the current DCC, else None.
- `current_user` _bip.link.user.User_ - Currently logged in Bip user.
- `active_project` _bip.link.project.Project_ - Active project of the currently logged in Bip user.
- `config` _dict_ - Plugin configuration dictionary, if the plugin descriptor defines a config template.
- `mapping` _dict_ - Plugin mapping dictionary, if the plugin descriptor defines a mapping template.
- `default_settings` _dict_ - Plugin default settings dictionary, if the plugin descriptor defines a setting template.
- `settings` _dict_ - Plugin user settings dictionary, if the plugin descriptor defines a setting template.

### `__init__`

```python
def __init__(plugin, logger, context, host=None, silent=False, stylesheet=None)
```

Initializes the application based on the given context.

:::caution
If you extend the `__init__` method behaviour, bear in mind that it
is run before the application gets executed ('execute()'). That means:
- The window is not yet shown, and any long operation ran there would be invisible, which is a bad UX design.
- The connection with the database has not yet been verified, which
means that any `link` call performed here could lead to an uncaught exception.
- The attributes `current_user`, `active_project` and `config` are still undefined.
:::

**Arguments**:

- `context` _str_ - Execution context. Accepted values are `wink.STANDALONE` and `wink.HOSTED`.
- `host` _str_ - If the application is hosted, id of the host plugin to be run.
- `silent` _bool_ - If `True`, the window is not shown after execution. Defaults to `False`.

### `silent`

```python
@property
def silent()
```

Is the application silent.

If the application has been set to silent, is it running without showing the window.

**Returns**:

- `bool` - True if silent, False otherwise.

### `hosted`

```python
@property
def hosted()
```

Is the application hosted.

The application is hosted if the application is executed from a DCC runtime.

**Returns**:

- `bool` - True if hosted, False otherwise.

### `stylesheet`

```python
@property
def stylesheet()
```

Complete stylesheet.

The complete compiled application stylesheet. It includes the SDK stylesheets,
the bip.uink stylesheets as well as the custom stylesheet if any.

**Returns**:

- `str` - Raw stylesheet.

### `last_used_version`

```python
@property
def last_used_version()
```

Last version used.

Gives the latest version of the application that has been successfully started on the current machine.
If `BIP_DEBUG_PLUGIN` is on, nothing is returned

**Returns**:

- `str` - Version number.

### `execute`

```python
def execute()
```

Execute (start) the application.

Executing the application shows the window if the application is not silent,
and performs the internal and custom initialization operation.

:::caution
This can only be run once.
:::

### `init`

```python
def init(callback)
```

Overridable method for executing operation during initialization.

This is the recommended way to perform startup operation, such as data retrieving.
It is particularly encouraged if this operation is time-consuming.

This method is executed at the end of the threaded loading, which means that at this stage, the application
window is shown and the loading overlay is visible, giving the user some visual feedback on
what is going on.

The callback can be used to update the text of the loading overlay.

:::info
Raising exception is the recommended way to properly interrupt the initialization.
:::

**Examples**:

  ```python
  def init(self, callback):
  callback("Loading data")
  data = my_api.get()
  
  callback("Inspect data")
  if not my_api.inspect():
  raise ValueError("The is are not valid")
  ```
  

**Arguments**:

  callback (func(`str)): Updates the loading overlay with a custom text. The callback is passed to the method by the execution thread.

### `load`

```python
def load()
```

Overridable method for executing operation after initialization.

This is the recommended way to perform post-initialization operation. This is
executed after the `init()` method, and the main difference with `init()` is
that `load()` is not threaded.

:::tip
The execution of this method is done is the main thread, which means it can be used to update the UI safely.
:::

### `set_widget`

```python
def set_widget(widget)
```

Set the main widget of the window.

**Arguments**:

- `widget` _QWidget_ - Main widget.

### `set_window_size`

```python
def set_window_size(height, width, centered=False)
```

Set the window size.

**Arguments**:

- `width` _int_ - Width in pixels.
- `height` _int_ - Height in pixels.
- `centered` _bool_ - If True, the window will be centered relatively to its previous position. Defaults to `False`.

### `set_title`

```python
def set_title(title)
```

Set the header title.

**Arguments**:

- `title` _str_ - Header title.

### `set_window_title`

```python
def set_window_title(title)
```

Set the window title.

**Arguments**:

- `title` _str_ - Window title.

### `show`

```python
def show()
```

Show the window.

### `hide`

```python
def hide()
```

Hide the window.

### `close`

```python
def close()
```

Close the window.

### `center`

```python
def center()
```

Center the window.

### `adjust_size`

```python
def adjust_size()
```

Adjust the window size.

### `get_changelog`

```python
def get_changelog()
```

Get the extracted changelog.

If the package root file CHANGELOG.md is correctly formatted,
a dict of the changelog for each version is returned.

The dictionary is structured like so:

```python
{"v0.0.1":
    {
        "date": _current_date,
        "url": _current_url,
        "content": content
    },
...
}
```

**Returns**:

- `dict` - Collection of changelog data.

### `quit`

```python
def quit()
```

Quit the application.

### `notify`

```python
def notify(text, subtext=None, level=NEUTRAL, position=BOTTOM, auto_close=5)
```

Show an overlay notification.

:::warning
**Experimental feature**: does not seem to work in hosted context.
:::

NEUTRAL = NEUTRAL
ERROR = ERROR
WARNING = WARNING
INFO = INFO
SUCCESS = SUCCESS

__Position__

TOP = TOP
MIDDLE = MIDDLE
BOTTOM = BOTTOM

**Arguments**:

- `text` _str_ - Text of the notification.
- `subtext` _str, optional_ - Additional text.
- `level` _str, optional_ - Level of the notification. Accepted values are
  `wink.NEUTRAL`, `wink.ERROR`, `wink.WARNING`, `wink.INFO`, `wink.SUCCESS`. Defaults to `NEUTRAL`.
- `position` _str, optional_ - Vertical alignment. Accepted values are `wink.TOP`, `wink.MIDDLE`, `wink.BOTTOM`.
- `auto_close` _int, optional_ - Auto-close delay value, in seconds. If set to 0, auto-close is disabled. Defaults to 5.

### `show_changelog`

```python
def show_changelog()
```

Show the changelog popup.

### `popup`

```python
def popup(widget, escape_allowed=True, escape_callback=None, expand=False, margin=None, transparent=False)
```

Add a popup widget.

A popup widget is a centered overlay on top of the main window. A typical use case is for modal dialogs.

**Arguments**:

- `widget` _QWidget_ - Widget to place in the overlay.
- `escape_allowed` _bool, optional_ - If True, the overlay can be closed by clicking outside. Defaults to `True`.
- `escape_callback` _func, optional_ - If provided and if `escape_allowed`
  is True, the callback gets called if the overlay is escaped.

### `close_popup`

```python
def close_popup()
```

Close the currently opened popup overlay.

### `show_loading`

```python
def show_loading(title, subtext=None)
```

Show a loading overlay.

**Arguments**:

- `title` _str_ - Title of the overlay.
- `subtext` _str, optional_ - Subtext (description) of the overlay.

### `close_loading`

```python
def close_loading()
```

Closes the currently opened loading overlay.

### `dialog`

```python
def dialog(title, description, details=None, level=NEUTRAL, mode=Dialog.SINGLE, choices=None, closable=True, overlay=True, hidden_details=False, log=True)
```

Shows a message dialog.

:::tip
- If an Exception is provided to the `details` argument,
its traceback is automatically retrieved and added to the details text.
- Any dialog message is automatically logged into the application logger. This can
be deactivated by setting the log argument to False.
:::

**Examples**:

  Standard:
  ```python
  self.dialog(
  title="Hello world",
  description="This is a dialog demonstration.",
  level=Dialog.INFO
  )
  ```
  
  Double choices:
  ```python
  title = "Forbidden operation"
  description = (
  "It is impossible to save this scene because you don't own this scene, "
  "do you want to save a new version?"
  )
  result = self.dialog(
  title=title,
  description=description,
  level=Dialog.WARNING,
  mode=Dialog.DOUBLE,
  choices=Dialog.YES_NO,
  )
  if result:
  save_up()
  ```
  
  
  Multiple choices:
  ```python
  title = "Save changes?"
  description = "The current scene has been modified, what do you want to do?"
  choices = {"Save": "save", "Ignore": "ignore", "Cancel": "cancel"}
  result = self.dialog(
  title=title,
  description=description,
  mode=Dialog.MULTIPLE,
  level=Dialog.WARNING,
  choices=choices,
  )
  if result == "save":
  save()
  elif result == "cancel":
  return
  ```
  

**Arguments**:

- `title` _str_ - Title of the dialog message.
- `description` _str, optional_ - Description of the dialog message.
- `details` _str or Exception, optional_ - Additional details displayed in a
  scrollable text box. Suitable for long texts such as error reports. If
  an Exception object is provided, the traceback is automatically
  retrieved.
- `level` _str, optional_ - Level of the dialog. Accepted values are
  `Dialog.NEUTRAL`, `Dialog.ERROR`, `Dialog.WARNING`, `Dialog.INFO`, `Dialog.SUCCESS`. Defaults to `NEUTRAL`.
- `mode` _str, optional_ - Level of the dialog. Accepted values are
  `Dialog.SINGLE`, `Dialog.DOUBLE`, `Dialog.MULTIPLE`. Defaults to `SINGLE`.
  - SINGLE: One validation button. If no `choices` specified, "ok" button.
  - DOUBLE: Two validation buttons. If no `choices` specified, "ok/cancel" buttons.
  - MULTIPLE: Multiple validation buttons. If used, a `choice` dictionary must be provided.
- `choices` _str or dict, optional_ - Button(s) choice(s). They can be a hint or a custom dictionary.
  - Hint: Accepted values are `Dialog.OK` and `Dialog.PROCEED` if the
  mode is set to `Dialog.SINGLE`, `Dialog.YES_NO` and
  `Dialog.OK_CANCEL` if the mode is set to `Dialog.DOUBLE`
  - Custom: A dictionary with the display texts as keys, and the returned values as values.
- `closable` _bool, optional_ - If `True`, the dialog can be closed normally,
  otherwise, a choice must be validated. Defaults to `True`.
- `overlay` _bool, optional_ - If `True`, the dialog is a modal of the main
  window, otherwise it is a regular window. Caution, if set to `True`, the
  application window must be visible. Defaults to `True`.
- `hidden_details` _bool, optional_ - If `True` and if `details` are
  provided, the details text box is collapsed by default. Defaults to `False`.
- `log` _bool, optional_ - If `True`, the message is automatically logged,
  with all the details provided. Defaults to `True`.
  

**Raises**:

  TypeError:
  - `title` is not a string.
  - `description` is not a string.
  
  ValueError:
  - `mode` is invalid.
  - `level` is invalid.
  - `choices` is not valid (unknown hint or wrong length)

### `error_dialog`

```python
def error_dialog(title, description, details=None, mode=Dialog.SINGLE, choices=None, closable=True, overlay=True, hidden_details=False)
```

Convenient preset shortcut to `App.dialog()`.

Level is set to `Dialog.ERROR`.

### `warning_dialog`

```python
def warning_dialog(title, description, details=None, mode=Dialog.SINGLE, choices=None, closable=True, overlay=True, hidden_details=False)
```

Convenient preset shortcut to `App.dialog()`.

Level is set to `Dialog.WARNING`.

### `info_dialog`

```python
def info_dialog(title, description, details=None, mode=Dialog.SINGLE, choices=None, closable=True, overlay=True, hidden_details=False)
```

Convenient preset shortcut to `App.dialog()`.

Level is set to `Dialog.INFO`.

### `success_dialog`

```python
def success_dialog(title, description, details=None, mode=Dialog.SINGLE, choices=None, closable=True, overlay=True, hidden_details=False)
```

Convenient preset shortcut to `App.dialog()`.

Level is set to `Dialog.SUCCESS`.

### `neutral_dialog`

```python
def neutral_dialog(title, description, details=None, mode=Dialog.SINGLE, choices=None, closable=True, overlay=True, hidden_details=False)
```

Convenient preset shortcut to `App.dialog()`.

Level is set to `Dialog.NEUTRAL`.

### `confirm_dialog`

```python
def confirm_dialog(title, description, details=None, mode=Dialog.DOUBLE, choices=None, closable=True, overlay=True, hidden_details=False)
```

Convenient preset shortcut to `App.dialog()`.

Level is set to `Dialog.WARNING` and mode is set to `Dialog.DOUBLE`.

### `get_projects`

```python
def get_projects()
```

Convenient shortcut to `link.project.get()

### `get_users`

```python
def get_users()
```

Convenient shortcut to `link.user.get()

### `get_config`

```python
def get_config()
```

Convenient shortcut to `link.config.get()

### `get_address`

```python
def get_address()
```

Returns a pink.Address object.

If the application is hosted, the Address object inspects the file currently
loaded in the host, if any, and returns its Bip representation, if the file is a tracked Version.

:::info
See the documentation of `pink.Address` for further information.
:::

**Returns**:

- `Address` - Address object from the file currently loaded in the host.
- `None` - If the application is not hosted.

### `refresh_current_user`

```python
def refresh_current_user()
```

Force get currently logged-in user.

It can be of use if an application is sleeping in the
background and the current user could have changed between two calls.

### `refresh_active_project`

```python
def refresh_active_project()
```

Force get currently logged-in user's active project.

It can be of use if an application is sleeping in the
background and the active project could have changed between two calls.

### `remote_order`

```python
def remote_order(order)
```

Overridable method for interpreting remote orders.

If a remote application needs to receive user instructions from the remote host during runtime,
those orders, sent with `mink.remote.send()`, would be sent to this method.

Overriding with a case per order is the recommended way to apply those orders to the application.

