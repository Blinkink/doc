---
sidebar_label: version
title: bip.plugins.sgink.version
---

## get\_all

```python
def get_all(item)
```

Get all versions linked to a given item.

**Arguments**:

- `item` _dict_ - Dictionary with the Shotgrid Item id.
  

**Returns**:

- `list` - Collection of Shotgrid entity dictionaries.

## create

```python
def create(user, project, item, **kwargs)
```

Create a version for a given item.

Any extra supported field will be caught by kwargs.
For harmonious naming convention, fields starting with `sg_`
can also be understood without this prefix.

Accepted optional fields:
- description
- code
- task (or sg_task)
- status_list (or sg_status_list)
- path_to_frames (or sg_path_to_frames)
- path_to_movies (or sg_path_to_movies)

**Arguments**:

- `user` _dict_ - Dictionary with the Shotgrid HumanUser id.
- `project` _dict_ - Dictionary with the Shotgrid Project id.
- `item` _dict_ - Dictionary with the Shotgrid Item id.
- `**kwargs` - Any supported optional fields.
  

**Returns**:

- `dict` - Shotgrid entity dictionary with `type` and `id` key/value pairs.

## upload\_file

```python
def upload_file(version, file_path)
```

Upload a movie to an existing version.

The file is attached to the `sg_uploaded_movie` of the version.

**Arguments**:

- `version` - Shotgrid entity dictionary with at list an `id` key/value pair.
- `file_path` - Path to a movie file.
  

**Returns**:

- `int` - Id of the Attachment entity that was created for the image.
  

**Raises**:

- `FileNotFoundError` - If the path does not exist.
- `ValueError` - If the path is not a file.

