---
sidebar_label: user
title: bip.plugins.sgink.user
---

## current\_user

```python
def current_user()
```

Get a Shotgrid user from the current Bip user.

**Returns**:

- `dict` - A dictionary with the Shotgrid HumanUser id and type.

## from\_bip

```python
def from_bip(user)
```

Get a Shotgrid user from a Bip user.

The matching is based on the email address.

**Arguments**:

- `user` _User_ - User BipObject.
  

**Returns**:

- `dict` - A dictionary with the Shotgrid HumanUser id and type.

