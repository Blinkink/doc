---
sidebar_label: utils
title: bip.plugins.sgink.utils
---

## fields

```python
def fields(entity)
```

Get all the available fields of a given entity.

**Arguments**:

- `entity` _str_ - Shotgrid entity.
  

**Returns**:

- `list` - List of strings.

## entities

```python
def entities()
```

Get all the available entities.

**Returns**:

- `list` - List of strings.

