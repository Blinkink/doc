---
title: Project
sidebar_position: 2
---
## Information

:::caution In Progress
This view is not yet implemented!
:::

## Dashboard

:::caution In Progress
This view is not yet implemented!
:::

## Items

![image](../assets/project_item_view.png)

![image](../assets/project_item_edit.png)

## Groups
![image](../assets/project_group_view.png)

![image](../assets/project_group_edit.png)

## Files

:::caution In Progress
This view is not yet implemented!
:::

## Metadata

:::caution In Progress
This view is not yet implemented!
:::

## Tasks

:::caution In Progress
This view is not yet implemented!
:::

## Members

:::caution In Progress
This view is not yet implemented!
:::

## Data model

:::caution In Progress
This view is not yet implemented!
:::

## Plugins

![image](../assets/project_plugin_view.png)

![image](../assets/project_plugin_enable.png)

![image](../assets/project_plugin_edit.png)

## Settings

:::caution In Progress
This view is not yet implemented!
:::


