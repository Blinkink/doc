---
title: For core
sidebar_position: 2
---

This guide explains how to set up your development environment for **working on the client codebase**.

## Setup

Clone the Bip client (https://git.blinkink.co.uk/bip/client.git) repository. Follow the [recommended folder structure](/dev/development/structure.md) for you working directory.

Create a **Python 3.8** virtual environment and install the client dependencies (`cd /path/to/bip/client & pip install -r requirements.txt`).

Follow [the setup tutorial](/dev/development/setup.mdx) to get a running database instance and a Bip config properly done.

## Environment variables

:::tip
For more information about environment variables, check **[this documentation](/dev/misc/env-var.md)**.
:::

When running your script, specify the following environment variables:
- `BIP_CONFIG`: Bip configuration file (_example_: `/path/to/bip/config/config.yml`)
- `BIP_AUTH`: Bip authentication file (_example_: `/path/to/bip/config/auth.yml`)
- `BIP_DATABASE_PASSWORD`: Neo4j password (_example_: `your-database-password`)

Optionally, **if you are working with the updating API** (`mink.update`), you might find those overrides come in handy:

- `BIP_COMPATIBILITY_VERSION`: If you need to override the server config's `compatibility_version` (_example_: `4.0.0`)
- `BIP_CHECK_UPDATES`: If you need to disable the updates checking at initialization. (_example_: `false`)
- `BIP_REFRESH_CONTENT`: If you need to disable the content automatic update at initialization (_example_: `false`)
- `BIP_PYTHON_HOST`: test (_example_: `C:\Users\corentin\Documents\Code\.venvs\host`)
- `BIP_DEBUG_UPDATE_TARGET`: test (_example_: `C:\Users\corentin\AppData\Local\Blink`)

## Guidelines

Make sure you follow the [contribution](/dev/contributing.md) guidelines.
