---
title: Tasks
sidebar_position: 5
---

:::info
The Task system is meant to be refactored with the new Hypervisor application. This one was a draft and is very much flawed.
:::

## Introduction
The Tasks are read by the **Scene Manager** application and allow executing instructions for a given object in a DCC.

- The Scene Manager inspect the current scene. Any know items are extracted and associated with a type (model, camera, node...)
- The Scene Manager inspects all `.py` files found in `[content]/tasks/[current_dcc_id]_tasks` and check their validity
- Based on the class attributes of the task, the Scene Manager will display, for each known scene object (elements), the task that matches the element type and dcc
- When an item is selected, the task UI is generated based on the custom inputs defined in its  initializing method
- When the user executes the task, the `validate()` method is run, and if successful, the `run()` method.

:::caution
Some DCC like Modo don't allow any scene modification from a separated Pyside runtime. In those cases, if you task modifies the scene (rename, move, delete), the DCC could crash
:::


## Skeleton
:::note Specifications
- Filename: `*.py`
- Location: `tasks/[dcc_id]_tasks/`
:::

The way to create a task is to create a class that inherits from the `Task` class, from `bip.api.dcc`. The newly created task must override some mandatory attributes, used by the Scene Manager for inspection or UI generation.

The following example shows the minimal form of a custom task. All attributes here are mandatory.

```python
from bip.deprecated.dcc import Task, Input


class Example(Task):
    actions = ["action"]
    element_types = ["type"]
    dcc = ["software"]
    uid = "something.unique"

    def __init__(self, *args, **kwargs):
        super(Example, self).__init__(*args, **kwargs)
        self.name = "Example"

    def run(self):
        super(DummyTask, self).run()
        pass

TASKS = [Example]
```

The file must comply with the following rules:
- Class inherits from `Task` object from `bip.api.dcc`
- Class attributes:
  - `actions`: See below for available actions
  - `element_types`: See below for available types
  - `dcc`: See below for available DCC
  - `uid`: A unique string identifier between all the tasks
- The `run()` method must be overridden but its original behaviour must be executed first, with `super()`
- The constructor `__init__` must forward arguments and keyword arguments (`*args, **kwargs`)
- A list named `TASKS` must contain a direct reference (non-instantiated) to the task class(es) in the file that you want to be read by the Scene Manager _(Not yet enabled, but recommended for future-proofing)_

## Reference
:::danger
The task folder is not a python package. Bip will inspect its content per file, individually. Therefore, **there cannot be relative imports** in those files. If you need to share resources between files, see the [templates workflow](#templates-workflow).
:::

### Attributes
#### Protected
- **`element`** : _bip.api.dcc._ Element()
- **`action`** : _bip.api.dcc._ Action()
- **`application`** : _bip.api.dcc._ Software()
- **`item`** : _bip.api.deprecated.data._ Item()
- **`project`** : _bip.api.deprecated.data._ Project()
- **`current_version`** : _bip.api.deprecated.data._ Version()
- **`current_component`** : _bip.api.deprecated.data._ Component()
- **`current_item`** : _bip.api.deprecated.data._ Item()
- **`current_file_type`** : _bip.api.deprecated.data._ Group()
- **`current_task`** : _bip.api.deprecated.data._ Task()
- **`current_variant`** : _bip.api.deprecated.data._ Group()

#### User-defined
##### Class-level
- **`actions`**: list _([see available actions ids](#actions))_
- **`modifiers`** (optional): list _([see available modifiers ids](#modifiers))_
- **`element_types`**: list _([see available types ids](#element-types))_
- **`dcc`**: list _([see available DCC ids](#dcc))_
- **`uid`**: str (eg. `mycompany.dcc.task.unique.id.1`)

##### Constructor
- **`name`**: str, task name, only for display purpose
- **`execution_title`**: str, informative text displayed during execution
- **`execution_description`** (optional): str, detailed process displayed during executing
- **`folder_name`**: str, will be used if the task is generating Bip-tracked files
- **`inputs`** _(read-only)_: Returns a list of registered inputs


### Methods
- `run()`: The method that gets run when the user executes the task
- `validate()`: The method that is executed before `run()`. Returns `True` by default. Can be overridden in order to add custom checks. Returning `False` would prevent execution.
- `add_input(new_input)`: Adds a new input to the task _([see below](#input))_
- `get_input(name)`: Returns an input if the given name matches

### Input
Inputs are a way to get user-defined data. You can add as many inputs to your task, they will be automatically turned into the most relevant UI, based on the information you provided for each input.

#### Constructor
- **`name`**: str, Display name for the UI
- **`target`**: str, name of the Task attribute that will store the data
- **`source`**: str, name of the Task method that returns the relevant data (depends on the Input type)
- **`type`**: python type _([see supported types](#supported-types))_
- **`bip`**: bool, indicates if the data are BipObject (from `bip.api.deprecated.data` only. `bip.link` is not yet supported)

:::caution
If the type is a `list`, the getter (source), must return a list of tuples. The tuples must have two items, id (internal), display name (ui). Ids must be unique.
:::

#### Attributes
:::info
These are optional
:::

- **`default`**: Force a default value
- **`range`**: In the type is `int` you can provide a tuple with two values, minimum and maximum.
- **`default_hint`**: Accepted value: `latest` (for a `list`, sets default to the highest value)

#### Methods
- **`depends_on(input, value=None)`: If you have cascading input relationship, you can use this method to trigger the ui populating of the input only when the given input has been modified, or when it returns a specified value.

#### Supported types
- `list`: Generates a **ComboBox**
- `str`: Generates a **LineEdit**
- `bool`: Generates a **CheckBox**
- `int`: Generates a **SpinBox**


## Example
```python
#!/usr/bin/env python
# -*- coding: utf-8 -*-
from bip.deprecated.dcc import Task, Input


class DummyTask(Task):
    """Non-sens task that prints given information and optionnaly saves it in a text file"""

    actions = ["generate"]
    element_types = ["model"]
    dcc = ["modo"]
    uid = "company.unique.task.id.01"

    def __init__(self, *args, **kwargs):
        super(DummyTask, self).__init__(*args, **kwargs)
        self.name = "Dummy Task"

        first_name = Input("First name", target="first_name", source=None, type_=str)
        self.add_input(first_name)
        
        last_name = Input("Last name", target="last_name", source=None, type_=str)
        self.add_input(last_name)
        
        role = Input("Role", target="role", source="get_roles", type_=list)
        role.default = 'freelance'
        self.add_input(role)
        
        save_to_file = Input("Save to file", target='save_to_file', source=None, type_=bool)
        save_to_file.default = False

        self.first_name = None
        self.last_name = None
        self.role = None
        self.save_to_file = None

    def get_roles(self):
        return [
        	("freelance", "Freelance"), 
        	("lead", "Lead"), 
        	("producer", "Producer")
        ]

    def run(self):
        super(DummyTask, self).run()
        message = "Hello {} {}. You are a {}".format(
        	self.first_name,
        	self.last_name,
        	self.role.lower()
        	)
        
        print(message)
        
        if self.save_to_file:
        	with open("result.txt", "w") as f:
                f.write(message)

    def validate(self):
        if not self.first_name.isalpha() or not self.last_name.isalpha():
        	return False

        return True

TASKS = [DummyTask]
```

## Templates workflow
If some of your tasks share the same basic structure (typically, most exporting tasks need the same Bip inputs: Task, Variant and Version), it is recommended to create a template, place it in a `python` at the root of your Content repository, in a package called, for example `my_company`.

The `python` directory being automatically appended to the `PYTHONPATH`, you will be able to import your template class from a task file

## Parameters
### Actions

:::tip
Some actions don't require a scene object (typically for creation tasks)
:::

| Id         | Compatible types | Requires object |
| ---------- | ---------------- | --------------- |
| `export`   | Model, Camera    | Yes             |
| `render`   | Viewport         | Yes             |
| `import`   | Model, Camera    | No              |
| `create`   | Camera           | No              |
| `generate` | Model            | Yes             |
| `update`   | Camera           | Yes             |
| `replace`  | Model            | Yes             |
| `check`    | Model            | Yes             |
| `clean`    | Model            | Yes             |
| `fix`      | Model            | Yes             |
| `remove`   | Model            | Yes             |
| `tag`      | Model            | Yes             |

### Modifiers
If your task is affecting a sub-part of the scene element, you can set the modifier class attribute. This is only for UI generation. _This is meant to be removed, but it is a better practice to keep defining modifiers when necessary, until the modifier concept is removed._

| Id         | Compatible types | Parent actions |
| ---------- | ---------------- | --------------- |
| `material`   | Model    | export, import             |
| `animation`   | Model         | export, import, remove             |

### Element types

- `camera`
- `model`
- `viewport`

### DCC

- `modo`
- `nuke`
- `maya`
- `blender`

