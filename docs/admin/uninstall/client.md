---
title: Client
sidebar_position: 1
---
:::caution
There is not automated tool for removing Bip yet
:::


Remove the following files or directories:
- `C:\Users\<username>\AppData\.blink`
- `C:\Users\<username>\AppData\Local\Blink`
- `C:\Users\<username>\AppData\Roaming\Microsoft\Windows\Start Menu\Programs\Startup\Bip.lnk`
- `C:\Users\<username>\AppData\Roaming\Microsoft\Windows\Start Menu\Programs\Bip.lnk`
- `C:\Users\<username>\AppData\Roaming\Microsoft\Windows\Start Menu\Programs\Update Bip.lnk`
- `C:\Users\<username>\Desktop\Bip.lnk`

Remove the following user environment variables:
:::tip
To remove an environment variable, search for "_Edit environment variables for your account_" in the Windows search menu, open the found tool, then select the variable you want to remove and press **Delete**.
:::

- `BIP_PATH`
- `BIP_PYTHON`