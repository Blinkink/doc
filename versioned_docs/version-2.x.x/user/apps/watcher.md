---
title: Watcher
---

<img height="300" src={require("../../assets/watcher_bip_users.png").default} />


## Introduction

The **Watcher** <img  width="20" height="20" src={require("../../assets/apps/watcher.png").default} /> is a monitoring application, showing metrics from various sources. For now, it serves two purposes:
- Track Bip users activity.
- Track third-party license usage.

### Features
- View active Bip users (for the last 24 hours).
- View third-party software licenses usage for users.
- View third-party software licenses usage for render nodes.
- Bip users and machine hostname are matched when possible.
- Auto-refresh.
- Flag forbidden cases (_i.e.,_ when a user is using several licenses while they should not).

#### Supported licence servers
- **Foundry (RLM)**: Modo, Mari, Nuke, NukeX, NukeStudio, NukeAssist.

### Future
- Support of Autodesk (Maya) license server.
- Support of Adobe license server.
- Bip usage statistics for:
    - Commands
    - Batches
- Flag rules should be editable by administrator.

## Reference
### User licenses
This view lists the users who are using one or more licenses.

:::tip
If the Bip user has set a **messaging link** in their profile, a messaging button appears for chatting with them in one click
:::

![](../../assets/watcher_user_licences.png)

### Render licenses
This view lists the render nodes which are using one or more licenses.

![](../../assets/watcher_render_licences.png)

### Licenses summary
This view lists all the tracked licenses, showing their total count, currently used count, or expiration date.

![](../../assets/watcher_licence_summary.png)

### Bip users
This view lists all the Bip users who have used the Launcher application in the last 24 hours.
![](../../assets/watcher_bip_users.png)

### Raw data
This view shows the raw text file provided by the RLM server. **It is mostly used for debugging.**

## Settings
The Watcher has the following settings are available in the Settings app:

:::caution
The **server** settings are usually automatically set by the administrator at account creation. Do not change them unless you know what you are doing.
:::

- **Server**
    - _Hostname_: Hostname of the RLM server.
    - _Port_: Port listened by the RLM server.
- **Preferences**
    - _Auto-refresh_: Enable automatic refresh of all displayed data.
    - _Refresh frequency_: How often the data is refreshed, in seconds.
    - _Connection timeout_: If the server does not reply, after how long does the Watcher gives up, in seconds.
    

![](../../assets/watcher_settings.png)