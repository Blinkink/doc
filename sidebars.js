/**
 * Creating a sidebar enables you to:
 - create an ordered group of docs
 - render a sidebar for each doc of that group
 - provide next/previous navigation

 The sidebars can be generated from the filesystem, or explicitly defined here.

 Create as many sidebars as you want.
 */

module.exports = {
  user: [{type: 'autogenerated', dirName: 'user'}],
  td: [{type: 'autogenerated', dirName: 'td'}],
  dev: [{type: 'autogenerated', dirName: 'dev'}],
  admin: [{type: 'autogenerated', dirName: 'admin'}],
};
