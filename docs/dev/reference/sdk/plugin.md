---
sidebar_label: _plugin
title: bip.sdk._plugin
sidebar_position: 0
---

## `_Plugin`

```python
class _Plugin()
```

Bip Plugin class.

**Attributes**:

- `logger` _logging.Logger_ - Application logger. If the app has been executed with the sdk runner, the logger is handled by `bip.mink.logs`.
- `config` _dict_ - Plugin configuration dictionary, if the plugin descriptor defines a config template.
- `mapping` _dict_ - Plugin mapping dictionary, if the plugin descriptor defines a mapping template.
- `default_settings` _dict_ - Plugin default settings dictionary, if the plugin descriptor defines a setting template.

### `__init__`

```python
def __init__(plugin, logger)
```

Initializes the plugin representation.

**Arguments**:

- `plugin` _link.plugin.Plugin_ - Associated plugin data. It owns the config, mapping and any exposed parameter in the plugin descriptor.
- `logger` _logging.Logger_ - Application logger. If the app has been executed with the sdk runner, the logger is handled by `bip.mink.logs`.

### `get_package_root`

```python
@classmethod
def get_package_root(cls)
```

Get the application package root.

**Returns**:

- `Pathlib` - package root.

### `get_plugin_descriptor`

```python
@classmethod
def get_plugin_descriptor(cls)
```

Get the plugin descriptor.

Caution, this is the descriptor extracted from the file, not from the database plugin.

This method should only be used internally (it is used by the runner).
If you need the raw descriptor, you should get it from the Plugin object associated with the application.

**Returns**:

- `dict` - raw plugin descriptor.

