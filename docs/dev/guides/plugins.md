---
title: Plugin
sidebar_position: 1
---

:::info Looking for?
- The design principles behind plugins, check **[guide](/dev/design/plugins.md)**.
- A complete reference for plugins classes, `App`, `Tracker`, `Host` and `Submiter`, check the **[SDK](/dev/reference/sdk/app.md)**.
- How to set up your environment for plugin development, check the **[development](/dev/development/plugins.md)**.
- How to manage plugins in your instance, check the **[TD documentation](/td/plugins.md)**.
:::

## Introduction

A Bip plugin must follow some **rules** in order to be validated at registration. This page describes these rules extensively.

Bip plugins also have many **optional features**, from integration to remote execution. The page also list and details how to use these features.

:::tip
Check the **[Chocolatine plugin](https://git.blinkink.co.uk/bip/apps/chocolatine)**, a demo Bip application which illustrates practically the main information given in this page.
:::

## Guidelines

Bip relies on Git features to inspect and update a plugin. Therefore, a plugin must be a valid and public (or at least reachable from your production Bip instance) repository.

### Repository

For a repository to be read as a valid Bip plugin at registration, it must at least contain the following elements:

- `my_plugin/`: Repository root
  - `my_plugin/`: Package root (named the same as the repository root)
  - `bip.yml`: Plugin descriptor
  - `CHANGELOG.md`: Changelog file
  - `pyproject.toml`: Python descriptor

In addition to that mandatory structure, it is recommended to respect the following convention:

- `my_plugin/`: Repository root
  - `my_plugin/`: Package root (named the same as the repository root)
  - `assets/`: Stylesheets, icons, graphic elements...
    - `stylesheet.ink`: Main stylesheet
  - `dev/`: Development scripts
  - `integrations/`: Bip batches for integration
  - `resources/`: Deployment scripts, integration utils...
  - **`bip.yml`**: Plugin descriptor
  - **`CHANGELOG.md`**: Changelog file
  - **`pyproject.toml`**: Python descriptor
  - `README.md`: Readme file


### Git tags

The repository tags are used for upgrading or downgrading a plugin version. Any non _SemVer_ version is ignored. The tags must therefore respect the [Semantic Versioning](https://semver.org/spec/v2.0.0.html) convention. Version number prefix "v" is allowed.

### Changelogs

For the changelog to be parsed by the API (`mink.changelog`), it must follow the following rules:
- Be written in Markdown.
- Be named `CHANGELOG.md`.
- Follow the [Keep a changelog](https://keepachangelog.com/en/1.1.0/) structure.
- Respect the [Semantic Versioning](https://semver.org/spec/v2.0.0.html) convention.
- Reference the related tickets as much as possible.
- Level-two (**h2**) headings must exactly follow this structure: `[x.x.x] - YYYY-MM-DD`
- Level-three (**h3**) headings can only be:
  - _Added_
  - _Changed_
  - _Fixed_
  - _Removed_
  - _Deprecated_
  - _Security_
- Version number prefix "v" is allowed.

Here is a minimalist example:

```markdown
# Changelog

All notable changes to this project will be documented in this file.

## [Unreleased]

### Removed

- Something removed (#548)

## [1.0.1] - 2023-03-05

### Added

- Something added (#842)

### Fixed

- Something fixed (#744)

## [1.0.0] - 2022-10-04

### Added

- Something added (#842)

[unreleased]: https://codebase.ext/my-repo/releases/compare/v1.0.1...HEAD
[1.0.1]: https://codebase.ext/my-repo/releases/compare/v1.0.0...v1.0.1
[1.0.0]: https://codebase.ext/my-repo/releases/tag/v1.0.0
```

## Applications

### Structure
For applications, there is no mandatory structure, since the entry point is defined by the developer themselves in the integration batches. Although, we suggest the following structure, or base structure:

- `my_plugin/my_plugin/`:
  - `__init__.py`
  - `api.py` or `api/`
  - `ui.py` or `ui/`
  - `app.py` (conventional entry point)

##### `__init__.py`

We suggest using this snippet:

```python
from pathlib import Path
import toml


here = Path(__file__).parent
package_root = here.parent
assets = package_root / "assets"
stylesheet = assets / "stylesheet.ink"  # For apps
pyproject_path = package_root / "pyproject.toml"
pyproject = toml.load(str(pyproject_path))
__version__ = pyproject["project"]["version"]
```

##### `app.py` 

If there is no reason not to, we suggest running the application (if standalone) from `app.py`:

```python
from bip.sdk import App, run
from my_app.ui import Window
from my_app import api, stylesheet, __version__


class MyApp(App):
    slug = "my_app"
    version = __version__

    def __init__(self, *args, **kwargs):
        super().__init__(*args, stylesheet=stylesheet, **kwargs)
        self.widget = Window(self)
        self.set_widget(self.widget)

if __name__ == "__main__":
    app = run(MyApp)

```

### Styling

The following keywords can be used in your stylesheet:

- `@app`: Path to the root directory of your application plugin.
- `@main-color`: Bip main color (#edc03b).
- `@quiet-color`: Bip secondary color (#b29c08).
- `@ui`: Path to the client `assets/ui` directory.
- `@icons`: Path to the client `assets/logo` directory.

## Handlers

### Structure
Handlers are expected to provide a `handler.py` entry point.

- `my_plugin/my_plugin/`:
  - `__init__.py`
  - `handler.py` (**mandatory** entry point)

##### `handler.py` 

The entry point is where the plugin class is created (but not initialized, since it is done in due time by the API getters (`mink.plugins`))

```python
from bip.sdk import Host


class MyHost(Host):
    # Override all the methods that are expected to be overriden
    pass
```

## Descriptor

The descriptor `bip.yml` is expected to be found at the repository root.

### General
#### Mandatory
- `name` (_str_): Plugin name.
- `slug` (_str_): Plugin slug (no special characters allowed).
- `type` (_str_): Plugin type, `handler` or `application`.
- `api_version` (_str_): Minimal Bip client version compatible.
- `handler_type` (_str_): **Handlers only** - Handler type, `host`, `submitter` or `tracker`.
- `product` (_str_): **Handlers only** - Wrapped product, `maya`, `shotgrid`, `deadline`...
- `scope` (_str_): **Applications only** - Application scope, `global` or `project`.

#### Optional
- `descriptive_name` (_str_): Functional name.
- `description` (_str_): One or two sentences description of what the plugin does.
- `author` (_str_): Name of the maintainer(s).
- `supported_models` (_list_): **Forbidden for global applications** - If specified, the Plugin would not launch if the current user's active project model is not listed.
- `integrations` (_list_): **Forbidden for handlers** - List of available integrations (see the _Integrations_ section).
- `dependencies` (_dict_): **Forbidden for global applications** - Required Bip plugins (see the _Dependencies_ section).
- `mapping` (_dict_): **Forbidden for global applications** - Mapped entities (see the _Mapping_ section).
- `config` (_dict_): Global plugin settings (see the _Config_ section).
- `settings` (_dict_): User plugin settings (see the _Settings_ section).

### Integrations

:::info
Read the general **[Integrations section](#integrations-1)** to learn more about integrations. This section here only describes how to define them in the descriptor.
:::

You must provide a list of available integrations that the administrator who manages your plugin will choose to enable.

It is **not** expected that all the integrations listed here have their matching batch(es) in the `integrations/` directory. You might have an integration that does not require any batch, since the application's root will anyways be added to the `PYTHONPATH` when a DCC matching a name in that list is started (if the administrator did enable the integration).

- If the application accepts the **standalone** context, add `bip` to the list.
- If the application accepts the **hosted** or **remote** context, add the product name of the host to the list.

### Settings, config and mapping

- The **settings** section of the descriptor allows to define the user settings. Those settings will be generated with their default value for all existing users when the Plugin is enabled and each time a new user is created.
- The **config** section of the descriptor allows to define the global configuration. It is usually set by the administrator, and is unique to the scope (if the scope is project, the config values are only for the project for which the application is enabled).
- The **mapping** section of the descriptor allows to map data entities with application concepts. Since Bip is data-agnostic, each project can have different data models. But some applications might need to ask the administrator "in this project's data structure, what entity corresponds to what I call a "scene"?".

Each entry must be named by its **slug** name. Here are the accepted fields
- `name` (_str_): Parameter name.
- `type` (_str_): Parameter type.
  - Settings accepted values are `str`, `bool`, `int`, `list`.
  - Config accepted values are `str`, `bool`, `int`, `list`.
  - Mapping accepted values are `ComponentTag`, `ItemTag`, `GroupTag`.
- `default` (_depends on "type"_): Default value. (If choices are defined, the default must be one of the choice values).
- `sub_type` (_mandatory if type is `list`_): List item type. Accepted values are `str`, `bool`, `int`.
- `choices` (_dict_) - optional: If you want to restrict choices, provide a dictionary of pairs of slugs and display names.
- `description` (_str_) - optional: Detailed description of the parameter.
- `category` (_str_) - optional: If you want the UI to group parameters, give them the same category value.
- `optional` (_str_) - optional: If true, the user is not forced to specify this value, and it is set to `None`. Default is `False`.

### Example
```yaml
name: Chocolatine
slug: chocolatine
type: application
scope: project
descriptive_name: Demo
description: Bip application demo.
author: Blink
api_version: 4.0.1
integrations:
    - maya
    - blender
    - after-effects
    - photoshop
    - bip
settings:
    string_setting:
        name: String user setting
        type: str
        default: "Hello world"
    bool_setting:
        name: Bool user setting
        description: Example of a boolean user setting.
        type: bool
        default: true
config:
    list_config:
        name: List config
        description: Example of a list of string config.
        type: str
        choices:
            choice1: First choice
            choice2: Second choice
        default: choice1
mapping:
    component_tag_mapping:
        name: Component tag mapping
        description: Example of an entity mapping.
        type: ComponentTag
        default: scene
```

## Integrations

:::caution
Integrations are for **Applications only**.
:::

Integrations are how an application is started by the launcher entry point (**Brioche** by default). They must be enabled in the plugin settings.

The way Bip has chosen to describe how to start a program in the most customizable fashion is to use [**JeanPaulStart**](https://github.com/cube-creative/jeanpaulstart), which allows to write Yaml batches with instruction sequences.

For describing what needs to be done when the user starts your application, you must **write a batch file**. 

:::tip
- You can use the internal environment variable **`$BIP_APPS`** in your batches if you need to use the path where the plugin is stored.
- [**See the TD documentation for the batch syntax and features**](/td/batches).
:::

There are two different launching scenarii:
- **Standalone**: The application is started from the launcher application.
- **Hosted**: The application is started from a DCC (host) which have been started from the launcher.

### Standalone

If you add a `bip.yml` batch in the `integrations/` directory at the root of your application repository, the batch API (`mink.batches`) will find it and it will be listed in the launcher batch list.

If your application has several standalone batches, for example in the case when an application can be started in different modes or views, you can create a `integrations/bip/` directory in which you can add as many batches as you want.

#### Examples

```yaml
---
uid: chocolatine
name: Chocolatine
description: 'Bip application demo'
icon_path: null
os:
  - Windows
tags:
  - Bip
tasks:
  - name: Running application
    raw:
      command: python "$BIP_APPS/chocolatine/chocolatine/app.py"
...
```
You can also check the [Clafoutis integrations directory](https://git.blinkink.co.uk/bip/apps/clafoutis/-/tree/main/integrations/bip) for multi-standalone batches examples.

### Hosted/Remote

By default, the root of the plugin is added to the `PYTHONPATH` by the batch API when the user launches a DCC batch which `host` name matches the integration listed in the `bip.yml`. In most cases, that is enough. But in some cases, you might want to copy some files to the DCC's special folders or preload some scripts.

If your application must be loaded or preloaded when the DCC is started, you can add in the `integrations/` a batch file named after the DCC product name. Then this batch will be executed at the beginning of any user batch which has the `host` parameter set to the same value. For example, if your content has a `maya-2023.yml` batch with `host` set as `maya`, add a `maya.yml` in your `integrations/` directory.

:::info
Not all cases require an integration for the host, even if the application is meant to be executed from a host. In the following examples, these are applications that need to be silently pre-loaded when the DCC starts, because they are slow applications. If your application doesn't need that, you can simply provide a snippet in your README explaining how to call your application, and then it is up to the administrator to integrate your application in their various custom menus or toolbars.
:::

#### Examples

Example from [Kouglof](https://git.blinkink.co.uk/bip/apps/kouglof/-/tree/main/integrations), which has a **hosted integration for Maya**.

```yaml
---
name: Maya
icon_path: $BIP_CONTENT/assets/icons/maya.png
tags:
  - Handler
tasks:
  - name: Add Kouglof to Python environment
    environment:
      name: PYTHONPATH
      value: $BIP_APPS/kouglof/resources/maya
      append: true
...
```

Example from [Kouglof](https://git.blinkink.co.uk/bip/apps/kouglof/-/tree/main/integrations), which has a **remote integration for Photoshop**.

```yaml
---
name: Photoshop
icon_path: $BIP_CONTENT/assets/icons/photoshop.png
tags:
  - Handler
tasks:
  - name: Preload Kouglof
    raw:
      command: python "$BIP_APPS/kouglof/kouglof/loader.py" --context remote
...
```

### Mixed

An application can have all kind of integration batches at the same time depending on the contexts it supports (see the [`supported_contexts`](/dev/reference/sdk/app#app) class attribute)


## Updates
What happens when a plugin is updated (config, api version....)

## Running
### Production
- **Application**: Ran using `sdk.runner` (**managed by the developper**)
- **Handler**: Ran using `mink.plugin` (**managed internally by the API**)
  - _Tracker_ (_WIP_): Automatically initialized and attached to the Application when the project has a tracker enabled.
  - _Submitter_ (_WIP_): Automatically initialized and attached to the Application when the project has a submitter enabled.
  - _Host_: Automatically initialized and attached to the Application when the context is `HOSTED` or `REMOTE`.

#### Application

Since there are often many different ways to execute a single application, it is left to the discretion of the developper to chose how to initialize the application.

A very typical case could be to have the main `foo/foo/app.py`:

```python
class Foo(App):
    slug = "foo"


if __name__ == "__main__":
    app = run(Foo)
```

And a corresponding standalone integration batch would simply be:

```yaml
---
uid: foo
name: Foo
icon_path: null
tasks:
  - name: Running application
    raw:
      command: python "$BIP_APPS/foo/foo/app.py"
...
```

### Development
[See the related documentation](/dev/development/plugins) to know how to run a plugin in a development context.