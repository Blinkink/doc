---
title: Client
sidebar_position: 1
---
## Installer
### Prerequisites
- Windows 10
- Git ([download here](https://git-scm.com/download/win))
    - Windows Explorer integration: No
    - Add to `PATH`: Yes
- Internet connection
- Administrator permissions (if Python 2.7 or Python 3.8 are not found, the installer will download and install them)


### Step-by-step
:::info
The installation is **per account**. It is not allowed to install Bip for all the users, because each user can set a specific version or branch of Bip based on their preferences.
:::

:::danger
If the machine has a previous Bip v0.x.x or v1.x.x install, it must be [removed first.](../uninstall/windows)
:::

Download the latest Bip installer from the [latest Bip release](https://git.blinkink.co.uk/bip/packages/-/raw/master/bip-latest-win10-amd64.exe).

Execute `bip-latest-win10-amd64.exe`.

The executable not being signed, Windows will prevent you from running it. Click on More info.

![Windows allow running dialog](./assets/allow-running-1.png)

Click on Run anyway.

![Windows allow running dialog expanded](./assets/allow-running-2.png)

:::info
A terminal will pop up, do not close it.
:::

#### Welcome

![Installer welcome view](./assets/welcome.png)

Press **Start**.

#### Paths

:::caution
You cannot choose yet the installation path and the content path. They will be set automatically
:::

![Installer paths view](./assets/paths.png)

Press **Next**

#### Database

Enter your database details and press **Next**

![Installer database view](./assets/database.png)

#### Working directory

:::caution
Offline mode not yet supported. Leave unchecked.
:::
Enter the root of your working directory and press **Install**.

![Installer working directory view](./assets/working-dir.png)

#### Install
The installer is now running. It can take a few minutes.

![Installer running](./assets/process.png)

After a few minutes, the installer will ask to start Bip in update Mode. Press **Ok**.

![Installer successful](./assets/success.png)

:::danger
Do not close the terminal yet!
:::

Wait for a few seconds for the Bip updater to start.

![Updater](./assets/updater.png)

The update should take less than 20 seconds.

Once completed, the updater will show the login dialog.

![Login](./assets/login.png)

The installation is finished, you can close the terminal (it will close the Bip instance that has been started by the installer).

## Manual
:::caution
Work in progress. If you need to do this, please **ask a developer**
:::

Install **Python 3.8** if it doesn't exist. Make sure it is added to `PATH`, as well as **pip**

```shell
git config --global http.sslVerify false
git clone https://git.blinkink.co.uk/bip/client.git
cd client/installer/
pip3 install -r requirements.txt
/c/Users/<username>/AppData/Local/Programs/Python/Python38/python.exe app.py
```

Follow regular[ instructions](#step-by-step)

## What is does
The **installer** will:
- Clone the **Bip** repository provided by the database using Git
- Clone the **Custom Content** repository provided by the database using Git
- Look for **Python 2.7**. If not found, it will download it and install it (administrator password required)
- Look for **Python 3.8**. If not found, it will download it and install it (administrator password required)
- Create a **Python 2.7 virtual environement** in the Bip install directory
- Create a **Python 3.8 virtual environement** in the Bip install directory
- Install Bip requiered Python **packages for the Python 2.7** virtual environement
- Install Bip requiered Python **packages for the Python 3.8** virtual environement

The **updater** will:
- Check out the **Bip** repository
- Check out the **Custom Content** repository
- Check for **dependencies** (and install the new ones if any)
- Install desktop and start menu **shortcuts**

## Troubleshooting
### The installer crashes with no explanation
:::tip Solution
You can start the installer from a **terminal** in order to have time to see the error message
:::
- Delete `C:\Users\<username>\AppData\Local\Blink`
- Start a terminal: Start menu > Command prompt
- Go to the directory with the installer executable. (eg: `cd C:/Users/<username>/Downloads`)
- Start the executable (eg. `bip-x.x.x-win10-amd64.exe`)

### **Crash**: QPaintDevice: Cannot destroy paint...

:::danger
No solution. You must **[install manually](#manual)**
:::

```python
QPaintDevice: Cannot destroy paint device that is being painted
QThread: Destroyed while thread is still running
```

The machine you are trying to install Bip on has a different Windows  version or CPU architecture than the one with wich the installer has  been compiled. There is no other workaround than **[installing by hand](#manual)**.

### **ImportError**: This package should not be accessible on Python 3

:::tip Solution
You should **clear** `PYTHONPATH`
:::

```python
ImportError: This package should not be accessible on Python 3. Either you are trying to run from the python-future src folder or your installation of python-future is corrupted.
```

Make sure the **`PYTHONPATH`** variable doesnt exists or does not contain paths heading to Python 2.x packages. If there was an Bip v1.x.x installed on this machine, that could be the cause.

### **ImportError**: Bad git executable
:::tip Solution
**Add** the git executable to `PATH`
:::

```python
Traceback (most recent call last):
  File "git\__init__.py", line 83, in <module>
  File "git\__init__.py", line 73, in refresh
  File "git\cmd.py", line 278, in refresh
ImportError: Bad git executable.
The git executable must be specified in one of the following ways:
    - be included in your $PATH
    - be set via $GIT_PYTHON_GIT_EXECUTABLE
    - explicitly set via git.refresh()
```

Git must be installed before executing the installer, and must have been added to `PATH`.
You can remove and re-install Git, making sure that `PATH` setup choices look like this:

![Login](./assets/git.png)

Or you can manually add git to the `PATH` environement variable

- *Start menu > Edit the system environement variables > Environement Variables...*
- Add the git path to either the User `PATH` or the System `PATH` (eg: `C:\Program Files\Git\cmd`)

### **Error**: Installation path is not empty
:::tip Solution
You must delete `C:\Users\<username>\AppData\Local\Blink`.
:::

![Login](./assets/error.png)