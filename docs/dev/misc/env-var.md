---
title: Environment variables
sidebar_position: 1
---
## Parametric
These variables are not set by Bip, but if they are manually set, they would override the default internal value they refer to.

| Name                            | Description                                                                                                                                                                                       | Type           | Default                                       |
|---------------------------------|---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|----------------|-----------------------------------------------|
| **`BIP_CHECK_UPDATES`**         | Defines if the Launcher checks for updates                                                                                                                                                        | bool           | `true`                                        |
| **`BIP_REFRESH_CONTENT`**       | Defines if the Launcher reloads (`git pull`) the content when started                                                                                                                             | bool           | `true`                                        |
| **`BIP_DATABASE_URI`**          | Overrides the database URI                                                                                                                                                                        | str            | Server-provided value (`ServerConfig` object) |
| **`BIP_DATABASE_USER`**         | Overrides the database user                                                                                                                                                                       | str            | Server-provided value (`ServerConfig` object) |
| **`BIP_DATABASE_PASSWORD`**     | Overrides the database password                                                                                                                                                                   | str            | Server-provided value (`ServerConfig` object) |
| **`BIP_COMPATIBILITY_VERSION`** | Overrides the minimal API compatibility version                                                                                                                                                   | str            | Server-provided value (`ServerConfig` object) |
| **`BIP_DEBUG_UPDATE_TARGET`**   | Path to apply the update on (useful when developing with `mink.update` functions)                                                                                                                 | directory path | `none`                                        |
| **`BIP_DEBUG_PLUGIN`**          | List of path to the root directory of plugins that should be fed with a fake temporary representation of itself, based on the found descriptor, instead of looking for the plugin in the database | bool           | `/path/to/my/plugin;/path/to/another/plugin`  |



## Data
These variables are always set by Bip and can be used in Bip-executed user scripts.
Some of them can be overridden for **debugging** purpose only.

:::info
On a classic installation, `BIP_PATH` should be set permanently unless the Updater runs for the first time.
Otherwise, unset `BIP_PATH` should only be overridden for development purpose.
:::

| Name                        | Description                                                        | Type           | Example                                               | Overridable |
|-----------------------------|--------------------------------------------------------------------|----------------|-------------------------------------------------------|-------------|
| **`BIP_PATH`**              | Main installation folder                                           | directory path | *`~\AppData\Local\Blink`*                             | ✔️          |
| **`BIP_ROOT`**              | Bip repository root                                                | directory path | *`~\AppData\Local\Blink\bip`*                         | ✔️          |
| **`BIP_PACKAGE`**           | Bip package root                                                   | directory path | *`~\AppData\Local\Blink\bip\bip`*                     | ✔️          |
| **`BIP_PYTHON`**            | Python 3.8 virtual environment                                     | directory path | *`~\AppData\Local\Blink\python`*                      | ✔️          |
| **`BIP_PYTHON_HOST`**       | Python 3.7 virtual environment                                     | directory path | *`~\AppData\Local\Blink\hosted`*                      | ✔️          |
| **`BIP_CONTENT`**           | Content repository defined in the system config (see `BIP_CONFIG`) | directory path | *`~\AppData\Local\Blink\content`*                     | ✔️          |
| **`BIP_USER_DIRECTORY`**    | Bip user folder                                                    | directory path | *`~\.blink`*                                          | ✔️          |
| **`BIP_CONFIG`**            | Local system config file                                           | file path      | *`~\.blink\config.yml`*                               | ✔️          |
| **`BIP_AUTH`**              | Authentication file (current username and password hash)           | file path      | *`~\.blink\auth.yml`*                                 | ✔️          |
| **`BIP_PACKAGE`**           | Bip package root                                                   | directory path | *`~\AppData\Local\Blink\bip\bip`*                     | ❌           |
| **`BIP_APPS`**              | Bip application plugins root                                       | directory path | *`~\AppData\Local\Blink\apps`*                        | ❌           |
| **`BIP_HANDLERS`**          | Bip handlers plugins root                                          | directory path | *`~\AppData\Local\Blink\handlers`*                    | ❌           |
| **`BIP_PYTHON_LIBS`**       | Python 3.8 site-packages folder                                    | directory path | *`~\AppData\Local\Blink\python\Lib\site-packages`*    | ❌           |
| **`BIP_PYTHON_HOST_LIBS`**  | Python 3.7 site-packages folder                                    | directory path | *`~\AppData\Local\Blink\hosted\Lib\site-packages`*    | ❌           |
| **`BIP_VERSION`**           | Current Bip version (from `__init__.py`)                           | version number | *`3.1.5`*                                             | ❌           |
| **`BIP_INTEGRATIONS`**      | Bip integration scripts (mainly for embedding in DCC)              | directory path | *`~\AppData\Local\Blink\bip\bip\integrations`*        | ❌           |
| **`BIP_ICONS`**             | Bip applications icons                                             | directory path | *`~\AppData\Local\Blink\bip\bip\assets\images\icons`* | ❌           |
| **`BIP_WORKING_DIRECTORY`** | Working directory defined in the system config (see `BIP_CONFIG`)  | directory path | *`\\network\location\where\you\work`*                 | ❌           |
| **`BIP_CURRENT_BRANCH`**    | Current Git branch of the Client repository                        | str            | *`develop`, `main`*                                   | ❌           |
| **`BIP_CURRENT_USER`**      | Username of the current user                                       | str            | *`firstname.lastname`*                                | ❌           |
| **`BIP_ACTIVE_PROJECT`**    | Slug of the currently active project                               | str            | *`my-project`*                                        | ❌           |
