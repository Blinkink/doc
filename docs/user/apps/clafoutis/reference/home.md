---
title: Home
sidebar_position: 1
---

## Start
### Dashboard

:::caution In Progress
This view is not yet implemented!
:::

### Projects
![image](../assets/home_projects.png)

## Utilities
### Activity

:::caution In Progress
This view is not yet implemented!
:::

### Report an issue

:::caution In Progress
This view is not yet implemented!
:::

### Help

:::caution In Progress
This view is not yet implemented!
:::

## Configuration
### My settings
[See the dedicated page...](settings.md)

### Administration
[See the dedicated page...](administration.md)