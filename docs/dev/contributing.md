---
title: Contributing
sidebar_position: 8
---

## Styleguide

### Workflow
We expect external contributors to comply with the following conventions:
* We follow the [Minifesto](http://minifesto.org/)
* We follow the [Agile Manifesto](https://agilemanifesto.org/)
* We follow the [SemVer](https://semver.org/) guidelines
* We follow the [Keep a Changelog](https://keepachangelog.com/en/1.0.0/) guidelines
* We follow the [VFX Reference Platform](https://vfxplatform.com/) recommendations when they fucking make sense.

### Python
* We format with [Black](https://black.readthedocs.io/en/stable/)
* We respect the [PEP 20 (Zen of Python)](https://peps.python.org/pep-0020/)
* We respect the [PEP 8 (Style guide)](https://peps.python.org/pep-0008/)
* We follow the [Google Style Guide](https://google.github.io/styleguide/pyguide.html)
* We strictly format our docstrings following the [Google style](https://google.github.io/styleguide/pyguide.html#s3.8-comments-and-docstrings)

## Code of conduct

We respect and enforce the [Contributor Covenant](https://git.blinkink.co.uk/bip/client/-/blob/main/CODE_OF_CONDUCT.md).